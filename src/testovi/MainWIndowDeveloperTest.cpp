#include "catch.hpp"

#include "../tinDev/mainwindowdeveloper.h"

TEST_CASE("MainWindowDeveloper", "[class]") {
  SECTION("Testiranje funkcije postaviUsername") {
    // Arrange
    MainWindowDeveloper *d = new MainWindowDeveloper();
    QString username = "papa";

    // Act
    d->postaviUsername(username);

    // Assert
    REQUIRE(d->getUi()->leUsername->text() == "papa");

    delete d;
  }
  SECTION("Testiranje funkcije ukloniPodatke") {
    // Arrange
    MainWindowDeveloper *d = new MainWindowDeveloper();

    // Act
    d->ukloniPodatke();

    // Assert
    REQUIRE(d->getUi()->lbNazivFirme->text() == "");
    REQUIRE(d->getUi()->lbPlata->text() == "");
    REQUIRE(d->getUi()->lbNacinRada->text() == "");
    REQUIRE(d->getUi()->lbStrucnaSprema->text() == "");

    delete d;
  }
}
