#include "../tinDev/oglas.h"
#include "catch.hpp"

TEST_CASE("Oglas konstruktor", "[function]") {
  // Arrange
  QString nazivPozicije = "Software Developer";
  unsigned plata = 50000;
  NacinRada nacinRada = NacinRada::Hybrid;
  StrucnaSprema strucnaSprema = StrucnaSprema::SrednjaSkola;
  QVector<QString> tehnologije = {"C++", "Qt"};
  QMap<QString, unsigned> brGodIskustva = {{"C++", 3}, {"Qt", 2}};
  QVector<QString> vestine = {"Problem Solving", "Teamwork"};
  QVector<QString> benefiti = {"Flexible hours", "Health insurance"};
  QString usernameFirma = "MyCompany";

  // Act
  Oglas *oglas = new Oglas(nazivPozicije, plata, nacinRada, strucnaSprema, tehnologije, brGodIskustva, vestine, benefiti, usernameFirma);
  // Assert
  REQUIRE(oglas->nazivPozicije() == nazivPozicije);
  REQUIRE(oglas->plata() == plata);
  REQUIRE(oglas->nacinRada() == nacinRada);
  REQUIRE(oglas->strucnaSprema() == strucnaSprema);
  REQUIRE(oglas->tehnologije() == tehnologije);
  REQUIRE(oglas->brGodIskustva() == brGodIskustva);
  REQUIRE(oglas->vestine() == vestine);
  REQUIRE(oglas->benefiti() == benefiti);
  REQUIRE(oglas->usernameFirma() == usernameFirma);

  delete oglas;
}

TEST_CASE("Oglas drugi konstruktor", "[function]") {
  // Arrange
  QString nazivPozicije = "Software Developer";
  unsigned plata = 50000;
  NacinRada nacinRada = NacinRada::Hybrid;
  StrucnaSprema strucnaSprema = StrucnaSprema::SrednjaSkola;
  QVector<QString> tehnologije = {"C++", "Qt"};
  QMap<QString, unsigned> brGodIskustva = {{"C++", 3}, {"Qt", 2}};
  QVector<QString> vestine = {"Problem Solving", "Teamwork"};
  QVector<QString> benefiti = {"Flexible hours", "Health insurance"};
  QString usernameFirma = "MyCompany";
  QString id = "12345";

  // Act
  Oglas *oglas = new Oglas(nazivPozicije, plata, nacinRada, strucnaSprema, tehnologije, brGodIskustva, vestine, benefiti, usernameFirma, id);
  // Assert
  REQUIRE(oglas->nazivPozicije() == nazivPozicije);
  REQUIRE(oglas->plata() == plata);
  REQUIRE(oglas->nacinRada() == nacinRada);
  REQUIRE(oglas->strucnaSprema() == strucnaSprema);
  REQUIRE(oglas->tehnologije() == tehnologije);
  REQUIRE(oglas->brGodIskustva() == brGodIskustva);
  REQUIRE(oglas->vestine() == vestine);
  REQUIRE(oglas->benefiti() == benefiti);
  REQUIRE(oglas->usernameFirma() == usernameFirma);
  REQUIRE(oglas->id() == id);

  delete oglas;
}

TEST_CASE("Oglas::stepenStrucneSpremeStr", "[function]") {

  SECTION("Srednja skola") {
    // Arrange
    QString nazivPozicije = "Software Developer";
    unsigned plata = 50000;
    NacinRada nacinRada = NacinRada::Hybrid;
    StrucnaSprema strucnaSprema = StrucnaSprema::SrednjaSkola;
    QVector<QString> tehnologije = {"C++", "Qt"};
    QMap<QString, unsigned> brGodIskustva = {{"C++", 3}, {"Qt", 2}};
    QVector<QString> vestine = {"Problem Solving", "Teamwork"};
    QVector<QString> benefiti = {"Flexible hours", "Health insurance"};
    QString usernameFirma = "MyCompany";
    QString id = "12345";

    Oglas *oglas = new Oglas(nazivPozicije, plata, nacinRada, strucnaSprema, tehnologije, brGodIskustva, vestine, benefiti, usernameFirma, id);
    // Act & Assert
    REQUIRE(oglas->stepenStrucneSpremeStr() == "Srednja skola");
    delete oglas;
  }

  SECTION("Osnovne studije") {
    // Arrange
    QString nazivPozicije = "Software Developer";
    unsigned plata = 50000;
    NacinRada nacinRada = NacinRada::Hybrid;
    StrucnaSprema strucnaSprema = StrucnaSprema::OsnovneStudije;
    QVector<QString> tehnologije = {"C++", "Qt"};
    QMap<QString, unsigned> brGodIskustva = {{"C++", 3}, {"Qt", 2}};
    QVector<QString> vestine = {"Problem Solving", "Teamwork"};
    QVector<QString> benefiti = {"Flexible hours", "Health insurance"};
    QString usernameFirma = "MyCompany";
    QString id = "12345";

    Oglas *oglas = new Oglas(nazivPozicije, plata, nacinRada, strucnaSprema, tehnologije, brGodIskustva, vestine, benefiti, usernameFirma, id);
    // Act & Assert
    REQUIRE(oglas->stepenStrucneSpremeStr() == "Osnovne studije");
    delete oglas;
  }

  SECTION("Master studije") {
    // Arrange
    QString nazivPozicije = "Software Developer";
    unsigned plata = 50000;
    NacinRada nacinRada = NacinRada::Hybrid;
    StrucnaSprema strucnaSprema = StrucnaSprema::MasterStudije;
    QVector<QString> tehnologije = {"C++", "Qt"};
    QMap<QString, unsigned> brGodIskustva = {{"C++", 3}, {"Qt", 2}};
    QVector<QString> vestine = {"Problem Solving", "Teamwork"};
    QVector<QString> benefiti = {"Flexible hours", "Health insurance"};
    QString usernameFirma = "MyCompany";
    QString id = "12345";

    Oglas *oglas = new Oglas(nazivPozicije, plata, nacinRada, strucnaSprema, tehnologije, brGodIskustva, vestine, benefiti, usernameFirma, id);
    // Act & Assert
    REQUIRE(oglas->stepenStrucneSpremeStr() == "Master studije");
    delete oglas;
  }

  SECTION("Doktorske studije") {
    // Arrange
    QString nazivPozicije = "Software Developer";
    unsigned plata = 50000;
    NacinRada nacinRada = NacinRada::Hybrid;
    StrucnaSprema strucnaSprema = StrucnaSprema::DoktorskeStudije;
    QVector<QString> tehnologije = {"C++", "Qt"};
    QMap<QString, unsigned> brGodIskustva = {{"C++", 3}, {"Qt", 2}};
    QVector<QString> vestine = {"Problem Solving", "Teamwork"};
    QVector<QString> benefiti = {"Flexible hours", "Health insurance"};
    QString usernameFirma = "MyCompany";
    QString id = "12345";

    Oglas *oglas = new Oglas(nazivPozicije, plata, nacinRada, strucnaSprema, tehnologije, brGodIskustva, vestine, benefiti, usernameFirma, id);
    // Act & Assert
    REQUIRE(oglas->stepenStrucneSpremeStr() == "Doktorske studije");
    delete oglas;
  }

  SECTION("Nepoznato obrazovanje") {
    // Arrange
    QString nazivPozicije = "Software Developer";
    unsigned plata = 50000;
    NacinRada nacinRada = NacinRada::Hybrid;
    StrucnaSprema strucnaSprema = static_cast<StrucnaSprema>(-1);
    QVector<QString> tehnologije = {"C++", "Qt"};
    QMap<QString, unsigned> brGodIskustva = {{"C++", 3}, {"Qt", 2}};
    QVector<QString> vestine = {"Problem Solving", "Teamwork"};
    QVector<QString> benefiti = {"Flexible hours", "Health insurance"};
    QString usernameFirma = "MyCompany";
    QString id = "12345";

    Oglas *oglas = new Oglas(nazivPozicije, plata, nacinRada, strucnaSprema, tehnologije, brGodIskustva, vestine, benefiti, usernameFirma, id);
    // Act & Assert
    REQUIRE_THROWS_WITH(oglas->stepenStrucneSpremeStr(), "Nepoznato obrazovanje");
    delete oglas;
  }
}

TEST_CASE("Oglas::nacinRadaStr", "[function]") {

  SECTION("Hybrid") {
    // Arrange
    QString nazivPozicije = "Software Developer";
    unsigned plata = 50000;
    NacinRada nacinRada = NacinRada::Hybrid;
    StrucnaSprema strucnaSprema = StrucnaSprema::SrednjaSkola;
    QVector<QString> tehnologije = {"C++", "Qt"};
    QMap<QString, unsigned> brGodIskustva = {{"C++", 3}, {"Qt", 2}};
    QVector<QString> vestine = {"Problem Solving", "Teamwork"};
    QVector<QString> benefiti = {"Flexible hours", "Health insurance"};
    QString usernameFirma = "MyCompany";
    QString id = "12345";

    Oglas *oglas = new Oglas(nazivPozicije, plata, nacinRada, strucnaSprema, tehnologije, brGodIskustva, vestine, benefiti, usernameFirma, id);
    // Act & Assert
    REQUIRE(oglas->nacinRadaStr() == "Hybrid");
    delete oglas;
  }

  SECTION("Office") {
    // Arrange
    QString nazivPozicije = "Software Developer";
    unsigned plata = 50000;
    NacinRada nacinRada = NacinRada::Office;
    StrucnaSprema strucnaSprema = StrucnaSprema::OsnovneStudije;
    QVector<QString> tehnologije = {"C++", "Qt"};
    QMap<QString, unsigned> brGodIskustva = {{"C++", 3}, {"Qt", 2}};
    QVector<QString> vestine = {"Problem Solving", "Teamwork"};
    QVector<QString> benefiti = {"Flexible hours", "Health insurance"};
    QString usernameFirma = "MyCompany";
    QString id = "12345";

    Oglas *oglas = new Oglas(nazivPozicije, plata, nacinRada, strucnaSprema, tehnologije, brGodIskustva, vestine, benefiti, usernameFirma, id);
    // Act & Assert
    REQUIRE(oglas->nacinRadaStr() == "Office");
    delete oglas;
  }

  SECTION("Remote") {
    // Arrange
    QString nazivPozicije = "Software Developer";
    unsigned plata = 50000;
    NacinRada nacinRada = NacinRada::Remote;
    StrucnaSprema strucnaSprema = StrucnaSprema::MasterStudije;
    QVector<QString> tehnologije = {"C++", "Qt"};
    QMap<QString, unsigned> brGodIskustva = {{"C++", 3}, {"Qt", 2}};
    QVector<QString> vestine = {"Problem Solving", "Teamwork"};
    QVector<QString> benefiti = {"Flexible hours", "Health insurance"};
    QString usernameFirma = "MyCompany";
    QString id = "12345";

    Oglas *oglas = new Oglas(nazivPozicije, plata, nacinRada, strucnaSprema, tehnologije, brGodIskustva, vestine, benefiti, usernameFirma, id);
    // Act & Assert
    REQUIRE(oglas->nacinRadaStr() == "Remote");
  }

  SECTION("Nepoznat nacin rada") {
    // Arrange
    QString nazivPozicije = "Software Developer";
    unsigned plata = 50000;
    NacinRada nacinRada = static_cast<NacinRada>(-1);
    StrucnaSprema strucnaSprema = StrucnaSprema::DoktorskeStudije;
    QVector<QString> tehnologije = {"C++", "Qt"};
    QMap<QString, unsigned> brGodIskustva = {{"C++", 3}, {"Qt", 2}};
    QVector<QString> vestine = {"Problem Solving", "Teamwork"};
    QVector<QString> benefiti = {"Flexible hours", "Health insurance"};
    QString usernameFirma = "MyCompany";
    QString id = "12345";

    Oglas *oglas = new Oglas(nazivPozicije, plata, nacinRada, strucnaSprema, tehnologije, brGodIskustva, vestine, benefiti, usernameFirma, id);
    // Act & Assert
    REQUIRE_THROWS_WITH(oglas->nacinRadaStr(), "Nepoznat nacin rada");
    delete oglas;
  }
}

TEST_CASE("Oglas::fromVariant", "[function]") {
  // Arrange
  QVariantMap sampleData;
  sampleData["nazivPozicije"] = "Software Developer";
  sampleData["plata"] = 50000;
  sampleData["tehnologije"] = QVariant::fromValue<QVector<QString>>({"C++", "Qt"});
  QVariantMap brGodIskustvaMap;
  brGodIskustvaMap["C++"] = 3;
  brGodIskustvaMap["Qt"] = 2;
  sampleData["brGodIskustva"] = brGodIskustvaMap;
  sampleData["nacinRada"] = static_cast<int>(NacinRada::Hybrid);
  sampleData["vestine"] = QVariant::fromValue<QVector<QString>>({"Problem Solving", "Teamwork"});
  sampleData["benefiti"] = QVariant::fromValue<QVector<QString>>({"Flexible hours", "Health insurance"});
  sampleData["strucnaSprema"] = static_cast<int>(StrucnaSprema::OsnovneStudije);
  sampleData["usernameFirma"] = "MyCompany";
  sampleData["id"] = "12345";

  Oglas *oglas = new Oglas();
  // Act
  oglas->fromVariant(QVariant::fromValue(sampleData));

  // Assert
  REQUIRE(oglas->nazivPozicije() == "Software Developer");
  REQUIRE(oglas->plata() == 50000);
  REQUIRE(oglas->nacinRada() == NacinRada::Hybrid);
  REQUIRE(oglas->strucnaSprema() == StrucnaSprema::OsnovneStudije);
  REQUIRE(oglas->tehnologije() == QVector<QString>({"C++", "Qt"}));
  REQUIRE(oglas->brGodIskustva() == QMap<QString, unsigned>({{"C++", 3}, {"Qt", 2}}));
  REQUIRE(oglas->vestine() == QVector<QString>({"Problem Solving", "Teamwork"}));
  REQUIRE(oglas->benefiti() == QVector<QString>({"Flexible hours", "Health insurance"}));
  REQUIRE(oglas->usernameFirma() == "MyCompany");

  delete oglas;
}

TEST_CASE("Oglas::toVariant", "[function]") {
  // Arrange
  QString nazivPozicije = "Software Developer";
  unsigned plata = 50000;
  NacinRada nacinRada = static_cast<NacinRada>(-1);
  StrucnaSprema strucnaSprema = StrucnaSprema::DoktorskeStudije;
  QVector<QString> tehnologije = {"C++", "Qt"};
  QMap<QString, unsigned> brGodIskustva = {{"C++", 3}, {"Qt", 2}};
  QVector<QString> vestine = {"Problem Solving", "Teamwork"};
  QVector<QString> benefiti = {"Flexible hours", "Health insurance"};
  QString usernameFirma = "MyCompany";
  QString id = "12345";

  Oglas *oglas = new Oglas(nazivPozicije, plata, nacinRada, strucnaSprema, tehnologije, brGodIskustva, vestine, benefiti, usernameFirma, id);

  // Act
  QVariant result = oglas->toVariant();

  // Assert
  QVariantMap resultMap = result.toMap();
  REQUIRE(resultMap["nazivPozicije"].toString() == nazivPozicije);
  REQUIRE(resultMap["plata"].toUInt() == plata);
  REQUIRE(resultMap["nacinRada"].toInt() == static_cast<int>(nacinRada));
  REQUIRE(resultMap["strucnaSprema"].toInt() == static_cast<int>(strucnaSprema));
  REQUIRE(resultMap["tehnologije"].toStringList() == tehnologije.toList());
  REQUIRE(resultMap["vestine"].toStringList() == vestine.toList());
  REQUIRE(resultMap["benefiti"].toStringList() == benefiti.toList());
  REQUIRE(resultMap["usernameFirma"].toString() == usernameFirma);
  REQUIRE(resultMap["id"].toString() == id);

  delete oglas;
}
