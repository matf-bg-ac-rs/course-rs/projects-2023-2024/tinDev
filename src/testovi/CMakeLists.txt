cmake_minimum_required(VERSION 3.14)

project(testovi VERSION 0.1 LANGUAGES CXX)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

find_package(QT NAMES Qt6 Qt5 REQUIRED COMPONENTS Core)
find_package(Qt${QT_VERSION_MAJOR} REQUIRED COMPONENTS Core)
find_package(Qt6 COMPONENTS Widgets Gui Core Network REQUIRED)
find_package(Qt6 REQUIRED COMPONENTS Test)

include_directories(
    .
    ../tinDev
    ../clientServer
    ../serialization
)

set(TEST_SOURCES
    main.cpp
    SearchThreadTest.cpp
    developerTest.cpp
    firmaTest.cpp
    LoginThreadTest.cpp
    matchTest.cpp
    oglasTest.cpp
    procenatTest.cpp
    ReadFileThreadTest.cpp
    UsernameTestThreadTest.cpp
    DodajOglasTest.cpp
    IzmeniNalogDeveloperTest.cpp
    IzmeniNalogFirmaTest.cpp
    IzmeniOglasTest.cpp
    MainWIndowDeveloperTest.cpp
    MainWindowFirmaTest.cpp
    SviOglasiTest.cpp
)

set(CPP_SOURCES
    ../tinDev/searchthread.cpp
    ../tinDev/searchthread.h
    ../tinDev/developer.cpp
    ../tinDev/developer.h
    ../tinDev/korisnik.cpp
    ../tinDev/korisnik.h
    ../tinDev/firma.cpp
    ../tinDev/firma.h
    ../tinDev/loginthread.h
    ../tinDev/loginthread.cpp
    ../tinDev/match.cpp
    ../tinDev/match.h
    ../tinDev/oglas.cpp
    ../tinDev/oglas.h
    ../tinDev/procenat.cpp
    ../tinDev/procenat.h
    ../tinDev/readfilethread.cpp
    ../tinDev/readfilethread.h
    ../tinDev/usernametestthread.cpp
    ../tinDev/usernametestthread.h
    ../tinDev/dodajoglas.cpp
    ../tinDev/dodajoglas.h
    ../tinDev/dodajoglas.ui
    ../serialization/jsonserializer.h
    ../serialization/jsonserializer.cpp
    ../serialization/serializer.h
    ../serialization/serializer.cpp
    ../tinDev/izmeninalogdeveloper.cpp
    ../tinDev/izmeninalogdeveloper.h
    ../tinDev/izmeninalogdeveloper.ui
    ../tinDev/izmeninalogfirma.cpp
    ../tinDev/izmeninalogfirma.h
    ../tinDev/izmeninalogfirma.ui
    ../tinDev/izmenioglas.cpp
    ../tinDev/izmenioglas.h
    ../tinDev/izmenioglas.ui
    ../tinDev/mainwindowfirma.cpp
    ../tinDev/mainwindowfirma.h
    ../tinDev/mainwindowfirma.ui
    ../tinDev/mainwindowdeveloper.cpp
    ../tinDev/mainwindowdeveloper.h
    ../tinDev/mainwindowdeveloper.ui
    ../clientServer/chatroom.cpp
    ../clientServer/chatroom.h
    ../clientServer/chatroom.ui
    ../tinDev/svioglasi.cpp
    ../tinDev/svioglasi.h
    ../tinDev/svioglasi.ui
    ../clientServer/client.cpp
    ../clientServer/client.h
    ../clientServer/connection.cpp
    ../clientServer/connection.h
    ../clientServer/server.cpp
    ../clientServer/server.h
    ../clientServer/peermanager.cpp
    ../clientServer/peermanager.h
    ../tinDev/rewritethread.cpp
    ../tinDev/rewritethread.h
    ../tinDev/usernamemanager.cpp
    ../tinDev/usernamemanager.h
    ../tinDev/svioglasi.h
    ../tinDev/svioglasi.cpp
    ../tinDev/svioglasi.ui
)

add_executable(testovi ${TEST_SOURCES} ${CPP_SOURCES} catch.hpp)
target_link_libraries(testovi PRIVATE Qt${QT_VERSION_MAJOR}::Widgets)
target_link_libraries(testovi PRIVATE Qt6::Network)
target_link_libraries(testovi PRIVATE Qt6::Gui)
target_link_libraries(testovi PRIVATE Qt6::Test)
