#include "catch.hpp"

#include "../tinDev/izmenioglas.h"

TEST_CASE("IzmeniOglas", "[class]") {
  SECTION("Testiranje funkcije postaviId") {
    // Arrange
    IzmeniOglas *o = new IzmeniOglas();
    QString id = "225883";

    // Act
    o->postaviId(id);

    // Assert
    REQUIRE(o->getUi()->lbId->text() == "225883");

    delete o;
  }
}
