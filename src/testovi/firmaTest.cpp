#include "catch.hpp"

#include "../tinDev/firma.h"

TEST_CASE("Firma konstruktor", "[function]") {
  // Arrange
  QString ime = "Naziv Firme";
  QString adresa = "Adresa Firme";
  QString brTelefona = "123456789";
  QString email = "firmapost@example.com";
  QString username = "firmUsername";
  QString sifra = "firmPassword";

  // Act
  Firma *firma = new Firma(ime, adresa, brTelefona, email, username, sifra);

  // Assert
  REQUIRE(firma->ime() == ime);
  REQUIRE(firma->adresa() == adresa);
  REQUIRE(firma->brTelefona() == brTelefona);
  REQUIRE(firma->email() == email);
  REQUIRE(firma->username() == username);
  REQUIRE(firma->sifra() == sifra);

  delete firma;
}

TEST_CASE("Firma::toVariant", "[function]") {
  // Arrange
  QString ime = "Naziv Firme";
  QString adresa = "Adresa Firme";
  QString brTelefona = "123456789";
  QString email = "firmapost@example.com";
  QString username = "firmUsername";
  QString sifra = "firmPassword";

  Firma *firma = new Firma(ime, adresa, brTelefona, email, username, sifra);

  // Act
  QVariant result = firma->toVariant();

  // Assert
  QVariantMap resultMap = result.toMap();

  REQUIRE(resultMap["username"].toString() == username);
  REQUIRE(resultMap["sifra"].toString() == sifra);
  REQUIRE(resultMap["ime"].toString() == ime);
  REQUIRE(resultMap["adresa"].toString() == adresa);
  REQUIRE(resultMap["brTelefona"].toString() == brTelefona);
  REQUIRE(resultMap["email"].toString() == email);

  delete firma;
}

TEST_CASE("Firma::fromVariant", "[function]") {
  // Arrange
  QVariantMap map;
  map["username"] = "firmUsername";
  map["sifra"] = "firmPassword";
  map["ime"] = "Naziv Firme";
  map["adresa"] = "Adresa Firme";
  map["brTelefona"] = "123456789";
  map["email"] = "firmapost@example.com";

  Firma *firma = new Firma;

  // Act
  firma->fromVariant(map);

  // Assert
  REQUIRE(firma->username() == map["username"].toString());
  REQUIRE(firma->sifra() == map["sifra"].toString());
  REQUIRE(firma->ime() == map["ime"].toString());
  REQUIRE(firma->adresa() == map["adresa"].toString());
  REQUIRE(firma->brTelefona() == map["brTelefona"].toString());
  REQUIRE(firma->email() == map["email"].toString());
}
