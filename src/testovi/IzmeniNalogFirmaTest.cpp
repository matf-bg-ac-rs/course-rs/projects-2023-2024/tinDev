#include "catch.hpp"

#include "../tinDev/izmeninalogfirma.h"

TEST_CASE("IzmeniNalogFirma", "[class]") {
  SECTION("Testiranje funkcije postaviUsername") {
    // Arrange
    IzmeniNalogFirma *f = new IzmeniNalogFirma();
    QString username = "ivana";

    // Act
    f->postaviUsername(username);

    // Assert
    REQUIRE(f->getUi()->leUsername->text() == "ivana");

    delete f;
  }
  SECTION("Testiranje funkcije postaviPodatke") {
    // Arrange
    IzmeniNalogFirma *f = new IzmeniNalogFirma();
    QString sifra = "1234";
    QString nazivFirme = "Delta Holding";
    QString adresa = "Jagiceva 23";
    QString brTelefona = "064789456";
    QString eMail = "miskovic@gmail.com";

    // Act
    f->postaviPodatke(sifra, nazivFirme, adresa, brTelefona, eMail);

    // Assert
    REQUIRE(f->getUi()->leSifra->text() == "1234");
    REQUIRE(f->getUi()->leNazivFirme->text() == "Delta Holding");
    REQUIRE(f->getUi()->leAdresa->text() == "Jagiceva 23");
    REQUIRE(f->getUi()->leBrojTelefona->text() == "064789456");
    REQUIRE(f->getUi()->leEMail->text() == "miskovic@gmail.com");

    delete f;
  }
}
