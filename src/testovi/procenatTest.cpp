#include "../tinDev/procenat.h"
#include "../tinDev/match.h"
#include "catch.hpp"

TEST_CASE("Testiranje konstruktora sa ispravnim argumentima") {
  SECTION("Test kada su oba argumenta prosleđena") {
    // Arrange
    Developer devObj;
    Oglas oglasObj;
    // Act & Assert
    REQUIRE_NOTHROW(Procenat(&devObj, &oglasObj));
  }

  SECTION("Test kada bar jedan argument nije prosleđen") {
    SECTION("Kada ne postoji Developer") {
      // Arrange
      Developer devObj;
      // Act & Assert
      REQUIRE_THROWS_AS(Procenat(&devObj, nullptr), std::invalid_argument);
    }

    SECTION("Kada ne postoji Oglas") {
      // Arrange
      Oglas oglasObj;
      // Act & Assert
      REQUIRE_THROWS_AS(Procenat(nullptr, &oglasObj), std::invalid_argument);
    }

    SECTION("Kada ne postoje ni Developer ni Oglas") {
      // Arrange & Act & Assert
      REQUIRE_THROWS_AS(Procenat(nullptr, nullptr), std::invalid_argument);
    }
  }
}

TEST_CASE("Testiranje uporediPoklapanjeDevOglas") {
  // Arrange
  QString username = "dukiSuzuki";
  QString sifra = "bundevica";
  QString ime = "Dragan";
  QString prezime = "Kuric";
  QString brTelefona = "0653069596";
  char pol = 'm';
  QVector<QString> tehnologije = {"Git"};
  QMap<QString, unsigned int> brGodinaIskustva = {{"C", 2}};
  unsigned opsegPlate = 2000;
  NacinRada nacinRadaDev = NacinRada::Hybrid;
  QVector<QString> vestine = {"Backend", "Frontend"};
  QVector<QString> benefiti = {"deonice"};
  StrucnaSprema nivoObrazovanja = StrucnaSprema::SrednjaSkola;

  Developer *dev = new Developer(
       ime, prezime, pol, tehnologije, brGodinaIskustva, opsegPlate, nacinRadaDev, vestine, benefiti, nivoObrazovanja, username, sifra, brTelefona);

  QString nazivPozicije = "Software Developer";
  unsigned plata = 50000;
  NacinRada nacinRadaOglas = NacinRada::Hybrid;
  StrucnaSprema strucnaSprema = StrucnaSprema::DoktorskeStudije;
  QVector<QString> tehnologijeOglas = {"C++", "Qt"};
  QMap<QString, unsigned> brGodIskustva = {{"C++", 3}, {"Qt", 2}};
  QVector<QString> vestineOglas = {"Problem Solving", "Teamwork"};
  QVector<QString> benefitiOglas = {"Flexible hours", "Health insurance"};
  QString usernameFirma = "MyCompany";
  QString id = "12345";

  Oglas *oglas =
       new Oglas(nazivPozicije, plata, nacinRadaOglas, strucnaSprema, tehnologijeOglas, brGodIskustva, vestineOglas, benefitiOglas, usernameFirma, id);

  Procenat procenat(dev, oglas);

  SECTION("Procenat poklapanja koji treba da vrati ne sme biti negativan") {
    // Act
    double rezultat = procenat.uporediPoklapanjeDevOglas();
    // Assert
    REQUIRE(rezultat >= 0);
  }

  delete dev;
  delete oglas;
}
