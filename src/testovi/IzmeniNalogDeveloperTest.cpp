#include "catch.hpp"

#include "../tinDev/izmeninalogdeveloper.h"

TEST_CASE("IzmeniNalogDeveloper", "[class]") {
  SECTION("Testiranje funkcije postaviUsername") {
    // Arrange
    IzmeniNalogDeveloper *d = new IzmeniNalogDeveloper();
    QString username = "papa";

    // Act
    d->postaviUsername(username);

    // Assert
    REQUIRE(d->getUi()->leUsername->text() == "papa");

    delete d;
  }
  SECTION("Testiranje funkcije postaviPodatke") {
    // Arrange
    IzmeniNalogDeveloper *d = new IzmeniNalogDeveloper();
    QString sifra = "barbarizam";
    QString ime = "Barbara";
    QString prezime = "Bobak";
    QString brTelefona = "0605427307";
    char pol = 'z';
    // Act
    d->postaviOsnovnePodatke(ime, prezime, pol, sifra, brTelefona);

    // Assert
    REQUIRE(d->getUi()->leIme->text() == "Barbara");
    REQUIRE(d->getUi()->lePrezime->text() == "Bobak");
    REQUIRE(d->getUi()->rbZenski->isChecked());
    REQUIRE(d->getUi()->leSifra->text() == "barbarizam");
    REQUIRE(d->getUi()->leBrojTelefona->text() == "0605427307");

    delete d;
  }
}
