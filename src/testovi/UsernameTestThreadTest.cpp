#include "../tinDev/usernametestthread.h"
#include <catch.hpp>

TEST_CASE("UsernameTestThread::searchForDirectory", "[UsernameTestThread]") {
  // Arrange
  QDir rootDir(QDir::homePath());
  UsernameTestThread usernameThread("test");

  // Act
  QString result = usernameThread.searchForDirectory(rootDir);

  // Assert
  REQUIRE_FALSE(result.isEmpty());
  REQUIRE(result.endsWith("tinDev"));
  REQUIRE(QDir(result).exists());
}
