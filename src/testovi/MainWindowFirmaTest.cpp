#include "catch.hpp"

#include "../tinDev/mainwindowfirma.h"
#include "ui_mainwindowfirma.h"

TEST_CASE("MainWindowFirma", "[class]") {
  SECTION("Testiranje funkcije postaviUsername") {
    // Arrange
    MainWindowFirma *f = new MainWindowFirma();
    QString username = "ivana";

    // Act
    f->postaviUsername(username);

    // Assert
    REQUIRE(f->getUi()->leUsername->text() == "ivana");

    delete f;
  }
  SECTION("Testiranje funkcije ukloniPodatke") {
    // Arrange
    MainWindowFirma *f = new MainWindowFirma();

    // Act
    f->ukloniPodatke();

    // Assert
    REQUIRE(f->getUi()->lbImeIPrezime->text() == "");
    REQUIRE(f->getUi()->lbMinPlata->text() == "");
    REQUIRE(f->getUi()->lbNacinRada->text() == "");
    REQUIRE(f->getUi()->lbStrucnaSprema->text() == "");

    delete f;
  }
}
