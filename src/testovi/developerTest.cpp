#include "catch.hpp"

#include "../tinDev/developer.h"

TEST_CASE("Developer konstruktor", "[function]") {
  SECTION("Developer se ispravno pravi") {

    // Arrange
    QString username = "dukiSuzuki";
    QString sifra = "bundevica";
    QString ime = "Dragan";
    QString prezime = "Kuric";
    QString brTelefona = "0653069596";
    char pol = 'm';
    QVector<QString> tehnologije = {"Git"};
    QMap<QString, unsigned int> brGodinaIskustva = {{"C", 2}};
    unsigned opsegPlate = 2000;
    NacinRada nacinRada = NacinRada::Hybrid;
    QVector<QString> vestine = {"Backend", "Frontend"};
    QVector<QString> benefiti = {"deonice"};
    StrucnaSprema nivoObrazovanja = StrucnaSprema::SrednjaSkola;

    // Act
    Developer *d = new Developer(
         ime, prezime, pol, tehnologije, brGodinaIskustva, opsegPlate, nacinRada, vestine, benefiti, nivoObrazovanja, username, sifra, brTelefona);

    // Assert
    REQUIRE(d->username() == username);
    REQUIRE(d->sifra() == sifra);
    REQUIRE(d->ime() == ime);
    REQUIRE(d->prezime() == prezime);
    REQUIRE(d->pol() == pol);
    REQUIRE(d->tehnologije() == tehnologije);
    REQUIRE(d->brGodinaIskustva() == brGodinaIskustva);
    REQUIRE(d->opsegPlate() == opsegPlate);
    REQUIRE(d->nacinRada() == nacinRada);
    REQUIRE(d->vestine() == vestine);
    REQUIRE(d->benefiti() == benefiti);
    REQUIRE(d->nivoObrazovanja() == nivoObrazovanja);
    REQUIRE(d->brTelefona() == brTelefona);

    delete d;
  }
}

TEST_CASE("Developer geteri", "[get]") {
  // Arrange
  QString username = "ivana";
  QString sifra = "anavolimilovana";
  QString ime = "Goran";
  QString prezime = "Bogdan";
  QString brTelefona = "061222222";
  char pol = 'm';
  QVector<QString> tehnologije = {"SQL"};
  QMap<QString, unsigned int> brGodinaIskustva = {{"Python", 12}};
  unsigned opsegPlate = 1836;
  NacinRada nacinRada = NacinRada::Office;
  QVector<QString> vestine = {"Data Analyst", "Scrum Master"};
  QVector<QString> benefiti = {"13. plata", "deonice"};
  StrucnaSprema nivoObrazovanja = StrucnaSprema::DoktorskeStudije;
  Developer *d =
       new Developer(ime, prezime, pol, tehnologije, brGodinaIskustva, opsegPlate, nacinRada, vestine, benefiti, nivoObrazovanja, username, sifra, brTelefona);

  // Act
  QString usernameGet = d->username();
  QString sifraGet = d->sifra();
  QString imeGet = d->ime();
  QString prezimeGet = d->prezime();
  QString telefonGet = d->brTelefona();
  char polGet = d->pol();
  QVector<QString> tehGet = d->tehnologije();
  QMap<QString, unsigned int> jeziciGet = d->brGodinaIskustva();
  unsigned int plataGet = d->opsegPlate();
  NacinRada radGet = d->nacinRada();
  QVector<QString> vestineGet = d->vestine();
  QVector<QString> benefitiGet = d->benefiti();
  StrucnaSprema obrazovanjeGet = d->nivoObrazovanja();

  // Assert
  REQUIRE(usernameGet == username);
  REQUIRE(sifraGet == sifra);
  REQUIRE(imeGet == ime);
  REQUIRE(prezimeGet == prezime);
  REQUIRE(polGet == pol);
  REQUIRE(tehGet == tehnologije);
  REQUIRE(jeziciGet == brGodinaIskustva);
  REQUIRE(plataGet == opsegPlate);
  REQUIRE(radGet == nacinRada);
  REQUIRE(vestineGet == vestine);
  REQUIRE(benefitiGet == benefiti);
  REQUIRE(obrazovanjeGet == nivoObrazovanja);
  REQUIRE(telefonGet == brTelefona);

  delete d;
}

TEST_CASE("Developer::toVariant", "[function]") {
  SECTION("Testiranje funkcije toVariant") {

    // Arrange
    QString username = "dukiSuzuki";
    QString sifra = "bundevica";
    QString ime = "Dragan";
    QString prezime = "Kuric";
    QString brTelefona = "0653069596";
    char pol = 'm';
    QVector<QString> tehnologije = {"Git"};
    QMap<QString, unsigned int> brGodinaIskustva = {{"C", 2}};
    unsigned opsegPlate = 2000;
    NacinRada nacinRada = NacinRada::Hybrid;
    QVector<QString> vestine = {"Backend", "Frontend"};
    QVector<QString> benefiti = {"deonice"};
    StrucnaSprema nivoObrazovanja = StrucnaSprema::SrednjaSkola;

    Developer *d = new Developer(
         ime, prezime, pol, tehnologije, brGodinaIskustva, opsegPlate, nacinRada, vestine, benefiti, nivoObrazovanja, username, sifra, brTelefona);

    // Act
    QVariant result = d->toVariant();

    // Assert
    QVariantMap resultMap = result.toMap();
    REQUIRE(resultMap["username"].toString() == username);
    REQUIRE(resultMap["sifra"].toString() == sifra);
    REQUIRE(resultMap["ime"].toString() == ime);
    REQUIRE(resultMap["prezime"].toString() == prezime);
    REQUIRE(resultMap["brTelefona"].toString() == brTelefona);
    REQUIRE(resultMap["pol"].toString() == QString(pol));
    REQUIRE(resultMap["tehnologije"].toStringList() == tehnologije.toList());
    REQUIRE(resultMap["opsegPlate"].toUInt() == opsegPlate);
    REQUIRE(resultMap["nacinRada"].toInt() == static_cast<int>(nacinRada));
    REQUIRE(resultMap["vestine"].toStringList() == vestine.toList());
    REQUIRE(resultMap["benefiti"].toStringList() == benefiti.toList());
    REQUIRE(resultMap["nivoObrazovanja"].toInt() == static_cast<int>(nivoObrazovanja));
    delete d;
  }
}

TEST_CASE("Developer::fromVariant", "[function]") {

  // Arrange
  QVariantMap map;
  map["username"] = "dukiSuzuki";
  map["sifra"] = "bundevica";
  map["ime"] = "Dragan";
  map["prezime"] = "Kuric";
  map["brTelefona"] = "0653069596";
  map["pol"] = "m";
  map["tehnologije"] = QVariant::fromValue<QVector<QString>>({"Git"});
  map["brGodinaIskustva"] = QVariant::fromValue<QMap<QString, unsigned>>({{"C", 2}});
  map["opsegPlate"] = 2000;
  map["nacinRada"] = static_cast<int>(NacinRada::Hybrid);
  map["vestine"] = QVariant::fromValue<QVector<QString>>({"Backend", "Frontend"});
  map["benefiti"] = QVariant::fromValue<QVector<QString>>({"deonice"});
  map["nivoObrazovanja"] = static_cast<int>(StrucnaSprema::SrednjaSkola);

  Developer *d = new Developer();

  // Act
  d->fromVariant(map);

  // Assert
  REQUIRE(d->username() == "dukiSuzuki");
  REQUIRE(d->sifra() == "bundevica");
  REQUIRE(d->ime() == "Dragan");
  REQUIRE(d->prezime() == "Kuric");
  REQUIRE(d->brTelefona() == "0653069596");
  REQUIRE(d->pol() == 'm');
  REQUIRE(d->tehnologije() == QVector<QString>({"Git"}));
  REQUIRE(d->brGodinaIskustva() == QMap<QString, unsigned>({{"C", 2}}));
  REQUIRE(d->opsegPlate() == 2000);
  REQUIRE(d->nacinRada() == NacinRada::Hybrid);
  REQUIRE(d->vestine() == QVector<QString>({"Backend", "Frontend"}));
  REQUIRE(d->benefiti() == QVector<QString>({"deonice"}));
  REQUIRE(d->nivoObrazovanja() == StrucnaSprema::SrednjaSkola);
}

TEST_CASE("Developer::stepenStrucneSpremeStr", "[function]") {
  SECTION("Srednja skola") {

    // Arrange
    QString username = "dukiSuzuki";
    QString sifra = "bundevica";
    QString ime = "Dragan";
    QString prezime = "Kuric";
    QString brTelefona = "0653069596";
    char pol = 'm';
    QVector<QString> tehnologije = {"Git"};
    QMap<QString, unsigned int> brGodinaIskustva = {{"C", 2}};
    unsigned opsegPlate = 2000;
    NacinRada nacinRada = NacinRada::Hybrid;
    QVector<QString> vestine = {"Backend", "Frontend"};
    QVector<QString> benefiti = {"deonice"};
    StrucnaSprema nivoObrazovanja = StrucnaSprema::SrednjaSkola;

    Developer *d = new Developer(
         ime, prezime, pol, tehnologije, brGodinaIskustva, opsegPlate, nacinRada, vestine, benefiti, nivoObrazovanja, username, sifra, brTelefona);

    // Act
    QString rezultat = d->stepenStrucneSpremeStr();

    // Assert
    REQUIRE("Srednja skola" == rezultat);
    delete d;
  }

  SECTION("Osnovne studije") {

    // Arrange
    QString username = "dukiSuzuki";
    QString sifra = "bundevica";
    QString ime = "Dragan";
    QString prezime = "Kuric";
    QString brTelefona = "0653069596";
    char pol = 'm';
    QVector<QString> tehnologije = {"Git"};
    QMap<QString, unsigned int> brGodinaIskustva = {{"C", 2}};
    unsigned opsegPlate = 2000;
    NacinRada nacinRada = NacinRada::Hybrid;
    QVector<QString> vestine = {"Backend", "Frontend"};
    QVector<QString> benefiti = {"deonice"};
    StrucnaSprema nivoObrazovanja = StrucnaSprema::OsnovneStudije;

    Developer *d = new Developer(
         ime, prezime, pol, tehnologije, brGodinaIskustva, opsegPlate, nacinRada, vestine, benefiti, nivoObrazovanja, username, sifra, brTelefona);

    // Act
    QString rezultat = d->stepenStrucneSpremeStr();

    // Assert
    REQUIRE("Osnovne studije" == rezultat);
    delete d;
  }

  SECTION("Master studije") {

    // Arrange
    QString username = "dukiSuzuki";
    QString sifra = "bundevica";
    QString ime = "Dragan";
    QString prezime = "Kuric";
    QString brTelefona = "0653069596";
    char pol = 'm';
    QVector<QString> tehnologije = {"Git"};
    QMap<QString, unsigned int> brGodinaIskustva = {{"C", 2}};
    unsigned opsegPlate = 2000;
    NacinRada nacinRada = NacinRada::Hybrid;
    QVector<QString> vestine = {"Backend", "Frontend"};
    QVector<QString> benefiti = {"deonice"};
    StrucnaSprema nivoObrazovanja = StrucnaSprema::MasterStudije;

    Developer *d = new Developer(
         ime, prezime, pol, tehnologije, brGodinaIskustva, opsegPlate, nacinRada, vestine, benefiti, nivoObrazovanja, username, sifra, brTelefona);

    // Act
    QString rezultat = d->stepenStrucneSpremeStr();

    // Assert
    REQUIRE("Master studije" == rezultat);
    delete d;
  }

  SECTION("Doktorske studije") {

    // Arrange
    QString username = "dukiSuzuki";
    QString sifra = "bundevica";
    QString ime = "Dragan";
    QString prezime = "Kuric";
    QString brTelefona = "0653069596";
    char pol = 'm';
    QVector<QString> tehnologije = {"Git"};
    QMap<QString, unsigned int> brGodinaIskustva = {{"C", 2}};
    unsigned opsegPlate = 2000;
    NacinRada nacinRada = NacinRada::Hybrid;
    QVector<QString> vestine = {"Backend", "Frontend"};
    QVector<QString> benefiti = {"deonice"};
    StrucnaSprema nivoObrazovanja = StrucnaSprema::DoktorskeStudije;

    Developer *d = new Developer(
         ime, prezime, pol, tehnologije, brGodinaIskustva, opsegPlate, nacinRada, vestine, benefiti, nivoObrazovanja, username, sifra, brTelefona);

    // Act
    QString rezultat = d->stepenStrucneSpremeStr();

    // Assert
    REQUIRE("Doktorske studije" == rezultat);
    delete d;
  }
}

TEST_CASE("Developer::nacinRadaStr", "[function]") {
  SECTION("Hybrid") {

    // Arrange
    QString username = "dukiSuzuki";
    QString sifra = "bundevica";
    QString ime = "Dragan";
    QString prezime = "Kuric";
    QString brTelefona = "0653069596";
    char pol = 'm';
    QVector<QString> tehnologije = {"Git"};
    QMap<QString, unsigned int> brGodinaIskustva = {{"C", 2}};
    unsigned opsegPlate = 2000;
    NacinRada nacinRada = NacinRada::Hybrid;
    QVector<QString> vestine = {"Backend", "Frontend"};
    QVector<QString> benefiti = {"deonice"};
    StrucnaSprema nivoObrazovanja = StrucnaSprema::SrednjaSkola;

    Developer *d = new Developer(
         ime, prezime, pol, tehnologije, brGodinaIskustva, opsegPlate, nacinRada, vestine, benefiti, nivoObrazovanja, username, sifra, brTelefona);

    // Act
    QString rezultat = d->nacinRadaStr();

    // Assert
    REQUIRE("Hybrid" == rezultat);
    delete d;
  }

  SECTION("Office") {

    // Arrange
    QString username = "dukiSuzuki";
    QString sifra = "bundevica";
    QString ime = "Dragan";
    QString prezime = "Kuric";
    QString brTelefona = "0653069596";
    char pol = 'm';
    QVector<QString> tehnologije = {"Git"};
    QMap<QString, unsigned int> brGodinaIskustva = {{"C", 2}};
    unsigned opsegPlate = 2000;
    NacinRada nacinRada = NacinRada::Office;
    QVector<QString> vestine = {"Backend", "Frontend"};
    QVector<QString> benefiti = {"deonice"};
    StrucnaSprema nivoObrazovanja = StrucnaSprema::OsnovneStudije;

    Developer *d = new Developer(
         ime, prezime, pol, tehnologije, brGodinaIskustva, opsegPlate, nacinRada, vestine, benefiti, nivoObrazovanja, username, sifra, brTelefona);

    // Act
    QString rezultat = d->nacinRadaStr();

    // Assert
    REQUIRE("Office" == rezultat);
    delete d;
  }

  SECTION("Remote") {

    // Arrange
    QString username = "dukiSuzuki";
    QString sifra = "bundevica";
    QString ime = "Dragan";
    QString prezime = "Kuric";
    QString brTelefona = "0653069596";
    char pol = 'm';
    QVector<QString> tehnologije = {"Git"};
    QMap<QString, unsigned int> brGodinaIskustva = {{"C", 2}};
    unsigned opsegPlate = 2000;
    NacinRada nacinRada = NacinRada::Remote;
    QVector<QString> vestine = {"Backend", "Frontend"};
    QVector<QString> benefiti = {"deonice"};
    StrucnaSprema nivoObrazovanja = StrucnaSprema::MasterStudije;

    Developer *d = new Developer(
         ime, prezime, pol, tehnologije, brGodinaIskustva, opsegPlate, nacinRada, vestine, benefiti, nivoObrazovanja, username, sifra, brTelefona);

    // Act
    QString rezultat = d->nacinRadaStr();

    // Assert
    REQUIRE("Remote" == rezultat);
    delete d;
  }
}
