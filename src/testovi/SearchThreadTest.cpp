#include "../tinDev/searchthread.h"
#include <catch.hpp>

TEST_CASE("SearchThread::searchForDirectory", "[function]") {
  // Arrange
  QDir rootDir(QDir::homePath());
  SearchThread searchThread("test");

  // Act
  QString result = searchThread.searchForDirectory(rootDir);

  // Assert
  REQUIRE_FALSE(result.isEmpty());
  REQUIRE(result.endsWith("tinDev"));
  REQUIRE(QDir(result).exists());
}
