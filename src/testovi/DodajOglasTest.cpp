#include "catch.hpp"

#include "../tinDev/dodajoglas.h"

TEST_CASE("DodajOglas", "[class]") {
  SECTION("Testiranje funkcije postaviUsername") {
    // Arrange
    DodajOglas *o = new DodajOglas();
    QString username = "nuccibebo";

    // Act
    o->postaviUsername(username);

    // Assert
    REQUIRE(o->getUi()->lbUsernameFirma->text() == "nuccibebo");

    delete o;
  }
}
