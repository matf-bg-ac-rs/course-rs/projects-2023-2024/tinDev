#include "../tinDev/loginthread.h"
#include <catch.hpp>

TEST_CASE("LoginThread::searchForDirectory", "[LoginThread]") {
  // Arrange
  QDir rootDir(QDir::homePath());
  loginthread logIn("test");

  // Act
  QString result = logIn.searchForDirectory(rootDir);

  // Assert
  REQUIRE_FALSE(result.isEmpty());
  REQUIRE(result.endsWith("tinDev"));
  REQUIRE(QDir(result).exists());
}
