#include "../tinDev/match.h"
#include "catch.hpp"

TEST_CASE("Match konstruktor", "[function]") {
  SECTION("Imam sve argumente") {
    // Arrange
    QString putanja = "nekaPutanjaDoFajla";
    QString targetUsername = "baja123";
    QString imeFirmeDevelopera = "imeFirme";

    // Act & Assert
    REQUIRE_NOTHROW(Match(putanja, targetUsername, imeFirmeDevelopera));
  }

  SECTION("Fali mi bar jedan argument") {
    SECTION("Nemam argument putanja") {
      // Arrange
      QString putanja;
      QString targetUsername = "baja123";
      QString imeFirmeDevelopera = "imeFirme";

      // Act & Assert
      REQUIRE_THROWS_AS(Match(putanja, targetUsername, imeFirmeDevelopera), std::invalid_argument);
    }

    SECTION("Nemam argument targetUsername") {
      // Arrange
      QString putanja = "nekaPutanjaDoFajla";
      QString targetUsername;
      QString imeFirmeDevelopera = "imeFirme";

      // Act & Assert
      REQUIRE_THROWS_AS(Match(putanja, targetUsername, imeFirmeDevelopera), std::invalid_argument);
    }

    SECTION("Nemam argument imeFirmeDevelopera") {
      // Arrange
      QString putanja = "nekaPutanjaDoFajla";
      QString targetUsername = "baja123";
      QString imeFirmeDevelopera;

      // Act & Assert
      REQUIRE_THROWS_AS(Match(putanja, targetUsername, imeFirmeDevelopera), std::invalid_argument);
    }
  }
}
