TEMPLATE = app
QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++20

SOURCES += \
    DodajOglasTest.cpp \
    IzmeniNalogDeveloperTest.cpp \
    IzmeniNalogFirmaTest.cpp \
    IzmeniOglasTest.cpp \
    LoginThreadTest.cpp \
    MainWIndowDeveloperTest.cpp \
    MainWindowFirmaTest.cpp \
    ReadFileThreadTest.cpp \
    SearchThreadTest.cpp \
    SviOglasiTest.cpp \
    UsernameTestThreadTest.cpp \
    developerTest.cpp \
    firmaTest.cpp \
    main.cpp \
    ../tinDev/developer.cpp \
    ../tinDev/rewritethread.cpp \
    ../tinDev/korisnik.cpp \
    ../tinDev/dodajoglas.cpp \
    ../tinDev/firma.cpp \
    ../tinDev/izmeninalogdeveloper.cpp \
    ../tinDev/izmeninalogfirma.cpp \
    ../tinDev/izmenioglas.cpp \
    ../tinDev/login.cpp \
    ../tinDev/loginthread.cpp \
    ../tinDev/mainwindowdeveloper.cpp \
    ../tinDev/mainwindowfirma.cpp \
    ../tinDev/match.cpp \
    ../tinDev/napravinalogdeveloper.cpp \
    ../tinDev/napravinalogfirma.cpp \
    ../tinDev/oglas.cpp \
    ../tinDev/procenat.cpp \
    ../tinDev/readfilethread.cpp \
    ../tinDev/searchthread.cpp \
    ../tinDev/usernamemanager.cpp \
    ../tinDev/usernametestthread.cpp \
    ../serialization/jsonserializer.cpp \
    ../serialization/serializable.cpp \
    ../serialization/serializer.cpp \
    ../clientServer/chatroom.cpp \
    ../clientServer/client.cpp \
    ../clientServer/connection.cpp \
    ../clientServer/peermanager.cpp \
    ../clientServer/server.cpp \
    ../tinDev/svioglasi.cpp \
    matchTest.cpp \
    oglasTest.cpp \
    procenatTest.cpp


HEADERS += \
    catch.hpp \
    ../tinDev/developer.h \
    ../tinDev/korisnik.h \
    ../tinDev/dodajoglas.h \
    ../tinDev/enums.h \
    ../tinDev/firma.h \
    ../tinDev/rewritethread.h \
    ../tinDev/iusernameprovider.h \
    ../tinDev/izmeninalogdeveloper.h \
    ../tinDev/izmeninalogfirma.h \
    ../tinDev/izmenioglas.h \
    ../tinDev/login.h \
    ../tinDev/loginthread.h \
    ../tinDev/mainwindowdeveloper.h \
    ../tinDev/mainwindowfirma.h \
    ../tinDev/match.h \
    ../tinDev/napravinalogdeveloper.h \
    ../tinDev/napravinalogfirma.h \
    ../tinDev/obradainterfejsa.h \
    ../tinDev/oglas.h \
    ../tinDev/procenat.h \
    ../tinDev/readfilethread.h \
    ../tinDev/searchthread.h \
    ../tinDev/svioglasi.h \
    ../tinDev/usernamemanager.h \
    ../tinDev/usernametestthread.h \
    ../serialization/jsonserializer.h \
    ../serialization/serializable.h \
    ../serialization/serializer.h \
    ../clientServer/chatroom.h \
    ../clientServer/client.h \
    ../clientServer/connection.h \
    ../clientServer/peermanager.h \
    ../clientServer/server.h

FORMS += \
    ../tinDev/dodajoglas.ui \
    ../tinDev/izmeninalogdeveloper.ui \
    ../tinDev/izmeninalogfirma.ui \
    ../tinDev/izmenioglas.ui \
    ../tinDev/login.ui \
    ../tinDev/mainwindowdeveloper.ui \
    ../tinDev/mainwindowfirma.ui \
    ../tinDev/napravinalogdeveloper.ui \
    ../tinDev/napravinalogfirma.ui \
    ../tinDev/svioglasi.ui \
    ../clientServer/chatroom.ui




#TARGET = testovi

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
