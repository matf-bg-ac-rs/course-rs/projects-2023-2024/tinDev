#include "../tinDev/readfilethread.h"
#include <catch.hpp>

TEST_CASE("ReadFileThread::searchForDirectory", "[ReadFileThread]") {
  // Arrange
  QDir rootDir(QDir::homePath());
  ReadFileThread readFileThread("test");

  // Act
  QString result = readFileThread.searchForDirectory(rootDir);

  // Assert
  REQUIRE_FALSE(result.isEmpty());
  REQUIRE(result.endsWith("tinDev"));
  REQUIRE(QDir(result).exists());
}
