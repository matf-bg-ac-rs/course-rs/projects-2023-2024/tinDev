#include "catch.hpp"

#include "../tinDev/svioglasi.h"

TEST_CASE("SviOglasi", "[class]") {
  SECTION("Testiranje funkcije postaviUsername") {
    // Arrange
    SviOglasi *o = new SviOglasi();
    QString username = "brrreskvica";

    // Act
    o->postaviUsername(username);

    // Assert
    REQUIRE(o->getUi()->lbImeFirme->text() == "brrreskvica");

    delete o;
  }
}
