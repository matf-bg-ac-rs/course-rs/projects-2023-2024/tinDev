#pragma once
#include "serializable.h"

class Serializer {
public:
  Serializer();
  virtual ~Serializer() = default;

  virtual void save(const Serializable &serializable, const QString &filepath, const QString &rootName = "") = 0;
  virtual void load(Serializable &serializable, const QString &filepath) = 0;
};
