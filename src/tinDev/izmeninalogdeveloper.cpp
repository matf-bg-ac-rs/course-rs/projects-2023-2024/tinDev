#include "izmeninalogdeveloper.h"
#include "../serialization/jsonserializer.h"
#include "obradainterfejsa.h"
#include "searchthread.h"
#include "ui_izmeninalogdeveloper.h"
#include <QDir>

IzmeniNalogDeveloper::IzmeniNalogDeveloper(QWidget *parent) : QWidget(parent), ui(new Ui::IzmeniNalogDeveloper) {
  ui->setupUi(this);
  connect(ui->pbNastaviDalje, &QPushButton::clicked, this, &IzmeniNalogDeveloper::kliknutoNastaviKaTehnologijama);
  connect(ui->pbNastaviDalje_2, &QPushButton::clicked, this, &IzmeniNalogDeveloper::kliknutoNastaviKaIskustvu);
  connect(ui->pbNastaviDalje_3, &QPushButton::clicked, this, &IzmeniNalogDeveloper::kliknutoNastaviKaPlati);
  connect(ui->pbNastaviDalje_4, &QPushButton::clicked, this, &IzmeniNalogDeveloper::kliknutoNastaviKaVestinama);
  connect(ui->pbNastaviDalje_5, &QPushButton::clicked, this, &IzmeniNalogDeveloper::kliknutoNastaviKaBenefitima);

  connect(ui->pbNazad, &QPushButton::clicked, this, &IzmeniNalogDeveloper::kliknutoPonistiIzmene);
  connect(ui->pbNazad_2, &QPushButton::clicked, this, &IzmeniNalogDeveloper::NazadNaOsnovnePodatke);
  connect(ui->pbNazad_3, &QPushButton::clicked, this, &IzmeniNalogDeveloper::NazadNaTehnologije);
  connect(ui->pbNazad_4, &QPushButton::clicked, this, &IzmeniNalogDeveloper::NazadNaIskustvo);
  connect(ui->pbNazad_5, &QPushButton::clicked, this, &IzmeniNalogDeveloper::NazadNaPlatu);
  connect(ui->pbNazad_6, &QPushButton::clicked, this, &IzmeniNalogDeveloper::NazadNaIskustvo);

  connect(ui->pbPotvrdiIzmene, &QPushButton::clicked, this, &IzmeniNalogDeveloper::kliknutoPotvrdiIzmene);
  connect(ui->pbPonisti, &QPushButton::clicked, this, &IzmeniNalogDeveloper::kliknutoPonistiIzmene);
}

IzmeniNalogDeveloper::~IzmeniNalogDeveloper() { delete ui; }

Ui::IzmeniNalogDeveloper *IzmeniNalogDeveloper::getUi() { return ui; }

void IzmeniNalogDeveloper::kliknutoNastaviKaTehnologijama() {
  const std::unique_ptr<ObradaInterfejsa<Ui::IzmeniNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::IzmeniNalogDeveloper>);
  if (!obrada->ispravniPodaci(ui)) {
    obrada->upozorenje("Morate uneti podatke u sva polja!");
  } else if (!obrada->ispravanBrojTelefona(ui)) {
    ui->leBrojTelefona->setText("Neispravan format");
  } else if (!obrada->izabranPol(ui)) {
    obrada->upozorenje("Morate izabrati pol!");
  } else {
    ui->stackedWidget->setCurrentIndex(1);
  }
}

void IzmeniNalogDeveloper::kliknutoNastaviKaIskustvu() {
  const std::unique_ptr<ObradaInterfejsa<Ui::IzmeniNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::IzmeniNalogDeveloper>);
  const QVector<QString> v = obrada->dohvatiTehnologije(ui);

  (v.size() < 3) ? obrada->upozorenje("Morate izabrati najmanje 3 tehnologije!") : ui->stackedWidget->setCurrentIndex(2);
}

void IzmeniNalogDeveloper::kliknutoNastaviKaPlati() {
  const std::unique_ptr<ObradaInterfejsa<Ui::IzmeniNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::IzmeniNalogDeveloper>);
  const QMap<QString, unsigned> m = obrada->dohvatiBrojGodinaIskustva(ui);

  (m.empty()) ? obrada->upozorenje("Morate imati iskustva u barem jednom jeziku!") : ui->stackedWidget->setCurrentIndex(3);
}

void IzmeniNalogDeveloper::kliknutoNastaviKaVestinama() {
  const std::unique_ptr<ObradaInterfejsa<Ui::IzmeniNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::IzmeniNalogDeveloper>);
  if (!obrada->izabranNacinRada(ui)) {
    obrada->upozorenje("Morate izabrati nacin rada!");
  } else if (!obrada->izabranaStrucnaSprema(ui)) {
    obrada->upozorenje("Morate izabrati stepen stucne spreme!");
  } else if (!obrada->ispravnaPlata(ui)) {
    obrada->upozorenje("Neispravan format plate!");
  } else {
    ui->stackedWidget->setCurrentIndex(4);
  }
}

void IzmeniNalogDeveloper::kliknutoNastaviKaBenefitima() {
  const std::unique_ptr<ObradaInterfejsa<Ui::IzmeniNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::IzmeniNalogDeveloper>);
  const QVector<QString> v = obrada->dohvatiVestine(ui);

  (v.empty()) ? obrada->upozorenje("Morate izabrati vestinu!") : ui->stackedWidget->setCurrentIndex(5);
}

void IzmeniNalogDeveloper::NazadNaOsnovnePodatke() { ui->stackedWidget->setCurrentIndex(0); }

void IzmeniNalogDeveloper::NazadNaTehnologije() { ui->stackedWidget->setCurrentIndex(1); }

void IzmeniNalogDeveloper::NazadNaIskustvo() { ui->stackedWidget->setCurrentIndex(2); }

void IzmeniNalogDeveloper::NazadNaPlatu() { ui->stackedWidget->setCurrentIndex(3); }
void IzmeniNalogDeveloper::NazadNaVestine() { ui->stackedWidget->setCurrentIndex(4); }

void IzmeniNalogDeveloper::postaviUsername(const QString &username) { ui->leUsername->setText(username); }

void IzmeniNalogDeveloper::postaviOsnovnePodatke(const QString &ime, const QString &prezime, char pol, const QString &sifra, const QString &brTelefona) {
  ui->leSifra->setText(sifra);
  ui->leIme->setText(ime);
  ui->lePrezime->setText(prezime);

  (pol == 'm') ? ui->rbMuski->setChecked(true) : ui->rbZenski->setChecked(true);

  ui->leBrojTelefona->setText(brTelefona);
}

void IzmeniNalogDeveloper::postaviTehnologije(const QVector<QString> &tehnologije) {
  int n = ui->glTehnologije->rowCount();
  int m = ui->glTehnologije->columnCount();

  for (int row = 0; row < n; row++) {
    for (int col = 0; col < m; col++) {
      QLayoutItem *item = ui->glTehnologije->itemAtPosition(row, col);

      if (item != nullptr) {
        QLayout *horizontalLayout = item->layout();
        if (horizontalLayout != nullptr) {
          for (int i = 0; i < horizontalLayout->count(); ++i) {
            QWidget *widget = horizontalLayout->itemAt(i)->widget();
            if (widget != nullptr) {

              if (QCheckBox *checkBox = qobject_cast<QCheckBox *>(widget)) {
                if (tehnologije.contains(checkBox->text())) {
                  checkBox->setChecked(true);
                }
              }
            }
          }
        }
      }
    }
  }
}

void IzmeniNalogDeveloper::postaviBrojGodinaIskustva(const QMap<QString, unsigned> &iskustvo) {
  QGridLayout *gridLayout = ui->glIskustvo;

  int n = gridLayout->rowCount();
  int m = gridLayout->columnCount();

  for (int row = 0; row < n; ++row) {
    for (int col = 0; col < m; ++col) {
      QLayoutItem *item = gridLayout->itemAtPosition(row, col);

      if (item != nullptr) {
        QLayout *horizontalLayout = item->layout();
        if (horizontalLayout != nullptr) {
          unsigned broj = 0;

          for (int i = 0; i < horizontalLayout->count(); ++i) {
            QWidget *widget = horizontalLayout->itemAt(i)->widget();
            if (widget != nullptr) {
              if (QLabel *label = qobject_cast<QLabel *>(widget)) {
                QString tekstLabela = label->text();

                if (!tekstLabela.isEmpty()) {

                  auto it = iskustvo.find(tekstLabela);
                  if (it != iskustvo.end()) {
                    broj = it.value();
                  }
                }
              }
              if (QSpinBox *spinBox = qobject_cast<QSpinBox *>(widget)) {
                spinBox->setValue(broj);
              }
            }
          }
        }
      }
    }
  }
}

void IzmeniNalogDeveloper::postaviPlatu(const unsigned &plata) { ui->lePlata->setText(QString::number(plata)); }

void IzmeniNalogDeveloper::postaviNacinRada(const NacinRada nacinRada) {
  QVBoxLayout *vl = ui->vlNacinRada;
  auto n = vl->count();

  for (int i = 0; i < n; ++i) {
    QWidget *widget = vl->itemAt(i)->widget();
    if (widget != nullptr) {
      if (QRadioButton *radioButtons = qobject_cast<QRadioButton *>(widget)) {
        if (nacinRada == NacinRada::Remote && radioButtons->text() == "Remote") {
          radioButtons->setChecked(true);
          return;
        } else if (nacinRada == NacinRada::Office && radioButtons->text() == "Office") {
          radioButtons->setChecked(true);
          return;
        } else if (nacinRada == NacinRada::Hybrid && radioButtons->text() == "Hybrid") {
          radioButtons->setChecked(true);
          return;
        }
      }
    }
  }
}

void IzmeniNalogDeveloper::postaviStrucnuSpremu(const StrucnaSprema strucnaSprema) {
  QVBoxLayout *vl = ui->vlObrazovanje;
  auto n = vl->count();

  for (int i = 0; i < n; ++i) {
    QWidget *widget = vl->itemAt(i)->widget();
    if (widget != nullptr) {
      if (QRadioButton *radioButtons = qobject_cast<QRadioButton *>(widget)) {
        if (strucnaSprema == StrucnaSprema::SrednjaSkola && radioButtons->text() == "Srednja škola") {
          radioButtons->setChecked(true);
          return;
        } else if (strucnaSprema == StrucnaSprema::OsnovneStudije && radioButtons->text() == "Fakultet") {
          radioButtons->setChecked(true);
          return;
        } else if (strucnaSprema == StrucnaSprema::MasterStudije && radioButtons->text() == "Master") {
          radioButtons->setChecked(true);
          return;
        } else if (strucnaSprema == StrucnaSprema::DoktorskeStudije && radioButtons->text() == "Doktorat") {
          radioButtons->setChecked(true);
          return;
        }
      }
    }
  }
}

void IzmeniNalogDeveloper::postaviVestine(const QVector<QString> &vestine) {
  int n = ui->glVestine->rowCount();
  int m = ui->glVestine->columnCount();

  for (int row = 0; row < n; row++) {
    for (int col = 0; col < m; col++) {
      QLayoutItem *item = ui->glVestine->itemAtPosition(row, col);

      if (item != nullptr) {
        QLayout *horizontalLayout = item->layout();
        if (horizontalLayout != nullptr) {
          for (int i = 0; i < horizontalLayout->count(); ++i) {
            QWidget *widget = horizontalLayout->itemAt(i)->widget();
            if (widget != nullptr) {

              if (QCheckBox *checkBox = qobject_cast<QCheckBox *>(widget)) {
                if (vestine.contains(checkBox->text())) {
                  checkBox->setChecked(true);
                }
              }
            }
          }
        }
      }
    }
  }
}

void IzmeniNalogDeveloper::postaviBenefite(const QVector<QString> &benefiti) {
  int n = ui->glBenefiti->rowCount();
  int m = ui->glBenefiti->columnCount();

  for (int row = 0; row < n; row++) {
    for (int col = 0; col < m; col++) {
      QLayoutItem *item = ui->glBenefiti->itemAtPosition(row, col);

      if (item != nullptr) {
        QLayout *horizontalLayout = item->layout();
        if (horizontalLayout != nullptr) {
          for (int i = 0; i < horizontalLayout->count(); ++i) {
            QWidget *widget = horizontalLayout->itemAt(i)->widget();
            if (widget != nullptr) {

              if (QCheckBox *checkBox = qobject_cast<QCheckBox *>(widget)) {
                if (benefiti.contains(checkBox->text())) {
                  checkBox->setChecked(true);
                }
              }
            }
          }
        }
      }
    }
  }
}

void IzmeniNalogDeveloper::kliknutoPotvrdiIzmene() {
  zavrsiIzmene();
  emit windowLoaded();
  this->close();
}

void IzmeniNalogDeveloper::kliknutoPonistiIzmene() { this->close(); }

void IzmeniNalogDeveloper::pronadjenDirektorijumDeveloper(const QString &dir) {
  Developer *d = noveIzmene();
  JSONSerializer json;
  json.save(*d, dir);
  delete d;
}

Developer *IzmeniNalogDeveloper::noveIzmene() {
  std::unique_ptr<ObradaInterfejsa<Ui::IzmeniNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::IzmeniNalogDeveloper>);

  NacinRada nr = obrada->dohvatiNacinRada(ui);

  return new Developer(
       obrada->dohvatiIme(ui), obrada->dohvatiPrezime(ui), obrada->dohvatiPol(ui), obrada->dohvatiTehnologije(ui), obrada->dohvatiBrojGodinaIskustva(ui),
       obrada->dohvatiMaxPlatu(ui).toUInt(), nr, obrada->dohvatiVestine(ui), obrada->dohvatiBenefite(ui), obrada->dohvatiObrazovanje(ui),
       obrada->dohvatiUsername(ui), obrada->dohvatiSifru(ui), obrada->dohvatiBrTelefona(ui));
}

void IzmeniNalogDeveloper::zavrsiIzmene() {
  const std::unique_ptr<ObradaInterfejsa<Ui::IzmeniNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::IzmeniNalogDeveloper>);
  QString filePath = "developer";
  filePath = filePath + QDir::separator();
  filePath = filePath + obrada->dohvatiUsername(ui);
  filePath = filePath + ".json";
  SearchThread *searchThread = new SearchThread(filePath);

  connect(searchThread, &SearchThread::directoryFound, this, &IzmeniNalogDeveloper::pronadjenDirektorijumDeveloper);

  searchThread->start();

  connect(searchThread, &SearchThread::finished, searchThread, &QObject::deleteLater);
}
