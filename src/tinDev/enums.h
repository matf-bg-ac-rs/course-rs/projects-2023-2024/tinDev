#pragma once

enum class StrucnaSprema { SrednjaSkola, OsnovneStudije, MasterStudije, DoktorskeStudije };

enum class NacinRada { Remote, Hybrid, Office };
