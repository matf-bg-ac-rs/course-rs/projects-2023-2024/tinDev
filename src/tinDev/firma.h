#pragma once
#include "../serialization/serializable.h"
#include "korisnik.h"
#include <QDir>
#include <QString>
#include <iostream>

class Firma : public Korisnik, public Serializable {
public:
  Firma();
  Firma(const QString &ime, const QString &adresa, const QString &brTelefona, const QString &email, const QString &username, const QString &sifra);

  ~Firma() override = default;

  inline QString username() const override { return m_username; }
  inline QString sifra() const override { return m_sifra; }
  inline QString ime() const { return m_ime; }
  inline QString adresa() const { return m_adresa; }
  inline QString brTelefona() const { return m_brTelefona; }
  inline QString email() const { return m_email; }

private:
  QString m_username;
  QString m_sifra;
  QString m_ime;
  QString m_adresa;
  QString m_brTelefona;
  QString m_email;

  friend void setUsername(Firma &firma, const QString &newUsername);
  friend void setSifra(Firma &firma, const QString &newSifra);
  friend void setIme(Firma &firma, const QString &newIme);
  friend void setAdresa(Firma &firma, const QString &newAdresa);
  friend void setBrTelefona(Firma &firma, const QString &newBrTelefona);
  friend void setEmail(Firma &firma, const QString &newEmail);

public:
  QVariant toVariant() const override;
  void fromVariant(const QVariant &variant) override;
};
