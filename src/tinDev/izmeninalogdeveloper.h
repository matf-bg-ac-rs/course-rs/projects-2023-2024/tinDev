#pragma once
#include "developer.h"
#include "ui_izmeninalogdeveloper.h"
#include <QWidget>

namespace Ui {
class IzmeniNalogDeveloper;
}

class IzmeniNalogDeveloper : public QWidget {
  Q_OBJECT

public:
  explicit IzmeniNalogDeveloper(QWidget *parent = nullptr);
  ~IzmeniNalogDeveloper();

  Ui::IzmeniNalogDeveloper *getUi();
  void postaviUsername(const QString &username);
  void postaviOsnovnePodatke(const QString &ime, const QString &prezime, char pol, const QString &sifra, const QString &brTelefona);
  void postaviTehnologije(const QVector<QString> &tehnologije);
  void postaviBrojGodinaIskustva(const QMap<QString, unsigned> &iskustvo);
  void postaviPlatu(const unsigned &plata);
  void postaviNacinRada(const NacinRada nacinRada);
  void postaviStrucnuSpremu(const StrucnaSprema strucnaSprema);
  void postaviVestine(const QVector<QString> &vestine);
  void postaviBenefite(const QVector<QString> &benefiti);

signals:
  void windowLoaded();

private slots:
  void kliknutoNastaviKaTehnologijama();
  void kliknutoNastaviKaIskustvu();
  void kliknutoNastaviKaPlati();
  void kliknutoNastaviKaVestinama();
  void kliknutoNastaviKaBenefitima();

  void kliknutoPotvrdiIzmene();
  void kliknutoPonistiIzmene();

  void NazadNaOsnovnePodatke();
  void NazadNaTehnologije();
  void NazadNaIskustvo();
  void NazadNaPlatu();
  void NazadNaVestine();

  void pronadjenDirektorijumDeveloper(const QString &dir);

private:
  Ui::IzmeniNalogDeveloper *ui;
  Developer *noveIzmene();
  void zavrsiIzmene();
};
