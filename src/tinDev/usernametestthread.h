#pragma once
#include <QDir>
#include <QThread>

class UsernameTestThread : public QThread {
  Q_OBJECT
public:
  explicit UsernameTestThread(const QString &targetFileName, QObject *parent = nullptr);

  QString getFoundPath() const;
  QString searchForDirectory(const QDir &currentDir);

signals:
  void directoryFound(const QString &directory);
  void directoryNotFound();

protected:
  void run() override;

private:
  const QString targetFileName;
  QString foundPath;
};
