#include "svioglasi.h"
#include "ui_svioglasi.h"

#include "../serialization/jsonserializer.h"
#include "izmenioglas.h"
#include "loginthread.h"
#include "obradainterfejsa.h"
#include "oglas.h"

#include <QDebug>
#include <QDir>
#include <QFontDatabase>
#include <QList>

SviOglasi::SviOglasi(QWidget *parent) : QWidget(parent), ui(new Ui::SviOglasi) {
  ui->setupUi(this);

  connect(ui->pbIzmeniOglas, &QPushButton::clicked, this, &SviOglasi::izmeniOglas);
  connect(ui->pbIzbrisiOglas, &QPushButton::clicked, this, &SviOglasi::izabranOglasZaBrisanje);

  int fontId = QFontDatabase::addApplicationFont(":/resource/font/resources/font/Humaroid.otf");
  if (fontId != -1) {
    QString fontName = QFontDatabase::applicationFontFamilies(fontId).at(0);
    QFont font(fontName);
    font.setPointSize(18);

    ui->lbImeFirme->setFont(font);
  }
}

SviOglasi::~SviOglasi() { delete ui; }

Ui::SviOglasi *SviOglasi::getUi() { return ui; }

void SviOglasi::postaviUsername(const QString &username) { ui->lbImeFirme->setText(username); }
void SviOglasi::postaviOglas(const QString &id) { ui->lwListaOglasa->addItem(id); }

void SviOglasi::izabranOglasZaBrisanje() {

  const std::unique_ptr<ObradaInterfejsa<Ui::SviOglasi>> obrada(new ObradaInterfejsa<Ui::SviOglasi>);

  QList<QListWidgetItem *> selectedItems = ui->lwListaOglasa->selectedItems();

  (!selectedItems.isEmpty()) ? zapocetoBrisanje() : obrada->upozorenje("Morate izabrati oglas!");
}

void SviOglasi::pronadjenDirektorijumOglasZaBrisanje(const QString &dir) {
  Oglas *o = new Oglas();
  JSONSerializer json;
  QFile fajl(dir);

  QListWidgetItem *selectedItem = ui->lwListaOglasa->currentItem();

  json.load(*o, dir);
  if (fajl.exists()) {
    int row = ui->lwListaOglasa->row(selectedItem);
    ui->lwListaOglasa->takeItem(row);
    delete selectedItem;
    fajl.remove();
  }

  emit windowLoaded();
}

void SviOglasi::windowLoaded2PR() { emit windowLoaded(); }
void SviOglasi::pronadjenDirektorijumOglas(const QString &dir) {
  auto *o = new Oglas();
  JSONSerializer json;
  json.load(*o, dir);

  auto *izmena = new IzmeniOglas();
  izmena->postaviNazivPozicije(o->nazivPozicije());
  izmena->postaviNazivFirme(o->usernameFirma());
  izmena->postaviPlatu(o->plata());
  izmena->postaviNacinRada(o->nacinRada());
  izmena->postaviStrucnuSpremu(o->strucnaSprema());
  izmena->postaviTehnologije(o->tehnologije());
  izmena->postaviBrojGodinaIskustva(o->brGodIskustva());
  izmena->postaviVestine(o->vestine());
  izmena->postaviBenefite(o->benefiti());
  izmena->postaviId(o->id());

  izmena->show();

  connect(izmena, &IzmeniOglas::windowLoaded, this, &SviOglasi::windowLoaded2PR);
}

void SviOglasi::izmeniOglas() {

  const std::unique_ptr<ObradaInterfejsa<Ui::SviOglasi>> obrada(new ObradaInterfejsa<Ui::SviOglasi>);

  QList<QListWidgetItem *> selectedItems = ui->lwListaOglasa->selectedItems();

  (!selectedItems.isEmpty()) ? zapoceteIzmene() : obrada->upozorenje("Morate izabrati oglas!");
}

void SviOglasi::zapoceteIzmene() {
  QString oglasStr = ui->lwListaOglasa->currentItem()->text();
  QString nazivFajla = oglasStr.section(" ", 0, 0);

  QString filePath = "oglas";
  filePath = filePath + QDir::separator();
  filePath = filePath + nazivFajla;
  filePath = filePath + ".json";

  loginthread *thread = new loginthread(filePath);

  connect(thread, &loginthread::directoryFound, this, &SviOglasi::pronadjenDirektorijumOglas);
  thread->start();

  connect(thread, &loginthread::finished, thread, &QObject::deleteLater);
}

void SviOglasi::zapocetoBrisanje() {
  QString oglasStr = ui->lwListaOglasa->currentItem()->text();
  QString nazivFajla = oglasStr.section(" ", 0, 0);

  QString filePath = "oglas";
  filePath = filePath + QDir::separator();
  filePath = filePath + nazivFajla;
  filePath = filePath + ".json";

  loginthread *thread = new loginthread(filePath);

  connect(thread, &loginthread::directoryFound, this, &SviOglasi::pronadjenDirektorijumOglasZaBrisanje);
  thread->start();

  connect(thread, &loginthread::finished, thread, &QObject::deleteLater);
}
