#pragma once
#include <QDir>
#include <QThread>

class loginthread : public QThread {
  Q_OBJECT
public:
  explicit loginthread(const QString &targetFileName, const QString &sifra = "", QObject *parent = nullptr);

  QString getFoundPath() const;
  QString searchForDirectory(const QDir &currentDir);

signals:
  void directoryFound(const QString &directory, const QString &sifra);
  void directoryNotFound();

protected:
  void run() override;

private:
  const QString targetFileName;
  QString foundPath;
  const QString sifra;
};
