#include "procenat.h"

Procenat::Procenat(Developer *d, Oglas *o) : m_d(d), m_o(o) {
  if (m_d == nullptr || m_o == nullptr) {
    throw std::invalid_argument("Pokazivač na objekat je nullptr!");
  }
}

double Procenat::uporediPoklapanjeDevOglas() {
  m_brojacPoklapanja = 0;
  QVector<QString> oglasVestine = m_o->vestine();
  QVector<QString> developerVestine = m_d->vestine();
  for (const QString &vestina : oglasVestine) {
    if (developerVestine.contains(vestina)) {
      m_brojacPoklapanja++;
    }
  }
  m_ukupno = m_ukupno + (m_brojacPoklapanja / oglasVestine.size());
  m_brojacPoklapanja = 0;

  QVector<QString> oglasTehnologije = m_o->tehnologije();
  QVector<QString> developerTehnologije = m_d->tehnologije();
  for (const QString &tehnologija : oglasTehnologije) {
    if (developerTehnologije.contains(tehnologija)) {
      m_brojacPoklapanja++;
    }
  }
  m_ukupno = m_ukupno + (m_brojacPoklapanja / oglasTehnologije.size());
  m_brojacPoklapanja = 0;

  if (m_o->plata() - m_d->opsegPlate() < 200 && m_o->plata() - m_d->opsegPlate() > 0) {
    m_ukupno = m_ukupno + 1;
  }
  if (m_o->plata() - m_d->opsegPlate() < 300 && m_o->plata() - m_d->opsegPlate() > 0) {
    m_ukupno = m_ukupno + 0.75;
  } else if (m_o->plata() - m_d->opsegPlate() < 400 && m_o->plata() - m_d->opsegPlate() > 0) {
    m_ukupno = m_ukupno + 0.25;
  } else {
    m_ukupno = m_ukupno + 0;
  }

  QMap<QString, unsigned> oglasIskustvo = m_o->brGodIskustva();
  QMap<QString, unsigned> developerIskustvo = m_d->brGodinaIskustva();
  for (const QString &iskustvo : oglasIskustvo.keys()) {
    if (developerIskustvo.contains(iskustvo) && developerIskustvo[iskustvo] >= oglasIskustvo[iskustvo]) {
      m_brojacPoklapanja++;
    }
  }
  m_ukupno = m_ukupno + (m_brojacPoklapanja / oglasIskustvo.size());
  m_brojacPoklapanja = 0;

  if (m_o->strucnaSprema() <= m_d->nivoObrazovanja()) {
    m_ukupno++;
  }

  if (m_o->nacinRada() == m_d->nacinRada()) {
    m_ukupno++;
  }

  m_procenat = m_ukupno / m_brojDisciplina;
  return m_procenat * 100;
}
