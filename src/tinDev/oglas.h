#pragma once
#include "../serialization/serializable.h"
#include "enums.h"
#include <QDebug>
#include <QMap>
#include <QPair>
#include <QString>
#include <QVector>
#include <iostream>

class Oglas : public Serializable {
public:
  Oglas();

  Oglas(
       const QString &nazivPozicije, const unsigned plata, const NacinRada nacinRada, const StrucnaSprema strucnaSprema, const QVector<QString> &tehnologije,
       const QMap<QString, unsigned> &brGodIskustva, const QVector<QString> &vestine, const QVector<QString> &benefiti, const QString &usernameFirma);

  Oglas(
       const QString &nazivPozicije, const unsigned plata, const NacinRada nacinRada, const StrucnaSprema strucnaSprema, const QVector<QString> &tehnologije,
       const QMap<QString, unsigned> &brGodIskustva, const QVector<QString> &vestine, const QVector<QString> &benefiti, const QString &usernameFirma,
       const QString &id);

  ~Oglas() = default;

  inline QString nazivPozicije() const { return m_nazivPozicije; }
  inline unsigned plata() const { return m_plata; }
  inline QVector<QString> tehnologije() const { return m_tehnologije; }
  inline QMap<QString, unsigned> brGodIskustva() const { return m_brGodIskustva; }
  inline QVector<QString> vestine() const { return m_vestine; }
  inline QVector<QString> benefiti() const { return m_benefiti; }
  inline QString usernameFirma() const { return m_usernameFirma; }
  inline QString id() const { return m_id; }
  inline NacinRada nacinRada() const { return m_nacinRada; }
  inline StrucnaSprema strucnaSprema() const { return m_strucnaSprema; }

  inline double procenatPoklapanja() const { return m_procenatPoklapanja; }
  inline void setProcenat(double procenat) { m_procenatPoklapanja = procenat; }

  QString stepenStrucneSpremeStr() const;
  QString nacinRadaStr() const;

private:
  QString m_nazivPozicije;
  unsigned m_plata;
  NacinRada m_nacinRada;
  StrucnaSprema m_strucnaSprema;
  QVector<QString> m_tehnologije;
  QMap<QString, unsigned> m_brGodIskustva;
  QVector<QString> m_vestine;
  QVector<QString> m_benefiti;
  QString m_usernameFirma;
  QString m_id;
  double m_procenatPoklapanja;

  friend void setNazivPozicije(Oglas &og, const QString &newNazivPozicije);
  friend void setPlata(Oglas &og, const unsigned &newPlata);
  friend void setNacinRada(Oglas &og, NacinRada newNacinRada);
  friend void setStrucnaSprema(Oglas &og, StrucnaSprema newStrucnaSprema);
  friend void setTehnologije(Oglas &og, const QVector<QString> &newTehnologije);
  friend void setBrGodIskustva(Oglas &og, const QMap<QString, unsigned> &newBrGodIskustva);
  friend void setVestine(Oglas &og, const QVector<QString> &newVestine);
  friend void setBenefiti(Oglas &og, const QVector<QString> &newBenefiti);

public:
  QVariant toVariant() const override;
  void fromVariant(const QVariant &variant) override;
};
