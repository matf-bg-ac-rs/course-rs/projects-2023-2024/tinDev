#pragma once
#include <QDir>
#include <QThread>

class SearchThread : public QThread {
  Q_OBJECT
public:
  SearchThread(const QString &targetFileName, QObject *parent = nullptr);

  QString getFoundPath() const;
  QString searchForDirectory(const QDir &currentDir);

signals:
  void directoryFound(const QString &directory);

protected:
  void run() override;

private:
  const QString targetFileName;
  QString foundPath;
};
