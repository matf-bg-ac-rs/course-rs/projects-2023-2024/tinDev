#include "oglas.h"
#include <chrono>

Oglas::Oglas() = default;

Oglas::Oglas(
     const QString &nazivPozicije, const unsigned plata, const NacinRada nacinRada, const StrucnaSprema strucnaSprema, const QVector<QString> &tehnologije,
     const QMap<QString, unsigned> &brGodIskustva, const QVector<QString> &vestine, const QVector<QString> &benefiti, const QString &usernameFirma)
     : m_nazivPozicije(nazivPozicije), m_plata(plata), m_nacinRada(nacinRada), m_strucnaSprema(strucnaSprema), m_tehnologije(tehnologije),
       m_brGodIskustva(brGodIskustva), m_vestine(vestine), m_benefiti(benefiti), m_usernameFirma(usernameFirma),
       m_id(QString::number(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count() / 10 * 10)) {}

Oglas::Oglas(
     const QString &nazivPozicije, const unsigned plata, const NacinRada nacinRada, const StrucnaSprema strucnaSprema, const QVector<QString> &tehnologije,
     const QMap<QString, unsigned> &brGodIskustva, const QVector<QString> &vestine, const QVector<QString> &benefiti, const QString &usernameFirma,
     const QString &id)
     : m_nazivPozicije(nazivPozicije), m_plata(plata), m_nacinRada(nacinRada), m_strucnaSprema(strucnaSprema), m_tehnologije(tehnologije),
       m_brGodIskustva(brGodIskustva), m_vestine(vestine), m_benefiti(benefiti), m_usernameFirma(usernameFirma), m_id(id) {}

QString Oglas::stepenStrucneSpremeStr() const {
  switch (m_strucnaSprema) {

  case StrucnaSprema::SrednjaSkola:
    return "Srednja skola";
  case StrucnaSprema::OsnovneStudije:
    return "Osnovne studije";
  case StrucnaSprema::MasterStudije:
    return "Master studije";
  case StrucnaSprema::DoktorskeStudije:
    return "Doktorske studije";
  default:
    throw std::exception("Nepoznato obrazovanje");
  }
}

QString Oglas::nacinRadaStr() const {
  switch (m_nacinRada) {
  case NacinRada::Hybrid:
    return "Hybrid";
  case NacinRada::Office:
    return "Office";
  case NacinRada::Remote:
    return "Remote";
  default:
    throw std::exception("Nepoznat nacin rada");
  }
}

void setNazivPozicije(Oglas &og, const QString &newNazivPozicije) { og.m_nazivPozicije = newNazivPozicije; }

void setPlata(Oglas &og, const unsigned &newPlata) { og.m_plata = newPlata; }

void setNacinRada(Oglas &og, NacinRada newNacinRada) { og.m_nacinRada = newNacinRada; }

void setStrucnaSprema(Oglas &og, StrucnaSprema newStrucnaSprema) { og.m_strucnaSprema = newStrucnaSprema; }

void setTehnologije(Oglas &og, const QVector<QString> &newTehnologije) { og.m_tehnologije = newTehnologije; }

void setBrGodIskustva(Oglas &og, const QMap<QString, unsigned> &newBrGodIskustva) { og.m_brGodIskustva = newBrGodIskustva; }

void setVestine(Oglas &og, const QVector<QString> &newVestine) { og.m_vestine = newVestine; }

void setBenefiti(Oglas &og, const QVector<QString> &newBenefiti) { og.m_benefiti = newBenefiti; }

QVariant Oglas::toVariant() const {
  QVariantMap result;

  result["nazivPozicije"] = m_nazivPozicije;
  result["plata"] = m_plata;
  result["tehnologije"] = QVariant::fromValue<QVector<QString>>(m_tehnologije);
  QVariantMap vMap;
  for (auto it = m_brGodIskustva.begin(); it != m_brGodIskustva.end(); ++it) {
    vMap[it.key()] = it.value();
  }
  result["brGodIskustva"] = vMap;
  result["nacinRada"] = static_cast<int>(m_nacinRada);
  result["vestine"] = QVariant::fromValue<QVector<QString>>(m_vestine);
  result["benefiti"] = QVariant::fromValue<QVector<QString>>(m_benefiti);
  result["strucnaSprema"] = static_cast<int>(m_strucnaSprema);
  result["usernameFirma"] = m_usernameFirma;
  result["id"] = m_id;

  return result;
}

void Oglas::fromVariant(const QVariant &variant) {
  QVariantMap map = variant.toMap();

  m_nazivPozicije = map["nazivPozicije"].toString();
  m_tehnologije = map["tehnologije"].value<QVector<QString>>();
  QVariantMap vMap = map["brGodIskustva"].toMap();
  QMap<QString, unsigned> deserializedMap;

  for (auto it = vMap.begin(); it != vMap.end(); ++it) {
    deserializedMap[it.key()] = it.value().value<unsigned>();
  }
  m_brGodIskustva = deserializedMap;
  m_plata = map["plata"].toUInt();
  m_nacinRada = static_cast<NacinRada>(map["nacinRada"].toInt());
  m_vestine = map["vestine"].value<QVector<QString>>();
  m_benefiti = map["benefiti"].value<QVector<QString>>();
  m_strucnaSprema = static_cast<StrucnaSprema>(map["strucnaSprema"].toInt());
  m_usernameFirma = map["usernameFirma"].toString();
  m_id = map["id"].toString();
}
