#pragma once
#include "firma.h"
#include "ui_izmeninalogfirma.h"
#include <QWidget>

namespace Ui {
class IzmeniNalogFirma;
}

class IzmeniNalogFirma : public QWidget {
  Q_OBJECT

public:
  explicit IzmeniNalogFirma(QWidget *parent = nullptr);
  ~IzmeniNalogFirma();
  Ui::IzmeniNalogFirma *getUi();
  void postaviUsername(const QString &username);
  void postaviPodatke(const QString &sifra, const QString &nazivFirme, const QString &adresa, const QString &brTelefona, const QString &eMail);

private slots:
  void kliknutoPotvrdiIzmene();
  void klinkutoPonistiIzmene();
  void directoryFoundFirma(const QString &directory);

private:
  Ui::IzmeniNalogFirma *ui;
  Firma *noveIzmene();
  void zavrsiIzmene();
};
