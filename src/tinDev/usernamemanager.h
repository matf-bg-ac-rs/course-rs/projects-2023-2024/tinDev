#pragma once
#include <QString>

class IUsernameProvider;

class UsernameManager {
public:
  static QString getUsername();

  static void setUsernameProvider(IUsernameProvider *provider);

private:
  static IUsernameProvider *usernameProvider;
};
