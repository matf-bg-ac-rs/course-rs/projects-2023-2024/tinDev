#include "usernamemanager.h"
#include "iusernameprovider.h"

IUsernameProvider *UsernameManager::usernameProvider = nullptr;

QString UsernameManager::getUsername() { return (usernameProvider) ? usernameProvider->getUsername() : "UnknownUsername"; }

void UsernameManager::setUsernameProvider(IUsernameProvider *provider) { usernameProvider = provider; }
