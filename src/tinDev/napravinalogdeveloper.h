#pragma once
#include "developer.h"
#include "obradainterfejsa.h"
#include <QGridLayout>
#include <QString>
#include <QWidget>

namespace Ui {
class NapraviNalogDeveloper;
}

class NapraviNalogDeveloper : public QWidget, ObradaInterfejsa<NapraviNalogDeveloper> {
  Q_OBJECT

public:
  explicit NapraviNalogDeveloper(QWidget *parent = nullptr);
  ~NapraviNalogDeveloper();

private slots:
  void kliknutoNastaviKaTehnologijama();
  void kliknutoNastaviKaIskustvu();
  void kliknutoNastaviKaPlati();
  void kliknutoNastaviKaVestinama();
  void kliknutoNastaviKaBenefitima();
  void kliknutoZavrsiRegistraciju();
  void kliknutoNazadNaVestine();
  void kliknutoNazadNaOpsegPlate();
  void kliknutoNazadNaIskustvo();
  void kliknutoNazadNaTehnologije();
  void kliknutoNazadNaPodatke();
  void kliknutoNazadNaLogIn();
  void promenjenaVrednostMinPlate(int value);

  void directoryFound(const QString &directory);
  void directoryNotFound();
  void usernameDobar();

  void sacuvaj();
  void zatvoriProzor();
  void proveraUsername();

private:
  Ui::NapraviNalogDeveloper *ui;
  Developer *zavrsenaRegistracija();
};
