#pragma once
#include <QObject>

class Match {
public:
  Match(const QString &putanja, const QString &targetUsername, const QString &imeFirmeDevelopera);

  ~Match() = default;

  void proveriMatch();
  inline bool indikator() const { return m_indikatorMatch; }
  inline QString putanja() const { return m_putanja; }
  inline QString targetUsername() const { return m_targetUsername; }
  inline QString imeFirmeDevelopera() const { return m_imeFirmeDevelopera; }

private:
  QString m_putanja;
  QString m_targetUsername;
  QString m_imeFirmeDevelopera;
  bool m_indikatorMatch;
};
