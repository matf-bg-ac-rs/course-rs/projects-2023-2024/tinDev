QT       += core gui
QT += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++20

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    developer.cpp \
    dodajoglas.cpp \
    firma.cpp \
    izmeninalogdeveloper.cpp \
    izmeninalogfirma.cpp \
    izmenioglas.cpp \
    korisnik.cpp \
    login.cpp \
    loginthread.cpp \
    main.cpp \
    mainwindowdeveloper.cpp \
    mainwindowfirma.cpp \
    match.cpp \
    napravinalogdeveloper.cpp \
    napravinalogfirma.cpp \
    oglas.cpp \
    procenat.cpp \
    readfilethread.cpp \
    rewritethread.cpp \
    searchthread.cpp \
    svioglasi.cpp \
    usernametestthread.cpp \
    usernamemanager.cpp

HEADERS += \
    rewritethread.h \
    developer.h \
    dodajoglas.h \
    enums.h \
    firma.h \
    iusernameprovider.h \
    izmeninalogdeveloper.h \
    izmeninalogfirma.h \
    izmenioglas.h \
    korisnik.h \
    login.h \
    loginthread.h \
    mainwindowdeveloper.h \
    mainwindowfirma.h \
    match.h \
    napravinalogdeveloper.h \
    napravinalogfirma.h \
    obradainterfejsa.h \
    oglas.h \
    procenat.h \
    readfilethread.h \
    searchthread.h \
    svioglasi.h \
    usernametestthread.h \
    usernamemanager.h

FORMS += \
    dodajoglas.ui \
    izmeninalogdeveloper.ui \
    izmeninalogfirma.ui \
    izmenioglas.ui \
    login.ui \
    mainwindowdeveloper.ui \
    mainwindowfirma.ui \
    napravinalogdeveloper.ui \
    napravinalogfirma.ui \
    svioglasi.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    README.md \
    resources/benefiti/13plata.png \
    resources/benefiti/car.png \
    resources/benefiti/fitpass.png \
    resources/benefiti/flexiblehours.png \
    resources/benefiti/lifeinsurance.png \
    resources/benefiti/lunch.png \
    resources/benefiti/parking.png \
    resources/benefiti/stocks.png \
    resources/benefiti/vacation.png \
    resources/jezici/Ada.png \
    resources/jezici/Haskell.png \
    resources/jezici/bash.png \
    resources/jezici/c \#.png
    resources/jezici/c.png \
    resources/jezici/clojure.png \
    resources/jezici/cppLogo.png \
    resources/jezici/css.png \
    resources/jezici/f \#.png
    resources/jezici/go.png \
    resources/jezici/html.png \
    resources/jezici/java.png \
    resources/jezici/javascript.png \
    resources/jezici/kotlin.png \
    resources/jezici/lisp.png \
    resources/jezici/objc.png \
    resources/jezici/perl.png \
    resources/jezici/php.png \
    resources/jezici/prolog.png \
    resources/jezici/python.png \
    resources/jezici/ruby.png \
    resources/jezici/rust.png \
    resources/jezici/scala.png \
    resources/jezici/sql.png \
    resources/jezici/swift.png \
    resources/tehnologije/AmazonS3.png \
    resources/tehnologije/ApacheCassandra.png \
    resources/tehnologije/amazonAurora.png \
    resources/tehnologije/android.png \
    resources/tehnologije/angular.png \
    resources/tehnologije/angularJS.png \
    resources/tehnologije/ansible.png \
    resources/tehnologije/apache.png \
    resources/tehnologije/apacheSolr.png \
    resources/tehnologije/apacheSpark.png \
    resources/tehnologije/appium.png \
    resources/tehnologije/asembler.png \
    resources/tehnologije/asp.png \
    resources/tehnologije/azure.png \
    resources/tehnologije/chatGPT.png \
    resources/tehnologije/cisco.png \
    resources/tehnologije/cypress.png \
    resources/tehnologije/devops.png \
    resources/tehnologije/django.png \
    resources/tehnologije/docker.png \
    resources/tehnologije/dotNet.png \
    resources/tehnologije/embeded.png \
    resources/tehnologije/git.png \
    resources/tehnologije/ios.png \
    resources/tehnologije/jenkins.png \
    resources/tehnologije/linux.png \
    resources/tehnologije/macos.png \
    resources/tehnologije/oracle.png \
    resources/tehnologije/powershell.png \
    resources/tehnologije/qt.png \
    resources/tehnologije/unity.png \
    resources/tehnologije/vmware.png \
    resources/tehnologije/wordpress.png \
    resources/vestine/backend.png \
    resources/vestine/dataAnalyst.png \
    resources/vestine/dataengineer.png \
    resources/vestine/datascience.png \
    resources/vestine/frontend.png \
    resources/vestine/fullstack.png \
    resources/vestine/gamedeveloper.png \
    resources/vestine/projectlead.png \
    resources/vestine/q&atester.png \
    resources/vestine/scrummaster.png \
    resources/vestine/teamlead.png \
    resources/vestine/web3.png

RESOURCES += \
    resource.qrc

INCLUDEPATH += ../serialization

SOURCES += ../serialization/jsonserializer.cpp \
            ../serialization/serializable.cpp \
            ../serialization/serializer.cpp

HEADERS += ../serialization/jsonserializer.h \
           ../serialization/serializable.h \
           ../serialization/serializer.h

INCLUDEPATH += ../clientServer

SOURCES += ../clientServer/chatroom.cpp \
            ../clientServer/client.cpp \
            ../clientServer/connection.cpp \
            ../clientServer/peermanager.cpp \
            ../clientServer/server.cpp

HEADERS += ../clientServer/chatroom.h \
           ../clientServer/client.h \
           ../clientServer/connection.h \
           ../clientServer/peermanager.h \
           ../clientServer/server.h

FORMS += ../clientServer/chatroom.ui
