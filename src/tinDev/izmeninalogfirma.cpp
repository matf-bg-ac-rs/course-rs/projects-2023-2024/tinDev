#include "izmeninalogfirma.h"
#include "../serialization/jsonserializer.h"
#include "firma.h"
#include "obradainterfejsa.h"
#include "searchthread.h"
#include "ui_izmeninalogfirma.h"
#include <QDir>

IzmeniNalogFirma::IzmeniNalogFirma(QWidget *parent) : QWidget(parent), ui(new Ui::IzmeniNalogFirma) {
  ui->setupUi(this);
  connect(ui->pbPotvrdiIzmene, &QPushButton::clicked, this, &IzmeniNalogFirma::kliknutoPotvrdiIzmene);
  connect(ui->pbPonisti, &QPushButton::clicked, this, &IzmeniNalogFirma::klinkutoPonistiIzmene);
}

IzmeniNalogFirma::~IzmeniNalogFirma() { delete ui; }

Ui::IzmeniNalogFirma *IzmeniNalogFirma::getUi() { return ui; }

void IzmeniNalogFirma::postaviUsername(const QString &username) { ui->leUsername->setText(username); }

void IzmeniNalogFirma::postaviPodatke(const QString &sifra, const QString &nazivFirme, const QString &adresa, const QString &brTelefona, const QString &eMail) {
  ui->leSifra->setText(sifra);
  ui->leNazivFirme->setText(nazivFirme);
  ui->leAdresa->setText(adresa);
  ui->leBrojTelefona->setText(brTelefona);
  ui->leEMail->setText(eMail);
}

void IzmeniNalogFirma::kliknutoPotvrdiIzmene() {
  zavrsiIzmene();
  this->close();
}

void IzmeniNalogFirma::klinkutoPonistiIzmene() { this->close(); }

void IzmeniNalogFirma::directoryFoundFirma(const QString &directory) {
  Firma *f = noveIzmene();
  JSONSerializer json;
  json.save(*f, directory);
  delete f;
}

Firma *IzmeniNalogFirma::noveIzmene() {
  std::unique_ptr<ObradaInterfejsa<Ui::IzmeniNalogFirma>> obrada(new ObradaInterfejsa<Ui::IzmeniNalogFirma>);
  QString username = obrada->dohvatiUsername(ui);
  QString sifra = obrada->dohvatiSifru(ui);
  QString nazivFirme = obrada->dohvatiNazivFirme(ui);
  QString brojTelefona = obrada->dohvatiBrTelefona(ui);
  QString email = obrada->dohvatiEmail(ui);
  QString adresa = obrada->dohvatiAdresu(ui);

  return new Firma(nazivFirme, adresa, brojTelefona, email, username, sifra);
}

void IzmeniNalogFirma::zavrsiIzmene() {
  const std::unique_ptr<ObradaInterfejsa<Ui::IzmeniNalogFirma>> obrada(new ObradaInterfejsa<Ui::IzmeniNalogFirma>);
  QString filePath = "firma";
  filePath = filePath + QDir::separator();
  filePath = filePath + obrada->dohvatiUsername(ui);
  filePath = filePath + ".json";
  SearchThread *searchThread = new SearchThread(filePath);

  connect(searchThread, &SearchThread::directoryFound, this, &IzmeniNalogFirma::directoryFoundFirma);

  searchThread->start();

  connect(searchThread, &SearchThread::finished, searchThread, &QObject::deleteLater);
}
