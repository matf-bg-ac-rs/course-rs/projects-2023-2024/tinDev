#include "napravinalogfirma.h"
#include "../serialization/jsonserializer.h"
#include "login.h"
#include "mainwindowfirma.h"
#include "obradainterfejsa.h"
#include "searchthread.h"
#include "ui_napravinalogfirma.h"
#include "usernametestthread.h"
#include <QDir>

NapraviNalogFirma::NapraviNalogFirma(QWidget *parent) : QWidget(parent), ui(new Ui::NapraviNalogFirma) {
  ui->setupUi(this);

  connect(ui->pbZavrsiRegistraciju, &QPushButton::clicked, this, &NapraviNalogFirma::kliknutoZavrsiRegistraciju);
  connect(ui->pbNazadNaLogIn, &QPushButton::clicked, this, &NapraviNalogFirma::kliknutoNazadNaLogIn);
}

NapraviNalogFirma::~NapraviNalogFirma() { delete ui; }

void NapraviNalogFirma::directoryFound(const QString &directory) {
  Firma *f = zavrsenaRegistracija();
  JSONSerializer json;
  json.save(*f, directory);
}

void NapraviNalogFirma::kliknutoZavrsiRegistraciju() {
  std::unique_ptr<ObradaInterfejsa<Ui::NapraviNalogFirma>> obrada(new ObradaInterfejsa<Ui::NapraviNalogFirma>);
  if (!obrada->ispravniPodaci1(ui)) {
    obrada->upozorenje("Morate uneti podatke u sva polja!");
  } else if (!obrada->ispravanBrojTelefona(ui)) {
    ui->leBrojTelefona->setText("Neispravan format");
  } else if (!obrada->ispravanEmail(ui)) {
    ui->leEMail->setText("Neispravan format");
  } else {
    proveraUsername();
  }
}

void NapraviNalogFirma::kliknutoNazadNaLogIn() {
  LogIn *login = new LogIn;
  login->show();
  this->close();
}

Firma *NapraviNalogFirma::zavrsenaRegistracija() {
  std::unique_ptr<ObradaInterfejsa<Ui::NapraviNalogFirma>> obrada(new ObradaInterfejsa<Ui::NapraviNalogFirma>);
  QString username = obrada->dohvatiUsername(ui);
  QString sifra = obrada->dohvatiSifru(ui);
  QString nazivFirme = obrada->dohvatiNazivFirme(ui);
  QString brojTelefona = obrada->dohvatiBrTelefona(ui);
  QString email = obrada->dohvatiEmail(ui);
  QString adresa = obrada->dohvatiAdresu(ui);

  return new Firma(nazivFirme, adresa, brojTelefona, email, username, sifra);
}

void NapraviNalogFirma::sacuvaj() {

  const std::unique_ptr<ObradaInterfejsa<Ui::NapraviNalogFirma>> obrada(new ObradaInterfejsa<Ui::NapraviNalogFirma>);
  QString filePath = "firma";
  filePath = filePath + QDir::separator();
  filePath = filePath + obrada->dohvatiUsername(ui);
  filePath = filePath + ".json";
  SearchThread *searchThread = new SearchThread(filePath);

  connect(searchThread, &SearchThread::directoryFound, this, &NapraviNalogFirma::directoryFound);

  searchThread->start();

  connect(searchThread, &SearchThread::finished, searchThread, &QObject::deleteLater);
}

void NapraviNalogFirma::proveraUsername() {
  const std::unique_ptr<ObradaInterfejsa<Ui::NapraviNalogFirma>> obrada(new ObradaInterfejsa<Ui::NapraviNalogFirma>);
  QString filePath = "firma";
  filePath = filePath + QDir::separator();
  filePath = filePath + obrada->dohvatiUsername(ui);
  filePath = filePath + ".json";

  UsernameTestThread *usernameTestThread = new UsernameTestThread(filePath);

  connect(usernameTestThread, &UsernameTestThread::directoryFound, this, &NapraviNalogFirma::directoryNotFound);
  connect(usernameTestThread, &UsernameTestThread::directoryNotFound, this, &NapraviNalogFirma::usernameDobar);

  usernameTestThread->start();

  connect(usernameTestThread, &UsernameTestThread::finished, usernameTestThread, &QObject::deleteLater);
}

void NapraviNalogFirma::directoryNotFound() {
  const std::unique_ptr<ObradaInterfejsa<Ui::NapraviNalogFirma>> obrada(new ObradaInterfejsa<Ui::NapraviNalogFirma>);
  obrada->upozorenje("Ovaj username je vec zauzet!");
}

void NapraviNalogFirma::usernameDobar() {
  const std::unique_ptr<ObradaInterfejsa<Ui::NapraviNalogFirma>> obrada(new ObradaInterfejsa<Ui::NapraviNalogFirma>);
  sacuvaj();
  MainWindowFirma *w = new MainWindowFirma;
  w->postaviUsername(obrada->dohvatiUsername(ui));
  emit w->windowLoaded();
  w->show();
  this->close();
}
