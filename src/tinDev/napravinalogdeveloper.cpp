#include "napravinalogdeveloper.h"
#include "../serialization/jsonserializer.h"
#include "login.h"
#include "mainwindowdeveloper.h"
#include "searchthread.h"
#include "ui_napravinalogdeveloper.h"
#include "usernametestthread.h"
#include <QAbstractSlider>
#include <QCheckBox>
#include <QDebug>
#include <QDir>
#include <QGridLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QMessageBox>
#include <QPixmap>
#include <QRegularExpression>
#include <QSpinBox>
#include <QWidget>
#include <iostream>

struct ImageInfo {
  const QString path;
  QLabel *label;
};

NapraviNalogDeveloper::NapraviNalogDeveloper(QWidget *parent) : QWidget(parent), ui(new Ui::NapraviNalogDeveloper) {
  ui->setupUi(this);
  connect(ui->pbNastaviDalje, &QPushButton::clicked, this, &NapraviNalogDeveloper::kliknutoNastaviKaTehnologijama);
  connect(ui->pbNastaviDalje2, &QPushButton::clicked, this, &NapraviNalogDeveloper::kliknutoNastaviKaIskustvu);
  connect(ui->pbNastaviDalje3, &QPushButton::clicked, this, &NapraviNalogDeveloper::kliknutoNastaviKaPlati);
  connect(ui->pbNastaviDalje4, &QPushButton::clicked, this, &NapraviNalogDeveloper::kliknutoNastaviKaVestinama);
  connect(ui->pbNastaviDalje5, &QPushButton::clicked, this, &NapraviNalogDeveloper::kliknutoNastaviKaBenefitima);

  connect(ui->pbNazadNaVestine, &QPushButton::clicked, this, &NapraviNalogDeveloper::kliknutoNazadNaVestine);
  connect(ui->pbNazadNaOpsegPlate, &QPushButton::clicked, this, &NapraviNalogDeveloper::kliknutoNazadNaOpsegPlate);
  connect(ui->pbNazadNaIskustvo, &QPushButton::clicked, this, &NapraviNalogDeveloper::kliknutoNazadNaIskustvo);
  connect(ui->pbNazadNaTehnologije, &QPushButton::clicked, this, &NapraviNalogDeveloper::kliknutoNazadNaTehnologije);
  connect(ui->pbNazadNaPodatke, &QPushButton::clicked, this, &NapraviNalogDeveloper::kliknutoNazadNaPodatke);
  connect(ui->pbNazadNaLogIn, &QPushButton::clicked, this, &NapraviNalogDeveloper::kliknutoNazadNaLogIn);

  connect(ui->hsOpsegPlate1, &QSlider::sliderMoved, this, &NapraviNalogDeveloper::promenjenaVrednostMinPlate);

  connect(ui->pbZavrsiRegistraciju, &QPushButton::clicked, this, &NapraviNalogDeveloper::sacuvaj);
  connect(ui->pbZavrsiRegistraciju, &QPushButton::clicked, this, &NapraviNalogDeveloper::zatvoriProzor);
  connect(ui->pbZavrsiRegistraciju, &QPushButton::clicked, this, &NapraviNalogDeveloper::kliknutoZavrsiRegistraciju);

  ImageInfo images[] = {
       {":/resources/tehnologije/resources/tehnologije/amazonAurora.png", ui->lbAmazonAurora},
       {":/resources/tehnologije/resources/tehnologije/AmazonS3.png", ui->lbAmazonS3},
       {":/resources/tehnologije/resources/tehnologije/android.png", ui->lbAndroid},
       {":/resources/tehnologije/resources/tehnologije/angular.png", ui->lbAngular},
       {":/resources/tehnologije/resources/tehnologije/angularJS.png", ui->lbAngularJS},
       {":/resources/tehnologije/resources/tehnologije/ansible.png", ui->lbAnsiable},
       {":/resources/tehnologije/resources/tehnologije/ApacheCassandra.png", ui->lbApacheCassandra},
       {":/resources/tehnologije/resources/tehnologije/apacheSolr.png", ui->lbSolr},
       {":/resources/tehnologije/resources/tehnologije/apacheSpark.png", ui->lbSpark},
       {":/resources/tehnologije/resources/tehnologije/appium.png", ui->lbAppium},
       {":/resources/tehnologije/resources/tehnologije/asembler.png", ui->lbAsembler},
       {":/resources/tehnologije/resources/tehnologije/asp.png", ui->lbASPNET},
       {":/resources/tehnologije/resources/tehnologije/azure.png", ui->lbAzure},
       {":/resources/tehnologije/resources/tehnologije/chatGPT.png", ui->lbChatGpt},
       {":/resources/tehnologije/resources/tehnologije/cisco.png", ui->lbCisco},
       {":/resources/tehnologije/resources/tehnologije/cypress.png", ui->lbCypress},
       {":/resources/tehnologije/resources/tehnologije/devops.png", ui->lbDevOps},
       {":/resources/tehnologije/resources/tehnologije/django.png", ui->lbDjango},
       {":/resources/tehnologije/resources/tehnologije/docker.png", ui->lbDocker},
       {":/resources/tehnologije/resources/tehnologije/dotNet.png", ui->lbNet},
       {":/resources/tehnologije/resources/tehnologije/embeded.png", ui->lbEmbeded},
       {":/resources/tehnologije/resources/tehnologije/git.png", ui->lbGit},
       {":/resources/tehnologije/resources/tehnologije/ios.png", ui->lbIos},
       {":/resources/tehnologije/resources/tehnologije/jenkins.png", ui->lbJenkins},
       {":/resources/tehnologije/resources/tehnologije/linux.png", ui->lbLinux},
       {":/resources/tehnologije/resources/tehnologije/macos.png", ui->lbMacOs},
       {":/resources/tehnologije/resources/tehnologije/oracle.png", ui->lbOracle},
       {":/resources/tehnologije/resources/tehnologije/powershell.png", ui->lbPowerShell},
       {":/resources/tehnologije/resources/tehnologije/qt.png", ui->lbQt},
       {":/resources/tehnologije/resources/tehnologije/unity.png", ui->lbUnity},
       {":/resources/tehnologije/resources/tehnologije/vmware.png", ui->lbVmWare},
       {":/resources/tehnologije/resources/tehnologije/wordpress.png", ui->lbWordPress},
       {":/resources/tehnologije/resources/tehnologije/apache.png", ui->lbApache}};

  for (const ImageInfo &image : images) {
    QPixmap pix(image.path);
    int w = image.label->width();
    int h = image.label->height();
    image.label->setPixmap(pix.scaled(w, h, Qt::KeepAspectRatio));
  }
}

NapraviNalogDeveloper::~NapraviNalogDeveloper() { delete ui; }

void NapraviNalogDeveloper::kliknutoNastaviKaTehnologijama() {
  const std::unique_ptr<ObradaInterfejsa<Ui::NapraviNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::NapraviNalogDeveloper>);
  if (!obrada->ispravniPodaci(ui)) {
    obrada->upozorenje("Morate uneti podatke u sva polja!");
  } else if (!obrada->ispravanBrojTelefona(ui)) {
    ui->leBrojTelefona->setText("Neispravan format");
  } else if (!obrada->izabranPol(ui)) {
    obrada->upozorenje("Morate izabrati pol!");
  } else
  {
    proveraUsername();
  }
}

void NapraviNalogDeveloper::kliknutoNastaviKaIskustvu() {
  const std::unique_ptr<ObradaInterfejsa<Ui::NapraviNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::NapraviNalogDeveloper>);
  const QVector<QString> v = obrada->dohvatiTehnologije(ui);

  (v.size() < 3) ? obrada->upozorenje("Morate izabrati najmanje 3 tehnologije!") : ui->stackedWidget->setCurrentIndex(2);
}

void NapraviNalogDeveloper::kliknutoNastaviKaPlati() {
  const std::unique_ptr<ObradaInterfejsa<Ui::NapraviNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::NapraviNalogDeveloper>);
  const QMap<QString, unsigned> m = obrada->dohvatiBrojGodinaIskustva(ui);

  (m.empty()) ? obrada->upozorenje("Morate imati iskustva u barem jednom jeziku!") : ui->stackedWidget->setCurrentIndex(3);
}

void NapraviNalogDeveloper::kliknutoNastaviKaVestinama() {
  const std::unique_ptr<ObradaInterfejsa<Ui::NapraviNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::NapraviNalogDeveloper>);
  if (!obrada->izabranNacinRada(ui)) {
    obrada->upozorenje("Morate izabrati nacin rada!");
  } else if (!obrada->izabranaStrucnaSprema(ui)) {
    obrada->upozorenje("Morate izabrati stepen stucne spreme!");
  } else {
    ui->stackedWidget->setCurrentIndex(4);
  }
}

void NapraviNalogDeveloper::kliknutoNastaviKaBenefitima() {
  const std::unique_ptr<ObradaInterfejsa<Ui::NapraviNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::NapraviNalogDeveloper>);
  const QVector<QString> v = obrada->dohvatiVestine(ui);

  (v.empty()) ? obrada->upozorenje("Morate izabrati vestinu!") : ui->stackedWidget->setCurrentIndex(5);
}

inline void NapraviNalogDeveloper::kliknutoNazadNaVestine() { ui->stackedWidget->setCurrentIndex(4); }

inline void NapraviNalogDeveloper::kliknutoNazadNaOpsegPlate() { ui->stackedWidget->setCurrentIndex(3); }

inline void NapraviNalogDeveloper::kliknutoNazadNaIskustvo() { ui->stackedWidget->setCurrentIndex(2); }

inline void NapraviNalogDeveloper::kliknutoNazadNaTehnologije() { ui->stackedWidget->setCurrentIndex(1); }

inline void NapraviNalogDeveloper::kliknutoNazadNaPodatke() { ui->stackedWidget->setCurrentIndex(0); }

inline void NapraviNalogDeveloper::promenjenaVrednostMinPlate(int value) { ui->prbOpsegPlate1->setValue(value); }

void NapraviNalogDeveloper::kliknutoNazadNaLogIn() {
  auto *login = new LogIn;
  login->show();
  this->close();
}

void NapraviNalogDeveloper::kliknutoZavrsiRegistraciju() {
  auto *w = new MainWindowDeveloper;
  const std::unique_ptr<ObradaInterfejsa<Ui::NapraviNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::NapraviNalogDeveloper>);
  w->postaviUsername(obrada->dohvatiUsername(ui));
  emit w->windowLoaded();
  w->show();
}

void NapraviNalogDeveloper::zatvoriProzor() { this->close(); }

void NapraviNalogDeveloper::proveraUsername() {
  const std::unique_ptr<ObradaInterfejsa<Ui::NapraviNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::NapraviNalogDeveloper>);
  QString filePath = "developer";
  filePath = filePath + QDir::separator();
  filePath = filePath + obrada->dohvatiUsername(ui);
  filePath = filePath + ".json";

  auto *usernameTestThread = new UsernameTestThread(filePath);

  connect(usernameTestThread, &UsernameTestThread::directoryFound, this, &NapraviNalogDeveloper::directoryNotFound);
  connect(usernameTestThread, &UsernameTestThread::directoryNotFound, this, &NapraviNalogDeveloper::usernameDobar);

  usernameTestThread->start();

  connect(usernameTestThread, &UsernameTestThread::finished, usernameTestThread, &QObject::deleteLater);
}

Developer *NapraviNalogDeveloper::zavrsenaRegistracija() {
  const std::unique_ptr<ObradaInterfejsa<Ui::NapraviNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::NapraviNalogDeveloper>);
  QString username = obrada->dohvatiUsername(ui);
  QString sifra = obrada->dohvatiSifru(ui);
  QString ime = obrada->dohvatiIme(ui);
  QString prezime = obrada->dohvatiPrezime(ui);
  QString brTelefona = obrada->dohvatiBrTelefona(ui);
  char pol = obrada->dohvatiPol(ui);
  QVector<QString> tehnologije = obrada->dohvatiTehnologije(ui);
  QMap<QString, unsigned> brGodIskustva = obrada->dohvatiBrojGodinaIskustva(ui);
  unsigned plata = obrada->dohvatiPlatu(ui);
  NacinRada nr = obrada->dohvatiNacinRada(ui);
  StrucnaSprema ss = obrada->dohvatiObrazovanje(ui);
  QVector<QString> vestine = obrada->dohvatiVestine(ui);
  QVector<QString> benefiti = obrada->dohvatiBenefite(ui);

  return new Developer(ime, prezime, pol, tehnologije, brGodIskustva, plata, nr, vestine, benefiti, ss, username, sifra, brTelefona);
}

void NapraviNalogDeveloper::directoryFound(const QString &directory) {
  Developer *d = zavrsenaRegistracija();

  JSONSerializer json;
  json.save(*d, directory);
}

void NapraviNalogDeveloper::directoryNotFound() {
  const std::unique_ptr<ObradaInterfejsa<Ui::NapraviNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::NapraviNalogDeveloper>);
  obrada->upozorenje("Ovaj username je vec zauzet!");
}

void NapraviNalogDeveloper::usernameDobar() { ui->stackedWidget->setCurrentIndex(1); }

void NapraviNalogDeveloper::sacuvaj() {

  const std::unique_ptr<ObradaInterfejsa<Ui::NapraviNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::NapraviNalogDeveloper>);
  QString filePath = "developer";
  filePath = filePath + QDir::separator();
  filePath = filePath + obrada->dohvatiUsername(ui);
  filePath = filePath + ".json";
  SearchThread *searchThread = new SearchThread(filePath);

  connect(searchThread, &SearchThread::directoryFound, this, &NapraviNalogDeveloper::directoryFound);

  searchThread->start();

  connect(searchThread, &SearchThread::finished, searchThread, &QObject::deleteLater);
}
