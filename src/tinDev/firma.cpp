#include "firma.h"

Firma::Firma() {}

Firma::Firma(const QString &ime, const QString &adresa, const QString &brTelefona, const QString &email, const QString &username, const QString &sifra)
     : m_username(username), m_sifra(sifra), m_ime(ime), m_adresa(adresa), m_brTelefona(brTelefona), m_email(email) {}

QVariant Firma::toVariant() const {
  QVariantMap result;
  result["username"] = m_username;
  result["sifra"] = m_sifra;
  result["ime"] = m_ime;
  result["adresa"] = m_adresa;
  result["brTelefona"] = m_brTelefona;
  result["email"] = m_email;

  return result;
}

void Firma::fromVariant(const QVariant &variant) {
  QVariantMap map = variant.toMap();

  m_username = map["username"].toString();
  m_sifra = map["sifra"].toString();
  m_ime = map["ime"].toString();
  m_adresa = map["adresa"].toString();
  m_brTelefona = map["brTelefona"].toString();
  m_email = map["email"].toString();
}

inline void setUsername(Firma &firma, const QString &newUsername) { firma.m_username = newUsername; }

inline void setSifra(Firma &firma, const QString &newSifra) { firma.m_sifra = newSifra; }

inline void setIme(Firma &firma, const QString &newIme) { firma.m_ime = newIme; }

inline void setAdresa(Firma &firma, const QString &newAdresa) { firma.m_adresa = newAdresa; }

inline void setBrTelefona(Firma &firma, const QString &newBrTelefona) { firma.m_brTelefona = newBrTelefona; }

inline void setEmail(Firma &firma, const QString &newEmail) { firma.m_email = newEmail; }
