#include "rewritethread.h"
#include <QDebug>
#include <QDir>

RewriteThread::RewriteThread(const QString &targetFileName, QObject *parent) : QThread(parent), targetFileName(targetFileName) {}

void RewriteThread::run() {
  try {
    QDir userDir(QDir::homePath());
    foundPath = searchForDirectory(userDir);

    if (!foundPath.isEmpty()) {
      foundPath = foundPath + QDir::separator() + "src" + QDir::separator() + "tinDev" + QDir::separator() + "resources" + QDir::separator() + "serialization" +
                  QDir::separator() + targetFileName;

      QFile file(foundPath);
      if (file.open(QIODevice::WriteOnly | QFile::Truncate)) {
        file.close();
      }
    }
  } catch (const std::exception &e) {
    qDebug() << "Exception occurred in thread:" << e.what();
  } catch (...) {
    qDebug() << "Unknown exception occurred in thread.";
  }

  this->deleteLater();
}

QString RewriteThread::searchForDirectory(const QDir &currentDir) {
  QStringList subdirs = currentDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
  if (subdirs.contains("tinDev")) {
    QString foundPath = currentDir.filePath("tinDev");
    return foundPath;
  }

  for (const QString &subdir : subdirs) {
    QDir nextDir = currentDir;
    nextDir.cd(subdir);
    QString result = searchForDirectory(nextDir);
    if (!result.isEmpty()) {
      return result;
    }
  }

  return QString("");
}
