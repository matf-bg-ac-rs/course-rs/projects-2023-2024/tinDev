#pragma once
#include "iusernameprovider.h"
#include "ui_mainwindowdeveloper.h"
#include "oglas.h"
#include <QMainWindow>
#include <QVector>

namespace Ui {
class MainWindowDeveloper;
}

class MainWindowDeveloper : public QMainWindow, public IUsernameProvider {
  Q_OBJECT

public:
  explicit MainWindowDeveloper(QWidget *parent = nullptr);
  ~MainWindowDeveloper();

  Ui::MainWindowDeveloper *getUi();
  void postaviUsername(const QString &username);
  void otvoriChatProzor();
  void pronadjenDirektorijumDeveloper(const QString &dir);
  void postavljanjeOglasa();
  void ukloniPodatke();
  void sledeciOglas();
  bool krajOglasa();

  void pronadjenDeveloper(const QString &dir);
  void pronadjenDirektorijumFirmaSvidjanja(const QString &dir);

  void threadNadjiFrimaSvidjanja(const QString &ime_firme);

signals:
  void windowLoaded();

private slots:
  void izmeniNalog();
  void directoryFoundOglas(const QVector<QString> &directory);
  void sviOglasi();
  void dislike();
  void like();
  void windowLoaded2PR();

  void directoryFoundSvidjanja(const QString &directory);
  void sacuvajSvidjanja();
  void krajSvajpovanja();

private:
  Ui::MainWindowDeveloper *ui;
  QVector<Oglas *> m_oglasi;
  QVector<Oglas *> m_svidjanja;
  void zapoceteIzmene();
  void racunajProcenat();

public:
  QString getUsername() const;
};
