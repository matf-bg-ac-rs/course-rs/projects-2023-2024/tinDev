#include "mainwindowdeveloper.h"
#include "../clientServer/chatroom.h"
#include "../serialization/jsonserializer.h"
#include "developer.h"
#include "izmeninalogdeveloper.h"
#include "loginthread.h"
#include "match.h"
#include "obradainterfejsa.h"
#include "procenat.h"
#include "readfilethread.h"
#include "rewritethread.h"
#include "searchthread.h"
#include "ui_mainwindowdeveloper.h"
#include "usernamemanager.h"
#include <QFile>
#include <QFontDatabase>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>

MainWindowDeveloper::MainWindowDeveloper(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindowDeveloper) {
  ui->setupUi(this);

  connect(ui->pbChat, &QPushButton::clicked, this, &MainWindowDeveloper::otvoriChatProzor);
  connect(ui->pbIzmeniNalog, &QPushButton::clicked, this, &MainWindowDeveloper::izmeniNalog);
  connect(this, &MainWindowDeveloper::windowLoaded, this, &MainWindowDeveloper::sviOglasi);
  connect(ui->pbDislike, &QPushButton::clicked, this, &MainWindowDeveloper::dislike);
  connect(ui->pbLike, &QPushButton::clicked, this, &MainWindowDeveloper::like);

  int fontId = QFontDatabase::addApplicationFont(":/resource/font/resources/font/Humaroid.otf");
  if (fontId != -1) {
    QString fontName = QFontDatabase::applicationFontFamilies(fontId).at(0);

    QFont font(fontName);
    QFont fontLabele(fontName);
    QFont fontUsername(fontName);
    font.setPointSize(20);
    fontLabele.setPointSize(22);
    fontUsername.setPointSize(28);

    ui->lbTinDev->setFont(font);
    ui->lbZdravo->setFont(fontLabele);
    ui->leUsername->setFont(fontUsername);
  }
  UsernameManager::setUsernameProvider(this);
}

MainWindowDeveloper::~MainWindowDeveloper() {
  qDeleteAll(m_oglasi);
  m_oglasi.clear();

  qDeleteAll(m_svidjanja);
  m_svidjanja.clear();
  delete ui;
}

Ui::MainWindowDeveloper *MainWindowDeveloper::getUi() { return ui; }

void MainWindowDeveloper::otvoriChatProzor() {
  ChatRoom *chat = new ChatRoom();
  chat->show();
}

void MainWindowDeveloper::ukloniPodatke() {
  ui->lbNazivFirme->setText("");
  ui->lbPozicija->setText("");
  ui->lbNacinRada->setText("");
  ui->lbPlata->setText("");
  ui->lbStrucnaSprema->setText("");
  ui->tbJezici->clear();
  ui->tbBenefiti->clear();
  ui->tbTehnologije->clear();
  ui->tbVestine->clear();
}

void MainWindowDeveloper::sledeciOglas() { m_oglasi.removeAt(0); }

bool MainWindowDeveloper::krajOglasa() { return m_oglasi.size() > 1; }

void MainWindowDeveloper::pronadjenDeveloper(const QString &dir) {
  Developer *d = new Developer;
  JSONSerializer json;
  json.load(*d, dir);

  for (Oglas *o : m_oglasi) {
    Procenat *p = new Procenat(d, o);
    double procenat = p->uporediPoklapanjeDevOglas();

    o->setProcenat(procenat);
    delete p;
  }
  if (!m_oglasi.isEmpty()) {
    ukloniPodatke();
    postavljanjeOglasa();
  }
  delete d;
}

void MainWindowDeveloper::postaviUsername(const QString &username) { ui->leUsername->setText(username); }

void MainWindowDeveloper::postavljanjeOglasa() {
  ui->lbNazivFirme->setText(m_oglasi.first()->usernameFirma());
  ui->lbPlata->setText(QString::number(m_oglasi.first()->plata()));
  ui->lbPozicija->setText(m_oglasi.first()->nazivPozicije());
  ui->lbStrucnaSprema->setText(m_oglasi.first()->stepenStrucneSpremeStr());
  ui->lbNacinRada->setText(m_oglasi.first()->nacinRadaStr());
  double x = m_oglasi.first()->procenatPoklapanja();
  ui->progressBar->setValue((x > 0) ? x : 40);

  const QMap<QString, unsigned int> &mapaJezika = m_oglasi.first()->brGodIskustva();
  if (!mapaJezika.isEmpty()) {
    for (auto it = mapaJezika.begin(); it != mapaJezika.end(); ++it) {
      QString kljuc = it.key();
      unsigned int vrednost = it.value();
      ui->tbJezici->append(kljuc + " " + QString::number(vrednost));
    }
  }

  for (QString &teh : m_oglasi.first()->tehnologije()) {
    ui->tbTehnologije->append(teh);
  }
  for (QString &ben : m_oglasi.first()->benefiti()) {
    ui->tbBenefiti->append(ben);
  }
  for (QString &vest : m_oglasi.first()->vestine()) {
    ui->tbVestine->append(vest);
  }
}

void MainWindowDeveloper::dislike() {
  const std::unique_ptr<ObradaInterfejsa<Ui::MainWindowDeveloper>> obrada(new ObradaInterfejsa<Ui::MainWindowDeveloper>);
  if (krajOglasa() == true) {
    sledeciOglas();
    ukloniPodatke();
    postavljanjeOglasa();
  } else {
    if (m_oglasi.size() == 1) {
      sledeciOglas();
      krajSvajpovanja();
    }

    obrada->upozorenje("Pregledali ste sve oglase!");
  }
}

void MainWindowDeveloper::like() {
  const std::unique_ptr<ObradaInterfejsa<Ui::MainWindowDeveloper>> obrada(new ObradaInterfejsa<Ui::MainWindowDeveloper>);
  if (krajOglasa()) {
    m_svidjanja.push_back(m_oglasi[0]);
    sacuvajSvidjanja();

    Oglas *o = m_svidjanja.last();
    QString ime_firme = o->usernameFirma();
    threadNadjiFrimaSvidjanja(ime_firme);

    sledeciOglas();
    ukloniPodatke();
    postavljanjeOglasa();
  } else {
    if (m_oglasi.size() == 1) {
      m_svidjanja.push_back(m_oglasi[0]);
      sacuvajSvidjanja();

      Oglas *o = m_svidjanja.last();
      QString ime_firme = o->usernameFirma();
      threadNadjiFrimaSvidjanja(ime_firme);
      krajSvajpovanja();

      sledeciOglas();
    }

    obrada->upozorenje("Pregledali ste sve oglase!");
  }
}

void MainWindowDeveloper::pronadjenDirektorijumFirmaSvidjanja(const QString &dir) {
  const std::unique_ptr<ObradaInterfejsa<Ui::MainWindowDeveloper>> obrada(new ObradaInterfejsa<Ui::MainWindowDeveloper>);

  QFileInfo fileInfo(dir);
  QString imeFirmeDevelopera = fileInfo.baseName();

  QString targetUsername = ui->leUsername->text();

  Match *m = new Match(dir, targetUsername, imeFirmeDevelopera);
  m->proveriMatch();

  if (m->indikator()) {
    QString porukaMatch = "Čestitamo " + targetUsername + " imate MATCH sa " + imeFirmeDevelopera;
    obrada->upozorenje(porukaMatch);
  }

  delete m;
}

void MainWindowDeveloper::threadNadjiFrimaSvidjanja(const QString &ime_firme) {
  QString filePath = "frimaSvidjanja";
  filePath = filePath + QDir::separator();
  filePath = filePath + ime_firme;
  filePath = filePath + ".json";

  SearchThread *searchThread = new SearchThread(filePath);

  connect(searchThread, &SearchThread::directoryFound, this, &MainWindowDeveloper::pronadjenDirektorijumFirmaSvidjanja);
  searchThread->start();
  connect(searchThread, &SearchThread::finished, searchThread, &QObject::deleteLater);
}

void MainWindowDeveloper::directoryFoundSvidjanja(const QString &directory) {
  Oglas *o = m_svidjanja.last();
  JSONSerializer json;
  json.save(*o, directory);
}

void MainWindowDeveloper::sacuvajSvidjanja() {
  const std::unique_ptr<ObradaInterfejsa<Ui::MainWindowDeveloper>> obrada(new ObradaInterfejsa<Ui::MainWindowDeveloper>);
  QString filePath = "developerSvidjanja";
  filePath = filePath + QDir::separator();
  filePath = filePath + obrada->dohvatiUsername(ui);
  filePath = filePath + ".json";

  SearchThread *searchThread = new SearchThread(filePath);

  connect(searchThread, &SearchThread::directoryFound, this, &MainWindowDeveloper::directoryFoundSvidjanja);

  searchThread->start();

  connect(searchThread, &SearchThread::finished, searchThread, &QObject::deleteLater);
}

void MainWindowDeveloper::krajSvajpovanja() {
  const std::unique_ptr<ObradaInterfejsa<Ui::MainWindowDeveloper>> obrada(new ObradaInterfejsa<Ui::MainWindowDeveloper>);

  QString filePath = "developerSvidjanja";
  filePath = filePath + QDir::separator();
  filePath = filePath + obrada->dohvatiUsername(ui);
  filePath = filePath + ".json";

  RewriteThread *rewriteThread = new RewriteThread(filePath);

  rewriteThread->start();

  connect(rewriteThread, &RewriteThread::finished, rewriteThread, &QObject::deleteLater);
}

void MainWindowDeveloper::izmeniNalog() { zapoceteIzmene(); }

void MainWindowDeveloper::directoryFoundOglas(const QVector<QString> &directory) {
  for (int j = 0; j < directory.size(); j++) {
    Oglas *o = new Oglas();
    JSONSerializer json;
    json.load(*o, directory[j]);

    m_oglasi.push_back(o);
  }

  racunajProcenat();
}

void MainWindowDeveloper::sviOglasi() {
  const std::unique_ptr<ObradaInterfejsa<Ui::MainWindowDeveloper>> obrada(new ObradaInterfejsa<Ui::MainWindowDeveloper>);

  QString filePath = "oglas";

  ReadFileThread *thread = new ReadFileThread(filePath);

  connect(thread, &ReadFileThread::directoryFound, this, &MainWindowDeveloper::directoryFoundOglas);
  thread->start();

  connect(thread, &ReadFileThread::finished, thread, &QObject::deleteLater);
}

void MainWindowDeveloper::pronadjenDirektorijumDeveloper(const QString &dir) {
  const std::unique_ptr<ObradaInterfejsa<Ui::IzmeniNalogDeveloper>> obrada(new ObradaInterfejsa<Ui::IzmeniNalogDeveloper>);

  Developer *d = new Developer;
  JSONSerializer json;
  json.load(*d, dir);

  IzmeniNalogDeveloper *izmena = new IzmeniNalogDeveloper();
  izmena->postaviUsername(ui->leUsername->text());
  izmena->postaviOsnovnePodatke(d->ime(), d->prezime(), d->pol(), d->sifra(), d->brTelefona());
  izmena->postaviTehnologije(d->tehnologije());
  izmena->postaviBrojGodinaIskustva(d->brGodinaIskustva());
  izmena->postaviPlatu(d->opsegPlate());
  izmena->postaviNacinRada(d->nacinRada());
  izmena->postaviStrucnuSpremu(d->nivoObrazovanja());
  izmena->postaviVestine(d->vestine());
  izmena->postaviBenefite(d->benefiti());
  izmena->show();

  connect(izmena, &IzmeniNalogDeveloper::windowLoaded, this, &MainWindowDeveloper::windowLoaded2PR);

  delete d;
}

void MainWindowDeveloper::windowLoaded2PR() {
  for (int i = 0; i < m_oglasi.size(); ++i) {
    m_oglasi.removeAt(i);
    --i;
  }
  for (int i = 0; i < m_oglasi.size(); ++i) {
    m_svidjanja.removeAt(i);
    --i;
  }
  ukloniPodatke();
  emit windowLoaded();
}

void MainWindowDeveloper::zapoceteIzmene() {
  const std::unique_ptr<ObradaInterfejsa<Ui::MainWindowDeveloper>> obrada(new ObradaInterfejsa<Ui::MainWindowDeveloper>);
  QString username = obrada->dohvatiUsername(ui);
  QString filePath = "developer";
  filePath = filePath + QDir::separator();
  filePath = filePath + username;
  filePath = filePath + ".json";

  loginthread *thread = new loginthread(filePath);

  connect(thread, &loginthread::directoryFound, this, &MainWindowDeveloper::pronadjenDirektorijumDeveloper);
  thread->start();

  connect(thread, &loginthread::finished, thread, &QObject::deleteLater);
}

void MainWindowDeveloper::racunajProcenat() {
  const std::unique_ptr<ObradaInterfejsa<Ui::MainWindowDeveloper>> obrada(new ObradaInterfejsa<Ui::MainWindowDeveloper>);
  QString username = obrada->dohvatiUsername(ui);
  QString filePath = "developer";
  filePath = filePath + QDir::separator();
  filePath = filePath + username;
  filePath = filePath + ".json";

  loginthread *thread = new loginthread(filePath);

  connect(thread, &loginthread::directoryFound, this, &MainWindowDeveloper::pronadjenDeveloper);
  thread->start();

  connect(thread, &loginthread::finished, thread, &QObject::deleteLater);
}

QString MainWindowDeveloper::getUsername() const { return ui->leUsername->text(); }
