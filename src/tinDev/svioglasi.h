#pragma once
#include <QListWidgetItem>
#include "ui_svioglasi.h"
#include <QWidget>

namespace Ui {
class SviOglasi;
}

class SviOglasi : public QWidget {
  Q_OBJECT
signals:
  void windowLoaded();

public:
  explicit SviOglasi(QWidget *parent = nullptr);
  ~SviOglasi();

  Ui::SviOglasi *getUi();
  void postaviUsername(const QString &username);
  void postaviOglas(const QString &id);
  void izabranOglasZaBrisanje();

private slots:
  void izmeniOglas();
  void pronadjenDirektorijumOglas(const QString &dir);
  void pronadjenDirektorijumOglasZaBrisanje(const QString &dir);
  void windowLoaded2PR();

private:
  Ui::SviOglasi *ui;
  void zapoceteIzmene();
  void zapocetoBrisanje();
};
