#pragma once
#include <QDir>
#include <QThread>

class RewriteThread : public QThread {
  Q_OBJECT
public:
  RewriteThread(const QString &targetFileName, QObject *parent = nullptr);

  QString getFoundPath() const;

protected:
  void run() override;

private:
  const QString targetFileName;
  QString foundPath;

  QString searchForDirectory(const QDir &currentDir);
};
