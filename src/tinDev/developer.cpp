#include "developer.h"

Developer::Developer() {}

Developer::Developer(
     const QString &ime, const QString &prezime, char pol, const QVector<QString> &tehnologije, const QMap<QString, unsigned int> &brGodinaIskustva,
     unsigned opsegPlate, NacinRada &nacinRada, const QVector<QString> &vestine, const QVector<QString> &benefiti, StrucnaSprema nivoObrazovanja,
     const QString &username, const QString &sifra, const QString &brTelefona)
     : m_username(username), m_sifra(sifra), m_ime(ime), m_prezime(prezime), m_brTelefona(brTelefona), m_pol(pol), m_tehnologije(tehnologije),
       m_brGodinaIskustva(brGodinaIskustva), m_opsegPlate(opsegPlate), m_nacinRada(nacinRada), m_vestine(vestine), m_benefiti(benefiti),
       m_nivoObrazovanja(nivoObrazovanja) {}

QVariant Developer::toVariant() const {
  QVariantMap result;

  result["username"] = m_username;
  result["sifra"] = m_sifra;
  result["ime"] = m_ime;
  result["prezime"] = m_prezime;
  result["brTelefona"] = m_brTelefona;
  result["pol"] = QString(m_pol);
  result["tehnologije"] = QVariant::fromValue<QVector<QString>>(m_tehnologije);
  QVariantMap vMap;
  for (auto it = m_brGodinaIskustva.begin(); it != m_brGodinaIskustva.end(); ++it) {
    vMap[it.key()] = it.value();
  }
  result["brGodinaIskustva"] = vMap;
  result["opsegPlate"] = m_opsegPlate;
  result["nacinRada"] = static_cast<int>(m_nacinRada);
  result["vestine"] = QVariant::fromValue<QVector<QString>>(m_vestine);
  result["benefiti"] = QVariant::fromValue<QVector<QString>>(m_benefiti);
  result["nivoObrazovanja"] = static_cast<int>(m_nivoObrazovanja);

  return result;
}

void Developer::fromVariant(const QVariant &variant) {
  QVariantMap map = variant.toMap();

  m_username = map["username"].toString();
  m_sifra = map["sifra"].toString();
  m_ime = map["ime"].toString();
  m_prezime = map["prezime"].toString();
  m_brTelefona = map["brTelefona"].toString();
  m_pol = map["pol"].toString().isEmpty() ? '\0' : map["pol"].toString().at(0).toLatin1();

  m_tehnologije = map["tehnologije"].value<QVector<QString>>();
  QVariantMap vMap = map["brGodinaIskustva"].toMap();
  QMap<QString, unsigned> deserializedMap;

  for (auto it = vMap.begin(); it != vMap.end(); ++it) {
    deserializedMap[it.key()] = it.value().value<unsigned>();
  }
  m_brGodinaIskustva = deserializedMap;
  m_opsegPlate = map["opsegPlate"].toUInt();
  m_nacinRada = static_cast<NacinRada>(map["nacinRada"].toInt());
  m_vestine = map["vestine"].value<QVector<QString>>();
  m_benefiti = map["benefiti"].value<QVector<QString>>();
  m_nivoObrazovanja = static_cast<StrucnaSprema>(map["nivoObrazovanja"].toInt());
}
QString Developer::stepenStrucneSpremeStr() const {
  switch (m_nivoObrazovanja) {

  case StrucnaSprema::SrednjaSkola:
    return "Srednja skola";
  case StrucnaSprema::OsnovneStudije:
    return "Osnovne studije";
  case StrucnaSprema::MasterStudije:
    return "Master studije";
  case StrucnaSprema::DoktorskeStudije:
    return "Doktorske studije";
  default:
    throw std::exception("Nepoznato obrazovanje");
  }
}

QString Developer::nacinRadaStr() const {
  switch (m_nacinRada) {
  case NacinRada::Hybrid:
    return "Hybrid";
  case NacinRada::Office:
    return "Office";
  case NacinRada::Remote:
    return "Remote";
  default:
    throw std::exception("Nepoznat nacin rada");
  }
}

inline void setUsername(Developer &dev, const QString &newUsername) { dev.m_username = newUsername; }
inline void setSifra(Developer &dev, const QString &newSifra) { dev.m_sifra = newSifra; }
inline void setIme(Developer &dev, const QString &newIme) { dev.m_ime = newIme; }
inline void setPrezime(Developer &dev, const QString &newPrezime) { dev.m_prezime = newPrezime; }
inline void setPol(Developer &dev, char newPol) { dev.m_pol = newPol; }
inline void setTehnologije(Developer &dev, const QVector<QString> &newTehnologije) { dev.m_tehnologije = newTehnologije; }
inline void setBrGodinaIskustva(Developer &dev, const QMap<QString, unsigned int> &newBrGodinaIskustva) { dev.m_brGodinaIskustva = newBrGodinaIskustva; }
inline void setOpsegPlate(Developer &dev, unsigned &newOpsegPlate) { dev.m_opsegPlate = newOpsegPlate; }
inline void setNacinRada(Developer &dev, NacinRada newNacinRada) { dev.m_nacinRada = newNacinRada; }
inline void setVestine(Developer &dev, const QVector<QString> &newVestine) { dev.m_vestine = newVestine; }
inline void setBenefiti(Developer &dev, const QVector<QString> &newBenefiti) { dev.m_benefiti = newBenefiti; }
inline void setNivoObrazovanja(Developer &dev, StrucnaSprema newNivoObrazovanja) { dev.m_nivoObrazovanja = newNivoObrazovanja; }
