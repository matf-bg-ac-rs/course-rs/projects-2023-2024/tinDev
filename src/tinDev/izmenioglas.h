#pragma once
#include "enums.h"
#include "oglas.h"
#include "ui_izmenioglas.h"
#include <QWidget>

namespace Ui {
class IzmeniOglas;
}

class IzmeniOglas : public QWidget {
  Q_OBJECT

public:
  explicit IzmeniOglas(QWidget *parent = nullptr);
  ~IzmeniOglas();

  Ui::IzmeniOglas *getUi();
  void postaviId(const QString &id);
  void postaviNazivPozicije(const QString &nazivPozicije);
  void postaviNazivFirme(const QString &usernameFirma);
  void postaviPlatu(const unsigned &plata);
  void postaviNacinRada(const NacinRada);
  void postaviStrucnuSpremu(const StrucnaSprema);
  void postaviTehnologije(const QVector<QString> &tehnologije);
  void postaviBrojGodinaIskustva(const QMap<QString, unsigned> &iskustvo);
  void postaviVestine(const QVector<QString> &vestine);
  void postaviBenefite(const QVector<QString> &benefiti);

signals:
  void windowLoaded();

private slots:
  void kliknutoNastaviKaTehnologijama();
  void kliknutoNastaviKaIskustvu();
  void kliknutoNastaviKaVestinama();

  void kliknutoPotvrdiIzmene();
  void kliknutoPonistiIzmene();

  void NazadNaOsnovnePodatke();
  void NazadNaTehnologije();
  void NazadNaIskustvo();

  void pronadjenDirektorijumOglas(const QString &dir);

private:
  Ui::IzmeniOglas *ui;
  Oglas *noveIzmene();
  void zavrsiIzmene();
};
