#include "dodajoglas.h"
#include "../serialization/jsonserializer.h"
#include "obradainterfejsa.h"
#include "searchthread.h"
#include "ui_dodajoglas.h"

DodajOglas::DodajOglas(QWidget *parent) : QWidget(parent), ui(new Ui::DodajOglas) {
  ui->setupUi(this);

  connect(ui->pbDalje, &QPushButton::clicked, this, &DodajOglas::kliknutoDaljeNaTehnologije);
  connect(ui->pbDalje2, &QPushButton::clicked, this, &DodajOglas::kliknutoDaljeNaJezike);
  connect(ui->pbDalje3, &QPushButton::clicked, this, &DodajOglas::kliknutoDaljeNaBenefite);
  connect(ui->pbNazad1, &QPushButton::clicked, this, &DodajOglas::kliknutoNadzadNaPlatu);
  connect(ui->pbNazad2, &QPushButton::clicked, this, &DodajOglas::kliknutoNadzadNaTehnologije);
  connect(ui->pbNazad3, &QPushButton::clicked, this, &DodajOglas::kliknutoNadzadNaJezike);
  connect(ui->pbZavrsi, &QPushButton::clicked, this, &DodajOglas::zavrsiPravljenjeOglasa);
}

DodajOglas::~DodajOglas() { delete ui; }

void DodajOglas::kliknutoDaljeNaTehnologije() {
  const std::unique_ptr<ObradaInterfejsa<Ui::DodajOglas>> obrada(new ObradaInterfejsa<Ui::DodajOglas>);
  if (!obrada->ispravnaPlata(ui)) {
    obrada->upozorenje("Morate uneti platu u ispranom formatu!");
  } else if (obrada->dohvatiPoziciju(ui).isEmpty()) {
    obrada->upozorenje("Morate uneti naziv pozicije!");
  } else if (!obrada->izabranNacinRada(ui)) {
    obrada->upozorenje("Morate izabrati nacin rada!");
  } else if (!obrada->izabranaStrucnaSprema(ui)) {
    obrada->upozorenje("Morate odabrati strucnu spremu!");
  } else {
    ui->stackedWidget->setCurrentIndex(1);
  }
}

void DodajOglas::kliknutoDaljeNaJezike() {
  const std::unique_ptr<ObradaInterfejsa<Ui::DodajOglas>> obrada(new ObradaInterfejsa<Ui::DodajOglas>);
  const QVector<QString> v = obrada->dohvatiTehnologije(ui);

  (v.empty()) ? obrada->upozorenje("Morate izabrati barem jednu tehnologiju!") : ui->stackedWidget->setCurrentIndex(2);
}

void DodajOglas::kliknutoDaljeNaBenefite() {
  const std::unique_ptr<ObradaInterfejsa<Ui::DodajOglas>> obrada(new ObradaInterfejsa<Ui::DodajOglas>);
  const QMap<QString, unsigned> m = obrada->dohvatiBrojGodinaIskustva(ui);

  (m.empty()) ? obrada->upozorenje("Morate izabrati barem jedan programski jezik!") : ui->stackedWidget->setCurrentIndex(3);
}

inline void DodajOglas::kliknutoNadzadNaJezike() { ui->stackedWidget->setCurrentIndex(2); }
inline void DodajOglas::kliknutoNadzadNaTehnologije() { ui->stackedWidget->setCurrentIndex(1); }
inline void DodajOglas::kliknutoNadzadNaPlatu() { ui->stackedWidget->setCurrentIndex(0); }

void DodajOglas::zavrsiPravljenjeOglasa() {
  const std::unique_ptr<ObradaInterfejsa<Ui::DodajOglas>> obrada(new ObradaInterfejsa<Ui::DodajOglas>);
  const QVector<QString> v = obrada->dohvatiVestine(ui);
  if (v.empty()) {
    obrada->upozorenje("Morate izabrati vestinu!");
  } else {
    sacuvaj();
    emit windowLoaded();
    this->close();
  }
}

void DodajOglas::postaviUsername(const QString &username) { ui->lbUsernameFirma->setText(username); }

Ui::DodajOglas *DodajOglas::getUi() { return ui; }

Oglas *DodajOglas::oglasZavrsen() {
  const std::unique_ptr<ObradaInterfejsa<Ui::DodajOglas>> obrada(new ObradaInterfejsa<Ui::DodajOglas>);

  return new Oglas(
       obrada->dohvatiPoziciju(ui), obrada->dohvatiMaxPlatu(ui).toUInt(), obrada->dohvatiNacinRada(ui), obrada->dohvatiObrazovanje(ui),
       obrada->dohvatiTehnologije(ui), obrada->dohvatiBrojGodinaIskustva(ui), obrada->dohvatiVestine(ui), obrada->dohvatiBenefite(ui),
       ui->lbUsernameFirma->text());
}

void DodajOglas::directoryFound(const QString &directory) {
  Oglas *o = oglasZavrsen();
  JSONSerializer json;
  json.save(*o, directory);
  delete o;
}

void DodajOglas::sacuvaj() {

  const std::unique_ptr<ObradaInterfejsa<Ui::DodajOglas>> obrada(new ObradaInterfejsa<Ui::DodajOglas>);
  Oglas *o = oglasZavrsen();
  QString filePath = "oglas";
  filePath = filePath + QDir::separator();
  filePath = filePath + o->id();
  filePath = filePath + ".json";
  delete (o);
  SearchThread *searchThread = new SearchThread(filePath);

  connect(searchThread, &SearchThread::directoryFound, this, &DodajOglas::directoryFound);

  searchThread->start();

  connect(searchThread, &SearchThread::finished, searchThread, &QObject::deleteLater);
}
