#include "match.h"
#include <QFile>
#include <QFontDatabase>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>

Match::Match(const QString &putanja, const QString &targetUsername, const QString &imeFirmeDevelopera)
     : m_putanja(putanja), m_targetUsername(targetUsername), m_imeFirmeDevelopera(imeFirmeDevelopera) {
  if (putanja.isEmpty() || targetUsername.isEmpty() || imeFirmeDevelopera.isEmpty()) {
    throw std::invalid_argument("Nedostaje jedan ili više argumenata");
  }
}

void Match::proveriMatch() {
  QFile file(m_putanja);
  if (!file.open(QIODevice::ReadOnly)) {
    return;
  }

  QByteArray jsonData = file.readAll();
  (jsonData.contains(m_targetUsername.toUtf8())) ? m_indikatorMatch = true : m_indikatorMatch = false;

  file.close();
}
