#include "login.h"

#include <QApplication>

#include <QDebug>
#include <QDir>

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  LogIn login;
  login.show();

  return a.exec();
}
