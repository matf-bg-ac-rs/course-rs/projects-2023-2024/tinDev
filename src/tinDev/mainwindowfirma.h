#pragma once
#include "developer.h"
#include "iusernameprovider.h"
#include "ui_mainwindowfirma.h"
#include "oglas.h"
#include <QMainWindow>
#include <QMap>
#include <QVector>

namespace Ui {
class MainWindowFirma;
}

class MainWindowFirma : public QMainWindow, public IUsernameProvider {
  Q_OBJECT

public:
  explicit MainWindowFirma(QWidget *parent = nullptr);
  ~MainWindowFirma();

  Ui::MainWindowFirma *getUi();
  void postaviUsername(const QString &username);
  void otvoriChatProzor();
  inline QVector<Developer *> developeri() { return m_developeri; }
  inline QVector<Oglas *> oglasi() { return m_oglasi; }
  void postaviDevelopere();
  void ukloniPodatke();
  void sledeciDeveloper();
  bool krajDevelopera();

  void uporediProcente();

  void pronadjenDirektorijumDevSvidjanja(const QString &dir);
  void threadNadjiDevSvidjanja(const QString &ime_developera);

signals:
  void windowLoaded();
  void windowLoaded2();

private slots:
  void dodajOglas();
  void vidiSveOglase();
  void izmeniNalog();
  void directoryFoundFirma(const QString &directory);
  void directoryFoundDeveloper(const QVector<QString> &directory);
  void sviDeveloperi();
  void directoryFoundOglasi(const QVector<QString> &directory);
  void sviOglasi();
  void windowLoaded2PR();

  void like();
  void dislike();
  void directoryFoundSvidjanja(const QString &directory);
  void sacuvajSvidjanja();

  void krajSvajpovanja();

private:
  Ui::MainWindowFirma *ui;
  QVector<Developer *> m_developeri;
  QVector<Oglas *> m_oglasi;
  QVector<Developer *> m_svidjanja;
  void zapoceteIzmene();
  double m_procenatIndikatorMax = 0;
  QVector<double> m_procentiVektor;

public:
  QString getUsername() const;
};
