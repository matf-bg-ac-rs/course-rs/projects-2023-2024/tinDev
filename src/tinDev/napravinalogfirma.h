#pragma once
#include "firma.h"
#include <QDebug>
#include <QMessageBox>
#include <QRegularExpression>
#include <QString>
#include <QWidget>
#include <iostream>

namespace Ui {
class NapraviNalogFirma;
}

class NapraviNalogFirma : public QWidget {
  Q_OBJECT

public:
  explicit NapraviNalogFirma(QWidget *parent = nullptr);
  ~NapraviNalogFirma();

private slots:
  void kliknutoZavrsiRegistraciju();
  void kliknutoNazadNaLogIn();
  void directoryFound(const QString &directory);
  void sacuvaj();

  void proveraUsername();
  void directoryNotFound();
  void usernameDobar();

private:
  Ui::NapraviNalogFirma *ui;
  Firma *zavrsenaRegistracija();
};
