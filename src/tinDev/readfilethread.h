#pragma once
#include <QDir>
#include <QThread>
#include <QVector>

class ReadFileThread : public QThread {
  Q_OBJECT
public:
  explicit ReadFileThread(const QString &targetFileName, QObject *parent = nullptr);
  QString getFoundPath() const;
  QString searchForDirectory(const QDir &currentDir);

signals:
  void directoryFound(const QVector<QString> &directory);

protected:
  void run() override;

private:
  const QString targetFileName;
  QVector<QString> foundPaths;
};
