#include "izmenioglas.h"
#include "../serialization/jsonserializer.h"
#include "obradainterfejsa.h"
#include "searchthread.h"
#include "ui_izmenioglas.h"
#include <QDir>

IzmeniOglas::IzmeniOglas(QWidget *parent) : QWidget(parent), ui(new Ui::IzmeniOglas) {
  ui->setupUi(this);
  connect(ui->pbNastaviDalje, &QPushButton::clicked, this, &IzmeniOglas::kliknutoNastaviKaTehnologijama);
  connect(ui->pbNastaviDalje_2, &QPushButton::clicked, this, &IzmeniOglas::kliknutoNastaviKaIskustvu);
  connect(ui->pbNastaviDalje_3, &QPushButton::clicked, this, &IzmeniOglas::kliknutoNastaviKaVestinama);

  connect(ui->pbNazad, &QPushButton::clicked, this, &IzmeniOglas::kliknutoPonistiIzmene);
  connect(ui->pbNazad_2, &QPushButton::clicked, this, &IzmeniOglas::NazadNaOsnovnePodatke);
  connect(ui->pbNazad_3, &QPushButton::clicked, this, &IzmeniOglas::NazadNaTehnologije);
  connect(ui->pbPonisti, &QPushButton::clicked, this, &IzmeniOglas::NazadNaIskustvo);

  connect(ui->pbPotvrdiIzmene, &QPushButton::clicked, this, &IzmeniOglas::kliknutoPotvrdiIzmene);
  connect(ui->pbPonisti, &QPushButton::clicked, this, &IzmeniOglas::kliknutoPonistiIzmene);
}

IzmeniOglas::~IzmeniOglas() { delete ui; }

Ui::IzmeniOglas *IzmeniOglas::getUi() { return ui; }

void IzmeniOglas::kliknutoNastaviKaTehnologijama() {
  const std::unique_ptr<ObradaInterfejsa<Ui::IzmeniOglas>> obrada(new ObradaInterfejsa<Ui::IzmeniOglas>);
  if (!obrada->ispravnaPlata(ui)) {
    obrada->upozorenje("Morate uneti platu u ispranom formatu!");
  } else if (!obrada->izabranNacinRada(ui)) {
    obrada->upozorenje("Morate izabrati nacin rada!");
  } else if (!obrada->izabranaStrucnaSprema(ui)) {
    obrada->upozorenje("Morate odabrati strucnu spremu!");
  } else {
    ui->stackedWidget->setCurrentIndex(1);
  }
}

void IzmeniOglas::kliknutoNastaviKaIskustvu() {
  const std::unique_ptr<ObradaInterfejsa<Ui::IzmeniOglas>> obrada(new ObradaInterfejsa<Ui::IzmeniOglas>);
  const QVector<QString> v = obrada->dohvatiTehnologije(ui);

  (v.empty()) ? obrada->upozorenje("Morate izabrati barem jednu tehnologiju!") : ui->stackedWidget->setCurrentIndex(2);
}

void IzmeniOglas::kliknutoNastaviKaVestinama() {
  const std::unique_ptr<ObradaInterfejsa<Ui::IzmeniOglas>> obrada(new ObradaInterfejsa<Ui::IzmeniOglas>);
  const QMap<QString, unsigned> m = obrada->dohvatiBrojGodinaIskustva(ui);

  (m.empty()) ? obrada->upozorenje("Morate izabrati barem jedan programski jezik!") : ui->stackedWidget->setCurrentIndex(3);
}

void IzmeniOglas::kliknutoPotvrdiIzmene() {
  zavrsiIzmene();
  emit windowLoaded();
  this->close();
}

inline void IzmeniOglas::kliknutoPonistiIzmene() { this->close(); }

inline void IzmeniOglas::NazadNaOsnovnePodatke() { ui->stackedWidget->setCurrentIndex(0); }
inline void IzmeniOglas::NazadNaTehnologije() { ui->stackedWidget->setCurrentIndex(1); }
inline void IzmeniOglas::NazadNaIskustvo() { ui->stackedWidget->setCurrentIndex(2); }

void IzmeniOglas::postaviId(const QString &id) { ui->lbId->setText(id); }

void IzmeniOglas::postaviNazivPozicije(const QString &nazivPozicije) { ui->lePozicija->setText(nazivPozicije); }

void IzmeniOglas::postaviNazivFirme(const QString &usernameFirma) { ui->leNazivFirme->setText(usernameFirma); }

void IzmeniOglas::postaviPlatu(const unsigned &plata) { ui->lePlata->setText(QString::number(plata)); }

void IzmeniOglas::postaviNacinRada(const NacinRada nacinRada) {
  QVBoxLayout *vl = ui->vlNacinRada;
  auto n = vl->count();

  for (int i = 0; i < n; ++i) {
    QWidget *widget = vl->itemAt(i)->widget();
    if (widget != nullptr) {
      if (QRadioButton *radioButtons = qobject_cast<QRadioButton *>(widget)) {
            auto text = radioButtons->text();
        if (nacinRada == NacinRada::Remote && text == "Remote") {
          radioButtons->setChecked(true);
          return;
        } else if (nacinRada == NacinRada::Office && text == "Office") {
          radioButtons->setChecked(true);
          return;
        } else if (nacinRada == NacinRada::Hybrid && text == "Hybrid") {
          radioButtons->setChecked(true);
          return;
        }
      }
    }
  }

  delete vl;
}

void IzmeniOglas::postaviStrucnuSpremu(const StrucnaSprema strucnaSprema) {
  QVBoxLayout *vl = ui->vlObrazovanje;
  auto n = vl->count();

  for (int i = 0; i < n; ++i) {
    QWidget *widget = vl->itemAt(i)->widget();
    if (widget != nullptr) {
      if (QRadioButton *radioButtons = qobject_cast<QRadioButton *>(widget)) {
        auto text = radioButtons->text();
        if (strucnaSprema == StrucnaSprema::SrednjaSkola && text == "Srednja škola") {
          radioButtons->setChecked(true);
          return;
        } else if (strucnaSprema == StrucnaSprema::OsnovneStudije && text == "Fakultet") {
          radioButtons->setChecked(true);
          return;
        } else if (strucnaSprema == StrucnaSprema::MasterStudije && text == "Master") {
          radioButtons->setChecked(true);
          return;
        } else if (strucnaSprema == StrucnaSprema::DoktorskeStudije && text == "Doktorat") {
          radioButtons->setChecked(true);
          return;
        }
      }
    }
  }

  delete vl;
}

void IzmeniOglas::postaviVestine(const QVector<QString> &vestine) {
  int n = ui->glVestine->rowCount();
  int m = ui->glVestine->columnCount();

  for (int row = 0; row < n; row++) {
    for (int col = 0; col < m; col++) {
      QLayoutItem *item = ui->glVestine->itemAtPosition(row, col);

      if (item != nullptr) {
        QLayout *horizontalLayout = item->layout();
        if (horizontalLayout != nullptr) {
          int n = horizontalLayout->count();
          for (int i = 0; i < n; ++i) {
            QWidget *widget = horizontalLayout->itemAt(i)->widget();
            if (widget != nullptr) {

              if (QCheckBox *checkBox = qobject_cast<QCheckBox *>(widget)) {
                if (vestine.contains(checkBox->text())) {
                  checkBox->setChecked(true);
                }
              }
            }
          }
        }
      }
    }
  }
}

void IzmeniOglas::postaviBenefite(const QVector<QString> &benefiti) {
  int n = ui->glBenefiti->rowCount();
  int m = ui->glBenefiti->columnCount();

  for (int row = 0; row < n; row++) {
    for (int col = 0; col < m; col++) {
      QLayoutItem *item = ui->glBenefiti->itemAtPosition(row, col);

      if (item != nullptr) {
        QLayout *horizontalLayout = item->layout();
        if (horizontalLayout != nullptr) {
          for (int i = 0; i < horizontalLayout->count(); ++i) {
            QWidget *widget = horizontalLayout->itemAt(i)->widget();
            if (widget != nullptr) {

              if (QCheckBox *checkBox = qobject_cast<QCheckBox *>(widget)) {
                if (benefiti.contains(checkBox->text())) {
                  checkBox->setChecked(true);
                }
              }
            }
          }
        }
      }
    }
  }
}

void IzmeniOglas::postaviTehnologije(const QVector<QString> &tehnologije) {
  int n = ui->glTehnologije->rowCount();
  int m = ui->glTehnologije->columnCount();

  for (int row = 0; row < n; row++) {
    for (int col = 0; col < m; col++) {
      QLayoutItem *item = ui->glTehnologije->itemAtPosition(row, col);

      if (item != nullptr) {
        QLayout *horizontalLayout = item->layout();
        if (horizontalLayout != nullptr) {
          for (int i = 0; i < horizontalLayout->count(); ++i) {
            QWidget *widget = horizontalLayout->itemAt(i)->widget();
            if (widget != nullptr) {

              if (QCheckBox *checkBox = qobject_cast<QCheckBox *>(widget)) {
                if (tehnologije.contains(checkBox->text())) {
                  checkBox->setChecked(true);
                }
              }
            }
          }
        }
      }
    }
  }
}

void IzmeniOglas::postaviBrojGodinaIskustva(const QMap<QString, unsigned> &iskustvo) {
  QGridLayout *gridLayout = ui->glIskustvo;

  int n = gridLayout->rowCount();
  int m = gridLayout->columnCount();

  for (int row = 0; row < n; ++row) {
    for (int col = 0; col < m; ++col) {
      QLayoutItem *item = gridLayout->itemAtPosition(row, col);

      if (item != nullptr) {
        QLayout *horizontalLayout = item->layout();
        if (horizontalLayout != nullptr) {
          unsigned broj = 0;

          for (int i = 0; i < horizontalLayout->count(); ++i) {
            QWidget *widget = horizontalLayout->itemAt(i)->widget();
            if (widget != nullptr) {
              if (QLabel *label = qobject_cast<QLabel *>(widget)) {
                QString tekstLabela = label->text();

                if (!tekstLabela.isEmpty()) {

                  auto it = iskustvo.find(tekstLabela);
                  if (it != iskustvo.end()) {
                    broj = it.value();
                  }
                }
              }
              if (QSpinBox *spinBox = qobject_cast<QSpinBox *>(widget)) {
                spinBox->setValue(broj);
              }
            }
          }
        }
      }
    }
  }
}

Oglas *IzmeniOglas::noveIzmene() {
  std::unique_ptr<ObradaInterfejsa<Ui::IzmeniOglas>> obrada(new ObradaInterfejsa<Ui::IzmeniOglas>);

  return new Oglas(
       obrada->dohvatiPoziciju(ui), obrada->dohvatiMaxPlatu(ui).toUInt(), obrada->dohvatiNacinRada(ui), obrada->dohvatiObrazovanje(ui),
       obrada->dohvatiTehnologije(ui), obrada->dohvatiBrojGodinaIskustva(ui), obrada->dohvatiVestine(ui), obrada->dohvatiBenefite(ui),
       obrada->dohvatiNazivFirme(ui), ui->lbId->text());
}

void IzmeniOglas::pronadjenDirektorijumOglas(const QString &dir) {
  Oglas *o = noveIzmene();
  JSONSerializer json;
  json.save(*o, dir);
}

void IzmeniOglas::zavrsiIzmene() {
  const std::unique_ptr<ObradaInterfejsa<Ui::IzmeniOglas>> obrada(new ObradaInterfejsa<Ui::IzmeniOglas>);

  QString filePath = "oglas";
  filePath = filePath + QDir::separator();
  filePath = filePath + ui->lbId->text();
  filePath = filePath + ".json";

  SearchThread *searchThread = new SearchThread(filePath);
  connect(searchThread, &SearchThread::directoryFound, this, &IzmeniOglas::pronadjenDirektorijumOglas);
  searchThread->start();

  connect(searchThread, &SearchThread::finished, searchThread, &QObject::deleteLater);
}
