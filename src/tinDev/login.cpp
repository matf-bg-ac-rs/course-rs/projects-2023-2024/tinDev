#include "login.h"
#include "../serialization/jsonserializer.h"
#include "loginthread.h"
#include "mainwindowdeveloper.h"
#include "mainwindowfirma.h"
#include "napravinalogdeveloper.h"
#include "napravinalogfirma.h"
#include "obradainterfejsa.h"
#include "ui_login.h"
#include <QFontDatabase>
#include <QVBoxLayout>

LogIn::LogIn(QWidget *parent) : QWidget(parent), ui(new Ui::LogIn) {
  ui->setupUi(this);

  connect(ui->pbUlogujSe, &QPushButton::clicked, this, &LogIn::kliknutoUlogujSe);

  connect(ui->pbFirma, &QPushButton::clicked, this, &LogIn::kliknutoNapraviNalogFirma);
  connect(ui->pbFirma, &QPushButton::clicked, this, &LogIn::zatvoriLogIn);

  connect(ui->pbDeveloper, &QPushButton::clicked, this, &LogIn::kliknutoNapraviNalogDeveloper);
  connect(ui->pbDeveloper, &QPushButton::clicked, this, &LogIn::zatvoriLogIn);

  int fontId = QFontDatabase::addApplicationFont(":/resource/font/resources/font/Humaroid.otf");
  if (fontId != -1) {
    QString fontName = QFontDatabase::applicationFontFamilies(fontId).at(0);

    QFont font(fontName);
    font.setPointSize(60);

    ui->lbtinDev->setFont(font);
  }
}

LogIn::~LogIn() { delete ui; }

void LogIn::kliknutoUlogujSe() {
  const std::unique_ptr<ObradaInterfejsa<Ui::LogIn>> obrada(new ObradaInterfejsa<Ui::LogIn>);
  QHBoxLayout *hl = ui->hlNalog;

  QWidget *firmaWidget = hl->itemAt(0)->widget();
  QWidget *devWidget = hl->itemAt(1)->widget();

  QRadioButton *firmaRb = qobject_cast<QRadioButton *>(firmaWidget);
  QRadioButton *devRb = qobject_cast<QRadioButton *>(devWidget);

  if (!obrada->ispravniPodaciLogIn(ui)) {
    obrada->upozorenje("Morate uneti username ili sifru");
  } else if (firmaRb->isChecked()) {
    zavrsenLogInFirma();
  } else if (devRb->isChecked()) {
    zavrsenLogInDeveloper();
  } else {
    obrada->upozorenje("Izaberite da li ste firma ili developer!");
  }
}

void LogIn::zatvoriLogIn() { this->close(); }

void LogIn::kliknutoNapraviNalogFirma() {
  NapraviNalogFirma *firma = new NapraviNalogFirma;
  firma->show();
}

void LogIn::kliknutoNapraviNalogDeveloper() {
  NapraviNalogDeveloper *dev = new NapraviNalogDeveloper;
  dev->show();
}

void LogIn::directoryNotFound() {
  const std::unique_ptr<ObradaInterfejsa<Ui::LogIn>> obrada(new ObradaInterfejsa<Ui::LogIn>);
  obrada->upozorenje("Morate imati nalog da bi mogli da se ulogujete!");
}

void LogIn::directoryFoundDeveloper(const QString &directory, const QString &sifra) {
  const std::unique_ptr<ObradaInterfejsa<Ui::LogIn>> obrada(new ObradaInterfejsa<Ui::LogIn>);
  Developer *d = new Developer;
  JSONSerializer json;
  json.load(*d, directory);

  if (d->sifra() == sifra) {
    MainWindowDeveloper *w = new MainWindowDeveloper;
    w->postaviUsername(ui->leUsername->text());
    emit w->windowLoaded();
    w->show();
    zatvoriLogIn();
  } else {
    obrada->upozorenje("Pogresna sifra!");
  }

  delete d;
}

void LogIn::zavrsenLogInDeveloper() {
  const std::unique_ptr<ObradaInterfejsa<Ui::LogIn>> obrada(new ObradaInterfejsa<Ui::LogIn>);
  QString username = obrada->dohvatiUsername(ui);
  QString sifra = obrada->dohvatiSifru(ui);
  QString filePath = "developer";
  filePath = filePath + QDir::separator();
  filePath = filePath + username;
  filePath = filePath + ".json";

  loginthread *thread = new loginthread(filePath, sifra);

  connect(thread, &loginthread::directoryNotFound, this, &LogIn::directoryNotFound);
  connect(thread, &loginthread::directoryFound, this, &LogIn::directoryFoundDeveloper);

  thread->start();

  connect(thread, &loginthread::finished, thread, &QObject::deleteLater);
}

void LogIn::directoryFoundFirma(const QString &directory, const QString &sifra) {
  const std::unique_ptr<ObradaInterfejsa<Ui::LogIn>> obrada(new ObradaInterfejsa<Ui::LogIn>);
  Firma *f = new Firma;
  JSONSerializer json;
  json.load(*f, directory);

  if (f->sifra() == sifra) {
    MainWindowFirma *w = new MainWindowFirma;
    w->postaviUsername(ui->leUsername->text());
    emit w->windowLoaded();
    w->show();
    zatvoriLogIn();
  } else {
    obrada->upozorenje("Pogresna sifra!");
  }
}

void LogIn::zavrsenLogInFirma() {
  const std::unique_ptr<ObradaInterfejsa<Ui::LogIn>> obrada(new ObradaInterfejsa<Ui::LogIn>);
  QString username = obrada->dohvatiUsername(ui);
  QString sifra = obrada->dohvatiSifru(ui);
  QString filePath = "firma";
  filePath = filePath + QDir::separator();
  filePath = filePath + username;
  filePath = filePath + ".json";
  loginthread *thread = new loginthread(filePath, sifra);

  connect(thread, &loginthread::directoryNotFound, this, &LogIn::directoryNotFound);
  connect(thread, &loginthread::directoryFound, this, &LogIn::directoryFoundFirma);

  thread->start();

  connect(thread, &loginthread::finished, thread, &QObject::deleteLater);
}
