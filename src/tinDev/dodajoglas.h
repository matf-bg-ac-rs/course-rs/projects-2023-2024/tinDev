#pragma once
#include "oglas.h"
#include <QDebug>
#include <QGridLayout>
#include <QMap>
#include <QMessageBox>
#include <QWidget>
#include "ui_dodajoglas.h"

namespace Ui {
class DodajOglas;
}

class DodajOglas : public QWidget {
  Q_OBJECT

public:
  explicit DodajOglas(QWidget *parent = nullptr);
  ~DodajOglas();
  void postaviUsername(const QString &username);

  Ui::DodajOglas *getUi();

signals:
  void windowLoaded();
private slots:

  void kliknutoDaljeNaTehnologije();
  void kliknutoDaljeNaJezike();
  void kliknutoDaljeNaBenefite();
  void kliknutoNadzadNaJezike();
  void kliknutoNadzadNaTehnologije();
  void kliknutoNadzadNaPlatu();
  void zavrsiPravljenjeOglasa();

  void directoryFound(const QString &directory);
  void sacuvaj();

private:
  Ui::DodajOglas *ui;
  Oglas *oglasZavrsen();
};
