#pragma once
#include "../serialization/serializable.h"
#include "enums.h"
#include "korisnik.h"
#include <QPair>
#include <QString>
#include <QVector>
#include <iostream>
#include <vector>

class Developer : public Korisnik, public Serializable {
public:
  Developer();
  Developer(
       const QString &ime, const QString &prezime, char pol, const QVector<QString> &tehnologije, const QMap<QString, unsigned int> &brGodinaIskustva,
       unsigned opsegPlate, NacinRada &nacinRada, const QVector<QString> &vestine, const QVector<QString> &benefiti, StrucnaSprema nivoObrazovanja,
       const QString &username, const QString &sifra, const QString &brTelefona);

  ~Developer() = default;

  inline QString username() const override { return m_username; }
  inline QString sifra() const override { return m_sifra; }
  inline QString ime() const { return m_ime; }
  inline QString prezime() const { return m_prezime; }
  inline char pol() const { return m_pol; }
  inline QVector<QString> tehnologije() const { return m_tehnologije; }
  inline QMap<QString, unsigned> brGodinaIskustva() const { return m_brGodinaIskustva; }
  inline unsigned opsegPlate() const { return m_opsegPlate; }
  inline NacinRada nacinRada() const { return m_nacinRada; }
  inline QVector<QString> vestine() const { return m_vestine; }
  inline QVector<QString> benefiti() const { return m_benefiti; }
  inline StrucnaSprema nivoObrazovanja() const { return m_nivoObrazovanja; }
  inline QString brTelefona() const { return m_brTelefona; }

private:
  QString m_username;
  QString m_sifra;
  QString m_ime;
  QString m_prezime;
  QString m_brTelefona;
  char m_pol;
  QVector<QString> m_tehnologije;
  QMap<QString, unsigned> m_brGodinaIskustva;
  unsigned m_opsegPlate;
  NacinRada m_nacinRada;
  QVector<QString> m_vestine;
  QVector<QString> m_benefiti;
  StrucnaSprema m_nivoObrazovanja;

  friend void setUsername(Developer &dev, const QString &newUsername);
  friend void setSifra(Developer &dev, const QString &newSifra);
  friend void setIme(Developer &dev, const QString &newIme);
  friend void setPrezime(Developer &dev, const QString &newPrezime);
  friend void setPol(Developer &dev, const char newPol);
  friend void setTehnologije(Developer &dev, const QVector<QString> &newTehnologije);
  friend void setBrGodinaIskustva(Developer &dev, const QMap<QString, unsigned int> &newBrGodinaIskustva);
  friend void setOpsegPlate(Developer &dev, unsigned &newOpsegPlate);
  friend void setNacinRada(Developer &dev, NacinRada newNacinRada);
  friend void setVestine(Developer &dev, const QVector<QString> &newVestine);
  friend void setBenefiti(Developer &dev, const QVector<QString> &newBenefiti);
  friend void setNivoObrazovanja(Developer &dev, StrucnaSprema newNivoObrazovanja);

public:
  QVariant toVariant() const override;
  void fromVariant(const QVariant &variant) override;
  QString stepenStrucneSpremeStr() const;
  QString nacinRadaStr() const;
};
