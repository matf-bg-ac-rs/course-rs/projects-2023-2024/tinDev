#pragma once
#include "enums.h"
#include <QCheckBox>
#include <QGridLayout>
#include <QLabel>
#include <QMap>
#include <QMessageBox>
#include <QRadioButton>
#include <QRegularExpression>
#include <QSpinBox>
#include <QString>

template <class UI> class ObradaInterfejsa {

public:
  ObradaInterfejsa() {}

public slots:
  QString dohvatiUsername(UI *ui) { return ui->leUsername->text(); }

  QString dohvatiSifru(UI *ui) { return ui->leSifra->text(); }

  QString dohvatiNazivFirme(UI *ui) { return ui->leNazivFirme->text(); }

  QString dohvatiAdresu(UI *ui) { return ui->leAdresa->text(); }

  QString dohvatiIme(UI *ui) { return ui->leIme->text(); }

  QString dohvatiPrezime(UI *ui) { return ui->lePrezime->text(); }

  QString dohvatiBrTelefona(UI *ui) { return ui->leBrojTelefona->text(); }

  QString dohvatiEmail(UI *ui) { return ui->leEMail->text(); }

  QString dohvatiMaxPlatu(UI *ui) { return ui->lePlata->text(); }

  QString dohvatiPoziciju(UI *ui) { return ui->lePozicija->text(); }

  char dohvatiPol(UI *ui) { return (ui->rbMuski->isChecked()) ? 'm' : 'z'; }

  bool ispravnaPlata(UI *ui) {
    static const QRegularExpression plataRegex("^(?:100|[1-9]\\d{2,})$");
    return plataRegex.match(dohvatiMaxPlatu(ui)).hasMatch();
  }

  bool ispravanBrojTelefona(UI *ui) {
    static const QRegularExpression brTelefonaRegex("^06\\d{7,8}$");
    return brTelefonaRegex.match(dohvatiBrTelefona(ui)).hasMatch();
  }

  bool ispravanEmail(UI *ui) {
    static const QRegularExpression emailRegex("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$");
    return emailRegex.match(dohvatiEmail(ui)).hasMatch();
  }

  bool izabranPol(UI *ui) { return ui->rbMuski->isChecked() || ui->rbZenski->isChecked(); }

  bool izabranNacinRada(UI *ui) { return ui->rbOffice->isChecked() || ui->rbHybrid->isChecked() || ui->rbRemote->isChecked(); }

  bool izabranaStrucnaSprema(UI *ui) {
    return ui->rbSrednjaSkola->isChecked() || ui->rbFakultet->isChecked() || ui->rbMaster->isChecked() || ui->rbDoktorat->isChecked();
  }

  bool ispravniPodaci(UI *ui) {
    return !(
         dohvatiUsername(ui).isEmpty() || dohvatiSifru(ui).isEmpty() || dohvatiIme(ui).isEmpty() || dohvatiPrezime(ui).isEmpty() ||
         dohvatiBrTelefona(ui).isEmpty());
  }

  bool ispravniPodaci1(UI *ui) {
    return !(
         dohvatiUsername(ui).isEmpty() || dohvatiSifru(ui).isEmpty() || dohvatiNazivFirme(ui).isEmpty() || dohvatiAdresu(ui).isEmpty() ||
         dohvatiEmail(ui).isEmpty() || dohvatiBrTelefona(ui).isEmpty());
  }

  bool ispravniPodaciLogIn(UI *ui) { return !(dohvatiUsername(ui).isEmpty() || dohvatiSifru(ui).isEmpty()); }

  QVector<QString> dohvatiTehnologije(UI *ui) {
    QVector<QString> tehnologije;

    int n = ui->glTehnologije->rowCount();
    int m = ui->glTehnologije->columnCount();

    for (int row = 0; row < n; row++) {
      for (int col = 0; col < m; col++) {
        QLayoutItem *item = ui->glTehnologije->itemAtPosition(row, col);

        if (item != nullptr) {
          QLayout *horizontalLayout = item->layout();
          if (horizontalLayout != nullptr) {
            for (int i = 0; i < horizontalLayout->count(); ++i) {
              QWidget *widget = horizontalLayout->itemAt(i)->widget();
              if (widget != nullptr) {

                if (QCheckBox *checkBox = qobject_cast<QCheckBox *>(widget)) {
                  if (checkBox->isChecked()) {
                    QString imeCheckBox = checkBox->text();
                    tehnologije.push_back(imeCheckBox);
                  }
                }
              }
            }
          }
        }
      }
    }

    return tehnologije;
  }

  QVector<QString> dohvatiBenefite(UI *ui) {
    QVector<QString> benefiti;

    int n = ui->glBenefiti->rowCount();
    int m = ui->glBenefiti->columnCount();

    for (int row = 0; row < n; row++) {
      for (int col = 0; col < m; col++) {
        QLayoutItem *item = ui->glBenefiti->itemAtPosition(row, col);

        if (item != nullptr) {
          QLayout *horizontalLayout = item->layout();
          if (horizontalLayout != nullptr) {
            for (int i = 0; i < horizontalLayout->count(); ++i) {
              QWidget *widget = horizontalLayout->itemAt(i)->widget();
              if (widget != nullptr) {

                if (QCheckBox *checkBox = qobject_cast<QCheckBox *>(widget)) {
                  if (checkBox->isChecked()) {
                    QString imeCheckBox = checkBox->text();
                    benefiti.push_back(imeCheckBox);
                  }
                }
              }
            }
          }
        }
      }
    }

    return benefiti;
  }

  QVector<QString> dohvatiVestine(UI *ui) {
    QVector<QString> vestine;

    int n = ui->glVestine->rowCount();
    int m = ui->glVestine->columnCount();

    for (int row = 0; row < n; row++) {
      for (int col = 0; col < m; col++) {
        QLayoutItem *item = ui->glVestine->itemAtPosition(row, col);

        if (item != nullptr) {
          QLayout *horizontalLayout = item->layout();
          if (horizontalLayout != nullptr) {
            for (int i = 0; i < horizontalLayout->count(); ++i) {
              QWidget *widget = horizontalLayout->itemAt(i)->widget();
              if (widget != nullptr) {

                if (QCheckBox *checkBox = qobject_cast<QCheckBox *>(widget)) {
                  if (checkBox->isChecked()) {
                    QString imeCheckBox = checkBox->text();
                    vestine.push_back(imeCheckBox);
                  }
                }
              }
            }
          }
        }
      }
    }

    return vestine;
  }

  QMap<QString, unsigned> dohvatiBrojGodinaIskustva(UI *ui) {
    QMap<QString, unsigned> mapaBrGodIskustva;
    QGridLayout *gridLayout = ui->glIskustvo;

    int n = gridLayout->rowCount();
    int m = gridLayout->columnCount();

    for (int row = 0; row < n; ++row) {
      for (int col = 0; col < m; ++col) {
        QLayoutItem *item = gridLayout->itemAtPosition(row, col);
        QString program;
        unsigned broj = 0;

        if (item != nullptr) {
          QLayout *horizontalLayout = item->layout();
          if (horizontalLayout != nullptr) {
            for (int i = 0; i < horizontalLayout->count(); ++i) {
              QWidget *widget = horizontalLayout->itemAt(i)->widget();
              if (widget != nullptr) {
                if (QLabel *label = qobject_cast<QLabel *>(widget)) {
                  QString tekstLabela = label->text();
                  if (!tekstLabela.isEmpty()) {
                    program = tekstLabela;
                  }
                }
                if (QSpinBox *spinBox = qobject_cast<QSpinBox *>(widget)) {
                  broj = spinBox->value();
                }
              }
            }
          }
        }
        if (broj > 0) {
          mapaBrGodIskustva[program] = broj;
        }
      }
    }
    return mapaBrGodIskustva;
  }

  unsigned dohvatiPlatu(UI *ui) { return ui->hsOpsegPlate1->value(); }

  NacinRada dohvatiNacinRada(UI *ui) {
    QVBoxLayout *vl = ui->vlNacinRada;
    auto redovi = vl->count();

    for (int i = 0; i < redovi; ++i) {
      QWidget *widget = vl->itemAt(i)->widget();
      if (widget != nullptr) {
        if (QRadioButton *radioButtons = qobject_cast<QRadioButton *>(widget)) {
          if (radioButtons->isChecked()) {
            if (radioButtons->text() == "Remote") {
              return NacinRada::Remote;
            } else if (radioButtons->text() == "Office") {
              return NacinRada::Office;
            }
          }
        }
      }
    }
    return NacinRada::Hybrid;
  };

  StrucnaSprema dohvatiObrazovanje(UI *ui) {
    QVBoxLayout *vl = ui->vlObrazovanje;
    auto redovi = vl->count();

    for (int i = 0; i < redovi; ++i) {
      QWidget *widget = vl->itemAt(i)->widget();

      if (widget != nullptr) {
        if (QRadioButton *radioButtons = qobject_cast<QRadioButton *>(widget)) {
          if (radioButtons->isChecked()) {
            if (radioButtons->text() == "Srednja skola") {
              return StrucnaSprema::SrednjaSkola;
            } else if (radioButtons->text() == "Fakultet") {
              return StrucnaSprema::OsnovneStudije;
            } else if (radioButtons->text() == "Master") {
              return StrucnaSprema::MasterStudije;
            } else if (radioButtons->text() == "Doktorat") {
              return StrucnaSprema::DoktorskeStudije;
            }
          }
        }
      }
    }
    return StrucnaSprema::SrednjaSkola;
  }

  void upozorenje(const QString &s) {
    QMessageBox msgBox;
    msgBox.setText(s);

    static const QString styleSheet = "QMessageBox { background-color: rgb(55, 55, 55); }"
                                      "QLabel { color: rgb(255, 195, 15); }"
                                      "QPushButton { background-color: rgb(55, 55, 55); color: rgb(255, 195, 15); }";
    msgBox.setStyleSheet(styleSheet);

    msgBox.exec();
  }
};
