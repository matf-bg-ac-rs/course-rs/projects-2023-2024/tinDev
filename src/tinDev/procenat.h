#pragma once
#include "developer.h"
#include "oglas.h"
#include <QDebug>
#include <QMap>
#include <QString>
#include <QVector>

class Procenat {
public:
  Procenat(Developer *d, Oglas *o);

  ~Procenat() = default;

  double uporediPoklapanjeDevOglas();

private:
  Developer *m_d;
  Oglas *m_o;
  double m_brojacPoklapanja = 0;
  double m_ukupno = 0;
  double m_brojDisciplina = 7;
  double m_procenat = 0;
};
