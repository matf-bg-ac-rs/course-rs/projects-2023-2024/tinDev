#pragma once
#include <QString>
#include <iostream>

class Korisnik {
public:
  Korisnik();
  Korisnik(const QString &username, const QString &sifra);

  virtual ~Korisnik() = default;
  virtual QString username() const = 0;
  virtual QString sifra() const = 0;

private:
  QString m_username;
  QString m_sifra;
};
