#include "mainwindowfirma.h"
#include "../clientServer/chatroom.h"
#include "../serialization/jsonserializer.h"
#include "developer.h"
#include "dodajoglas.h"
#include "izmeninalogfirma.h"
#include "loginthread.h"
#include "match.h"
#include "obradainterfejsa.h"
#include "oglas.h"
#include "procenat.h"
#include "readfilethread.h"
#include "rewritethread.h"
#include "searchthread.h"
#include "svioglasi.h"
#include "ui_mainwindowfirma.h"
#include "usernamemanager.h"
#include <QFile>
#include <QFontDatabase>
#include <QVector>

MainWindowFirma::MainWindowFirma(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindowFirma) {
  ui->setupUi(this);

  connect(ui->pbChat, &QPushButton::clicked, this, &MainWindowFirma::otvoriChatProzor);
  connect(ui->pbDodajOglas, &QPushButton::clicked, this, &MainWindowFirma::dodajOglas);
  connect(ui->pbSviOglasi, &QPushButton::clicked, this, &MainWindowFirma::vidiSveOglase);
  connect(ui->pbIzmeniNalog, &QPushButton::clicked, this, &MainWindowFirma::izmeniNalog);
  connect(this, &MainWindowFirma::windowLoaded, this, &MainWindowFirma::sviOglasi);
  connect(ui->pbDislike, &QPushButton::clicked, this, &MainWindowFirma::dislike);
  connect(ui->pbLike, &QPushButton::clicked, this, &MainWindowFirma::like);

  int fontId = QFontDatabase::addApplicationFont(":/resource/font/resources/font/Humaroid.otf");
  if (fontId != -1) {
    QString fontName = QFontDatabase::applicationFontFamilies(fontId).at(0);

    QFont font(fontName);
    QFont fontLabele(fontName);
    font.setPointSize(20);
    fontLabele.setPointSize(18);

    ui->lbTinDev->setFont(font);
    ui->lbZdravo->setFont(fontLabele);
    ui->leUsername->setFont(fontLabele);
  }
  UsernameManager::setUsernameProvider(this);
}

MainWindowFirma::~MainWindowFirma() {
  qDeleteAll(m_developeri);
  m_developeri.clear();

  qDeleteAll(m_oglasi);
  m_oglasi.clear();

  qDeleteAll(m_svidjanja);
  m_svidjanja.clear();

  m_procentiVektor.clear();

  delete ui;
}

Ui::MainWindowFirma *MainWindowFirma::getUi() { return ui; }

void MainWindowFirma::postaviUsername(const QString &username) { ui->leUsername->setText(username); }

void MainWindowFirma::otvoriChatProzor() {
  ChatRoom *chat = new ChatRoom();
  chat->show();
}

void MainWindowFirma::windowLoaded2PR() {
  for (int i = 0; i < m_developeri.size(); ++i) {
    m_developeri.removeAt(i);
    --i;
  }
  for (int i = 0; i < m_oglasi.size(); ++i) {
    m_oglasi.removeAt(i);
    --i;
  }
  ukloniPodatke();
  emit windowLoaded();
}

void MainWindowFirma::ukloniPodatke() {
  ui->lbImeIPrezime->setText("");
  ui->lbMinPlata->setText("");
  ui->lbNacinRada->setText("");
  ui->lbStrucnaSprema->setText("");
  ui->tbJezici->clear();
  ui->tbBenefiti->clear();
  ui->tbTehnologije->clear();
  ui->tbVestine->clear();
}

void MainWindowFirma::sledeciDeveloper() {
  m_developeri.removeAt(0);
  m_procentiVektor.removeAt(0);
}

bool MainWindowFirma::krajDevelopera() { return m_developeri.size() > 1; }

void MainWindowFirma::dislike() {
  const std::unique_ptr<ObradaInterfejsa<Ui::MainWindowFirma>> obrada(new ObradaInterfejsa<Ui::MainWindowFirma>);
  if (krajDevelopera()) {
    sledeciDeveloper();
    ukloniPodatke();
    postaviDevelopere();
  } else {
    if (m_developeri.size() == 1) {
      sledeciDeveloper();
      krajSvajpovanja();
    }
    obrada->upozorenje("Pregledali ste sve developere!");
  }
}

void MainWindowFirma::threadNadjiDevSvidjanja(const QString &ime_developera) {
  QString filePath = "developerSvidjanja";
  filePath = filePath + QDir::separator();
  filePath = filePath + ime_developera;
  filePath = filePath + ".json";

  SearchThread *searchThread = new SearchThread(filePath);

  connect(searchThread, &SearchThread::directoryFound, this, &MainWindowFirma::pronadjenDirektorijumDevSvidjanja);
  searchThread->start();
  connect(searchThread, &SearchThread::finished, searchThread, &QObject::deleteLater);
}

void MainWindowFirma::pronadjenDirektorijumDevSvidjanja(const QString &dir) {
  const std::unique_ptr<ObradaInterfejsa<Ui::MainWindowFirma>> obrada(new ObradaInterfejsa<Ui::MainWindowFirma>);
  QFileInfo fileInfo(dir);
  QString imeFirmeDevelopera = fileInfo.baseName();
  QString targetUsername = ui->leUsername->text();

  Match *m = new Match(dir, targetUsername, imeFirmeDevelopera);
  m->proveriMatch();

  if (m->indikator()) {
    QString porukaMatch = "Čestitamo " + targetUsername + " imate MATCH sa " + imeFirmeDevelopera;
    obrada->upozorenje(porukaMatch);
  }
}

void MainWindowFirma::like() {
  const std::unique_ptr<ObradaInterfejsa<Ui::MainWindowFirma>> obrada(new ObradaInterfejsa<Ui::MainWindowFirma>);
  if (krajDevelopera()) {
    m_svidjanja.push_back(m_developeri[0]);
    sacuvajSvidjanja();

    Developer *d = m_svidjanja.last();
    QString userDevelopera = d->username();
    threadNadjiDevSvidjanja(userDevelopera);

    sledeciDeveloper();
    ukloniPodatke();
    postaviDevelopere();
  } else {
    if (m_developeri.size() == 1) {
      m_svidjanja.push_back(m_developeri[0]);
      sacuvajSvidjanja();

      krajSvajpovanja();
      sledeciDeveloper();
    }
    obrada->upozorenje("Pregledali ste sve developere!");
  }
}

void MainWindowFirma::dodajOglas() {
  DodajOglas *oglas = new DodajOglas;
  oglas->postaviUsername(ui->leUsername->text());

  oglas->show();
  connect(oglas, &DodajOglas::windowLoaded, this, &MainWindowFirma::windowLoaded2PR);
}

void MainWindowFirma::vidiSveOglase() {
  SviOglasi *sviOglasi = new SviOglasi;
  sviOglasi->postaviUsername(ui->leUsername->text());

  for (auto oglas : m_oglasi) {
    QString ime = oglas->id() + " " + oglas->nazivPozicije();
    sviOglasi->postaviOglas(ime);
  }
  sviOglasi->show();
  connect(sviOglasi, &SviOglasi::windowLoaded, this, &MainWindowFirma::windowLoaded2PR);
}

void MainWindowFirma::izmeniNalog() { zapoceteIzmene(); }

void MainWindowFirma::directoryFoundFirma(const QString &directory) {
  const std::unique_ptr<ObradaInterfejsa<Ui::IzmeniNalogFirma>> obrada(new ObradaInterfejsa<Ui::IzmeniNalogFirma>);

  Firma *f = new Firma;
  JSONSerializer json;
  json.load(*f, directory);

  IzmeniNalogFirma *izmena = new IzmeniNalogFirma();
  izmena->postaviUsername(ui->leUsername->text());
  izmena->postaviPodatke(f->sifra(), f->ime(), f->adresa(), f->brTelefona(), f->email());
  izmena->show();
}

void MainWindowFirma::uporediProcente() {
  for (Developer *d : m_developeri) {
    m_procenatIndikatorMax = 0;
    for (Oglas *o : m_oglasi) {
      Procenat *p = new Procenat(d, o);
      double procenat = p->uporediPoklapanjeDevOglas();
      o->setProcenat(procenat);

      if (o->procenatPoklapanja() > m_procenatIndikatorMax)
        m_procenatIndikatorMax = o->procenatPoklapanja();
    }

    m_procentiVektor.push_back(m_procenatIndikatorMax);
  }
}

void MainWindowFirma::directoryFoundDeveloper(const QVector<QString> &directory) {
  m_developeri.clear();
  for (int j = 0; j < directory.size(); j++) {
    Developer *d = new Developer();
    JSONSerializer json;
    json.load(*d, directory[j]);

    m_developeri.push_back(d);
  }

  if (!m_developeri.isEmpty()) {
    uporediProcente();
    postaviDevelopere();
  }
}

void MainWindowFirma::directoryFoundSvidjanja(const QString &directory) {
  Developer *d = m_svidjanja.last();
  JSONSerializer json;
  json.save(*d, directory);
}

void MainWindowFirma::sacuvajSvidjanja() {
  const std::unique_ptr<ObradaInterfejsa<Ui::MainWindowFirma>> obrada(new ObradaInterfejsa<Ui::MainWindowFirma>);
  QString filePath = "frimaSvidjanja";
  filePath = filePath + QDir::separator();
  filePath = filePath + obrada->dohvatiUsername(ui);
  filePath = filePath + ".json";
  SearchThread *searchThread = new SearchThread(filePath);

  connect(searchThread, &SearchThread::directoryFound, this, &MainWindowFirma::directoryFoundSvidjanja);

  searchThread->start();

  connect(searchThread, &SearchThread::finished, searchThread, &QObject::deleteLater);
}

void MainWindowFirma::krajSvajpovanja() {
  const std::unique_ptr<ObradaInterfejsa<Ui::MainWindowFirma>> obrada(new ObradaInterfejsa<Ui::MainWindowFirma>);

  QString filePath = "frimaSvidjanja";
  filePath = filePath + QDir::separator();
  filePath = filePath + obrada->dohvatiUsername(ui);
  filePath = filePath + ".json";

  RewriteThread *rewriteThread = new RewriteThread(filePath);

  rewriteThread->start();

  connect(rewriteThread, &RewriteThread::finished, rewriteThread, &QObject::deleteLater);
}

void MainWindowFirma::postaviDevelopere() {
  ui->lbImeIPrezime->setText(m_developeri.first()->ime() + " " + m_developeri.first()->prezime());
  ui->lbMinPlata->setText(QString::number(m_developeri.first()->opsegPlate()));
  ui->lbNacinRada->setText(m_developeri.first()->nacinRadaStr());
  ui->lbStrucnaSprema->setText(m_developeri.first()->stepenStrucneSpremeStr());

  double procenat = m_procentiVektor[0];
  ui->progressBar->setValue(procenat);

  const QMap<QString, unsigned int> &mapaJezika = m_developeri.first()->brGodinaIskustva();
  if (!mapaJezika.isEmpty()) {
    for (auto it = mapaJezika.begin(); it != mapaJezika.end(); ++it) {
      QString kljuc = it.key();
      unsigned int vrednost = it.value();
      ui->tbJezici->append(kljuc + " " + QString::number(vrednost));
    }
  }

  for (QString &teh : m_developeri.first()->tehnologije()) {
    ui->tbTehnologije->append(teh);
  }
  for (QString &ben : m_developeri.first()->benefiti()) {
    ui->tbBenefiti->append(ben);
  }
  for (QString &vest : m_developeri.first()->vestine()) {
    ui->tbVestine->append(vest);
  }
}

void MainWindowFirma::directoryFoundOglasi(const QVector<QString> &directory) {
  m_procentiVektor.clear();
  m_oglasi.clear();
  for (int j = 0; j < directory.size(); j++) {
    Oglas *o = new Oglas();
    JSONSerializer json;
    json.load(*o, directory[j]);
    if (o->usernameFirma() == ui->leUsername->text()) {
      m_oglasi.push_back(o);
    }
  }

  sviDeveloperi();
}

void MainWindowFirma::sviOglasi() {
  QString filePath = "oglas";

  ReadFileThread *thread = new ReadFileThread(filePath);

  connect(thread, &ReadFileThread::directoryFound, this, &MainWindowFirma::directoryFoundOglasi);
  thread->start();

  connect(thread, &ReadFileThread::finished, thread, &QObject::deleteLater);
}

void MainWindowFirma::zapoceteIzmene() {
  const std::unique_ptr<ObradaInterfejsa<Ui::MainWindowFirma>> obrada(new ObradaInterfejsa<Ui::MainWindowFirma>);
  QString username = obrada->dohvatiUsername(ui);
  QString filePath = "firma";
  filePath = filePath + QDir::separator();
  filePath = filePath + username;
  filePath = filePath + ".json";

  loginthread *thread = new loginthread(filePath);

  connect(thread, &loginthread::directoryFound, this, &MainWindowFirma::directoryFoundFirma);
  thread->start();

  connect(thread, &loginthread::finished, thread, &QObject::deleteLater);
}

QString MainWindowFirma::getUsername() const { return ui->leUsername->text(); }

void MainWindowFirma::sviDeveloperi() {
  const std::unique_ptr<ObradaInterfejsa<Ui::MainWindowFirma>> obrada(new ObradaInterfejsa<Ui::MainWindowFirma>);

  QString filePath = "developer";

  ReadFileThread *thread = new ReadFileThread(filePath);

  connect(thread, &ReadFileThread::directoryFound, this, &MainWindowFirma::directoryFoundDeveloper);
  thread->start();

  connect(thread, &ReadFileThread::finished, thread, &QObject::deleteLater);
}
