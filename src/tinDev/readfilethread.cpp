#include "readfilethread.h"
#include <QDebug>
#include <QDir>
#include <QStringList>

ReadFileThread::ReadFileThread(const QString &targetFileName, QObject *parent) : QThread{parent}, targetFileName(targetFileName) {}

void ReadFileThread::run() {
  try {
    QDir userDir(QDir::homePath());
    QString directory = searchForDirectory(userDir);

    if (!directory.isEmpty()) {
      directory = directory + QDir::separator() + "src" + QDir::separator() + "tinDev" + QDir::separator() + "resources" + QDir::separator() + "serialization" +
                  QDir::separator() + targetFileName;
    }
    QDir folder(directory);
    folder.setFilter(QDir::Files | QDir::NoDotAndDotDot);

    QStringList files = folder.entryList(QStringList() << "*.json", QDir::Files);
    for (const QString &file : files) {
      QString absoluteFilePath = directory + QDir::separator() + file;

      QFile fileToRead(absoluteFilePath);
      if (fileToRead.open(QIODevice::ReadOnly)) {
        fileToRead.close();
        foundPaths.append(absoluteFilePath);
      }
    }
    emit directoryFound(foundPaths);
  } catch (const std::exception &e) {
    qDebug() << "Exception occurred in thread:" << e.what();
  } catch (...) {
    qDebug() << "Unknown exception occurred in thread.";
  }

  this->deleteLater();
}

QString ReadFileThread::searchForDirectory(const QDir &currentDir) {
  QStringList subdirs = currentDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
  if (subdirs.contains("tinDev")) {
    QString foundPath = currentDir.filePath("tinDev");
    return foundPath;
  }

  for (const QString &subdir : subdirs) {
    QDir nextDir = currentDir;
    nextDir.cd(subdir);
    QString result = searchForDirectory(nextDir);
    if (!result.isEmpty()) {
      return result;
    }
  }

  return QString("");
}
