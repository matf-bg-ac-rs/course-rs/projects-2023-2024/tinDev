#pragma once
#include <QString>

class IUsernameProvider {
public:
  virtual ~IUsernameProvider() {}
  virtual QString getUsername() const = 0;
};
