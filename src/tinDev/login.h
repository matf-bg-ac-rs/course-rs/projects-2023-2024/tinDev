#pragma once
#include <QWidget>

namespace Ui {
class LogIn;
}

class LogIn : public QWidget {
  Q_OBJECT

public:
  explicit LogIn(QWidget *parent = nullptr);
  ~LogIn();

public slots:
  void kliknutoUlogujSe();
  void zatvoriLogIn();
  void kliknutoNapraviNalogFirma();
  void kliknutoNapraviNalogDeveloper();

  void directoryNotFound();
  void directoryFoundDeveloper(const QString &directory, const QString &sifra);
  void directoryFoundFirma(const QString &directory, const QString &sifra);

private:
  Ui::LogIn *ui;
  void zavrsenLogInDeveloper();
  void zavrsenLogInFirma();
};
