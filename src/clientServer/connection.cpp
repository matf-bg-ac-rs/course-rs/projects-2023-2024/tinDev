#include "connection.h"
#include <QTimerEvent>

static const int TransferTimeout = 30 * 1000;
static const int PongTimeout = 60 * 1000;
static const int PingInterval = 5 * 1000;

Connection::Connection(QObject *parent) : QTcpSocket(parent), writer(this) {
  pingTimer.setInterval(PingInterval);

  connect(this, &QTcpSocket::readyRead, this, &Connection::processReadyRead);
  connect(this, &QTcpSocket::disconnected, &pingTimer, &QTimer::stop);
  connect(&pingTimer, &QTimer::timeout, this, &Connection::sendPing);
  connect(this, &QTcpSocket::connected, this, &Connection::sendGreetingMessage);
}

Connection::Connection(qintptr socketDescriptor, QObject *parent) : Connection(parent) {
  setSocketDescriptor(socketDescriptor);
  reader.setDevice(this);
}

Connection::~Connection() {
  if (isGreetingMessageSent && QAbstractSocket::state() != QAbstractSocket::UnconnectedState) {

    writer.endArray();
    waitForBytesWritten(2000);
  }
}

QString Connection::name() const { return username; }

void Connection::setGreetingMessage(const QString &message, const QByteArray &uniqueId) {
  greetingMessage = message;
  localUniqueId = uniqueId;
}

QByteArray Connection::uniqueId() const { return peerUniqueId; }

bool Connection::sendMessage(const QString &message) {
  if (message.isEmpty())
    return false;

  writer.startMap(1);
  writer.append(PlainText);
  writer.append(message);
  writer.endMap();
  return true;
}

void Connection::timerEvent(QTimerEvent *timerEvent) {
  if (timerEvent->timerId() == transferTimerId) {
    abort();
    killTimer(transferTimerId);
    transferTimerId = -1;
  }
}

void Connection::processReadyRead() {

  reader.reparse();
  while (reader.lastError() == QCborError::NoError) {
    if (state == WaitingForGreeting) {
      if (!reader.isArray())
        break;

      reader.enterContainer();
      state = ReadingGreeting;
    } else if (reader.containerDepth() == 1) {

      if (!reader.hasNext()) {
        reader.leaveContainer();
        disconnectFromHost();
        return;
      }

      if (!reader.isMap() || !reader.isLengthKnown() || reader.length() != 1)
        break;
      reader.enterContainer();
    } else if (currentDataType == Undefined) {

      if (!reader.isInteger())
        break;
      currentDataType = DataType(reader.toInteger());
      reader.next();
    } else {

      if (currentDataType == Greeting) {
        if (state == ReadingGreeting) {
          if (!reader.isContainer() || !reader.isLengthKnown() || reader.length() != 2)
            break;
          state = ProcessingGreeting;
          reader.enterContainer();
        }
        if (state != ProcessingGreeting)
          break;
        if (reader.isString()) {
          auto r = reader.readString();
          buffer += r.data;
        } else if (reader.isByteArray()) {
          auto r = reader.readByteArray();
          peerUniqueId += r.data;
          if (r.status == QCborStreamReader::EndOfString) {
            reader.leaveContainer();
            processGreeting();
          }
        }
        if (state == ProcessingGreeting)
          continue;
      } else if (reader.isString()) {
        auto r = reader.readString();
        buffer += r.data;
        if (r.status != QCborStreamReader::EndOfString)
          continue;
      } else if (reader.isNull()) {
        reader.next();
      } else {
        break;
      }

      reader.leaveContainer();
      if (transferTimerId != -1) {
        killTimer(transferTimerId);
        transferTimerId = -1;
      }

      processData();
    }
  }

  if (reader.lastError() != QCborError::EndOfFile)
    abort();

  if (transferTimerId != -1 && reader.containerDepth() > 1)
    transferTimerId = startTimer(TransferTimeout);
}

void Connection::sendPing() {
  if (pongTime.elapsed() > PongTimeout) {
    abort();
    return;
  }

  writer.startMap(1);
  writer.append(Ping);
  writer.append(nullptr);
  writer.endMap();
}

void Connection::sendGreetingMessage() {
  writer.startArray();

  writer.startMap(1);
  writer.append(Greeting);
  writer.startArray(2);
  writer.append(greetingMessage);
  writer.append(localUniqueId);
  writer.endArray();
  writer.endMap();
  isGreetingMessageSent = true;

  if (!reader.device())
    reader.setDevice(this);
}

void Connection::processGreeting() {
  username = buffer;
  currentDataType = Undefined;
  buffer.clear();

  if (!isValid()) {
    abort();
    return;
  }

  if (!isGreetingMessageSent)
    sendGreetingMessage();

  pingTimer.start();
  pongTime.start();
  state = ReadyForUse;
  emit readyForUse();
}

void Connection::processData() {
  switch (currentDataType) {
  case PlainText:
    emit newMessage(username, buffer);
    break;
  case Ping:
    writer.startMap(1);
    writer.append(Pong);
    writer.append(nullptr);
    writer.endMap();
    break;
  case Pong:
    pongTime.restart();
    break;
  default:
    break;
  }

  currentDataType = Undefined;
  buffer.clear();
}
