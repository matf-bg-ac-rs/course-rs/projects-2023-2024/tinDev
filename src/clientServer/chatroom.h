#pragma once
#include "client.h"
#include <QTextTableFormat>
#include <QWidget>

namespace Ui {
class ChatRoom;
}

class ChatRoom : public QWidget {
  Q_OBJECT

public:
  explicit ChatRoom(QWidget *parent = nullptr);
  ~ChatRoom();

public slots:
  void appendMessage(const QString &from, const QString &message);

private slots:
  void returnPressed();

private:
  Ui::ChatRoom *ui;
  Client client;
  QString myNickName;
  QTextTableFormat tableFormat;
};
