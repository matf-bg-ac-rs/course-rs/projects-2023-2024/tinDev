#include "chatroom.h"
#include "ui_chatroom.h"
#include <QLineEdit>
#include <QMessageBox>
#include <QScrollBar>
#include <QTextTable>
#include <QTimer>

ChatRoom::ChatRoom(QWidget *parent) : QWidget(parent), ui(new Ui::ChatRoom) {
  ui->setupUi(this);

  ui->lePoruka->setFocusPolicy(Qt::StrongFocus);
  ui->teProzor->setFocusPolicy(Qt::NoFocus);
  ui->teProzor->setReadOnly(true);

  connect(ui->lePoruka, &QLineEdit::returnPressed, this, &ChatRoom::returnPressed);
  connect(&client, &Client::newMessage, this, &ChatRoom::appendMessage);

  myNickName = client.nickName();

  tableFormat.setBorder(0);
}

ChatRoom::~ChatRoom() { delete ui; }

void ChatRoom::appendMessage(const QString &from, const QString &message) {
  if (from.isEmpty() || message.isEmpty())
    return;

  QTextCursor cursor(ui->teProzor->textCursor());
  cursor.movePosition(QTextCursor::End);
  QTextTable *table = cursor.insertTable(1, 2, tableFormat);
  table->cellAt(0, 0).firstCursorPosition().insertText('[' + from + "] ");
  table->cellAt(0, 1).firstCursorPosition().insertText(message);
  QScrollBar *bar = ui->teProzor->verticalScrollBar();
  bar->setValue(bar->maximum());
}

void ChatRoom::returnPressed() {
  QString text = ui->lePoruka->text();
  if (text.isEmpty())
    return;

  if (text.startsWith(QChar('/'))) {
    QColor color = ui->teProzor->textColor();
    ui->teProzor->setTextColor(Qt::red);
    ui->teProzor->append(tr("! Unknown command: %1").arg(text.left(text.indexOf(' '))));
    ui->teProzor->setTextColor(color);
  } else {
    client.sendMessage(text);
    appendMessage(myNickName, text);
  }

  ui->lePoruka->clear();
}
