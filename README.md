# tinDev

Inspired by a most popular match making app Tinder, tinDev is an app where you can find future employer/employees with just a swipe. While making an account select program languages you have worked with and the ones you want to do in the future. If you are a company use our app for finding new people to join your team. If you both swipe for each other, match has been made and you can begin your company-developer relationship.

## Demo Video 
Link: [tinDev](https://youtu.be/O6fYCCvBbl8?si=4uK0zOtOq6K5hMud)

## Installation 🔨

1. Download and install [Qt](https://www.qt.io/download) and [Qt Creator](https://www.qt.io/download-qt-installer).
2. If necessary, upgrade the C++ version to C++20.

## Download and Run 🔧

1. Open the terminal and navigate to the desired directory.

2. Clone the repository using the following command:
    ```bash
    git clone git@gitlab.com:matf-bg-ac-rs/course-rs/projects-2023-2024/tinDev.git
    ```

3. Open Qt Creator and load the `project.pro` file.

4. Press the "Run" button in the bottom-left corner of the screen.

## Note
App has been tested on Linux and Windows.


## Developers 👷

 - <a href="https://gitlab.com/labovicca">Aleksandra Labović 150/2019</a>
 - <a href="https://gitlab.com/ivanaivaneza">Ivana Ivaneža 89/2019</a>
 - <a href="https://gitlab.com/pavleparandilovic">Pavle Parandilović 180/2019</a>
 - <a href="https://gitlab.com/meda000">Pavle Medić 409/2021</a>
 - <a href="https://gitlab.com/pele016">Predrag Đorđević 365/2020</a>
 - <a href="https://gitlab.com/pajici">Igor Pajić 152/2019</a>
